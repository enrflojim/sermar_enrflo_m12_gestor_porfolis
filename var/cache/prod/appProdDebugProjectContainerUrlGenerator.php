<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdDebugProjectContainerUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdDebugProjectContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = array(
        'articles' => array (  0 =>   array (    0 => 'card',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ArticleController::listArticlesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'card',    ),    1 =>     array (      0 => 'text',      1 => '/articles',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'formarticle' => array (  0 =>   array (    0 => 'card',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ArticleController::createArticlesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'card',    ),    1 =>     array (      0 => 'text',      1 => '/formarticle',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'editarticle' => array (  0 =>   array (    0 => 'article',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ArticleController::editArticlesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'article',    ),    1 =>     array (      0 => 'text',      1 => '/editarticle',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'deletearticle' => array (  0 =>   array (    0 => 'article',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ArticleController::deleteArticleAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'article',    ),    1 =>     array (      0 => 'text',      1 => '/deletearticle',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cards' => array (  0 =>   array (    0 => 'portfoli',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\CardsController::listCardsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'portfoli',    ),    1 =>     array (      0 => 'text',      1 => '/cards',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'formcard' => array (  0 =>   array (    0 => 'portfoli',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\CardsController::createCardAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'portfoli',    ),    1 =>     array (      0 => 'text',      1 => '/formcard',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'editcard' => array (  0 =>   array (    0 => 'card',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\CardsController::editCardAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'card',    ),    1 =>     array (      0 => 'text',      1 => '/editcard',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'deletecard' => array (  0 =>   array (    0 => 'card',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\CardsController::deleteArticleAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'card',    ),    1 =>     array (      0 => 'text',      1 => '/deletecard',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'card' => array (  0 =>   array (    0 => 'card',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\CardsController::test1Action',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'card',    ),    1 =>     array (      0 => 'text',      1 => '/card',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'contactme' => array (  0 =>   array (    0 => 'portfoli',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ContactController::contactMeFormAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'portfoli',    ),    1 =>     array (      0 => 'text',      1 => '/contactme',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'contact' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ContactController::contactFormAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/contact',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'accesdenied' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ErrorController::accessDeniedction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/accesdenied',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'notfound' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ErrorController::norFoundAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/notfound',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'image' => array (  0 =>   array (    0 => 'article',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ImageController::listImagesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'article',    ),    1 =>     array (      0 => 'text',      1 => '/image',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'formimage' => array (  0 =>   array (    0 => 'article',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ImageController::createImagesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'article',    ),    1 =>     array (      0 => 'text',      1 => '/formimage',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'editimage' => array (  0 =>   array (    0 => 'image',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ImageController::editImagesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'image',    ),    1 =>     array (      0 => 'text',      1 => '/editimage',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'deleteimage' => array (  0 =>   array (    0 => 'image',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ImageController::deleteImagesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'image',    ),    1 =>     array (      0 => 'text',      1 => '/deleteimage',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'portfolis' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\PortfolisController::portfolisAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/portfolis',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'searchportfolis' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\PortfolisController::searchPortfolisAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/searchportfolis',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'portfoli' => array (  0 =>   array (    0 => 'portfoli',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\PortfolisController::portfoliAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'portfoli',    ),    1 =>     array (      0 => 'text',      1 => '/portfoli',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'createportfoli' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\PortfolisController::createPortfoliAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/createportfoli',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'editeportfoli' => array (  0 =>   array (    0 => 'portfoli',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\PortfolisController::editPortfoliAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'portfoli',    ),    1 =>     array (      0 => 'text',      1 => '/editportfoli',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'myportfolis' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\PortfolisController::myPortfolisAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/myportfolis',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'deleteportfoli' => array (  0 =>   array (    0 => 'portfoli',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\PortfolisController::deletePortfoliAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'portfoli',    ),    1 =>     array (      0 => 'text',      1 => '/deleteportfoli',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'register' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\UserController::registroAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/register',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'login' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\UserController::loginAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'edituser' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\UserController::editUserAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/edituser',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'forgotpass' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\UserController::forgotUserAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/forgotpass',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'logout' => array (  0 =>   array (  ),  1 =>   array (  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/logout',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );
        }
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
