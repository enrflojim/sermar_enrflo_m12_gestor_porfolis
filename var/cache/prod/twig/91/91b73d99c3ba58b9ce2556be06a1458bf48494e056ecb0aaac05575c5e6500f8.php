<?php

/* default/error/acces_denied.html.twig */
class __TwigTemplate_30c01d7e4b40e8fb2dd5f7525f358485244702a798bff79b925f70903273ac4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/error/acces_denied.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8e7b75e8c603a23f04e4a203341d861c555ee60da9e3bf27ac0f9db3cab6df7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8e7b75e8c603a23f04e4a203341d861c555ee60da9e3bf27ac0f9db3cab6df7->enter($__internal_d8e7b75e8c603a23f04e4a203341d861c555ee60da9e3bf27ac0f9db3cab6df7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/error/acces_denied.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d8e7b75e8c603a23f04e4a203341d861c555ee60da9e3bf27ac0f9db3cab6df7->leave($__internal_d8e7b75e8c603a23f04e4a203341d861c555ee60da9e3bf27ac0f9db3cab6df7_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_ec72338b51c40a91bfc3145710d05c546d29d2bb1d005a4318d2c07a8cba517f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec72338b51c40a91bfc3145710d05c546d29d2bb1d005a4318d2c07a8cba517f->enter($__internal_ec72338b51c40a91bfc3145710d05c546d29d2bb1d005a4318d2c07a8cba517f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"separator-xd\"></div>
    <div class=\"separator-md\"></div>
    <div class=\"row\">
        <div class=\"col-md-4 col-md-offset-4\">
          <img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/icons/403-forbidden-error.jpg"), "html", null, true);
        echo "\" width=\"400\" alt=\"access denied\" title=\"access denied\">
        </div>
    </div>
";
        
        $__internal_ec72338b51c40a91bfc3145710d05c546d29d2bb1d005a4318d2c07a8cba517f->leave($__internal_ec72338b51c40a91bfc3145710d05c546d29d2bb1d005a4318d2c07a8cba517f_prof);

    }

    public function getTemplateName()
    {
        return "default/error/acces_denied.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 7,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block body %}
    <div class=\"separator-xd\"></div>
    <div class=\"separator-md\"></div>
    <div class=\"row\">
        <div class=\"col-md-4 col-md-offset-4\">
          <img src=\"{{asset('img/icons/403-forbidden-error.jpg')}}\" width=\"400\" alt=\"access denied\" title=\"access denied\">
        </div>
    </div>
{% endblock %}
", "default/error/acces_denied.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/error/acces_denied.html.twig");
    }
}
