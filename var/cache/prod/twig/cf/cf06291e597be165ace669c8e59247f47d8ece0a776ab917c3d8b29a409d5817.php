<?php

/* default/user/form_forgot.html.twig */
class __TwigTemplate_d3fafce219c8b0859bc8359debf8f1cdb6bfd3380dddbc2ede6feb82f2c0e91c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("default/header/header_all.html.twig", "default/user/form_forgot.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_all.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6a742c2edfb68386a9a845325fc5f96fc1cf837f7f04591688fc09218a5b2cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e6a742c2edfb68386a9a845325fc5f96fc1cf837f7f04591688fc09218a5b2cf->enter($__internal_e6a742c2edfb68386a9a845325fc5f96fc1cf837f7f04591688fc09218a5b2cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/user/form_forgot.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e6a742c2edfb68386a9a845325fc5f96fc1cf837f7f04591688fc09218a5b2cf->leave($__internal_e6a742c2edfb68386a9a845325fc5f96fc1cf837f7f04591688fc09218a5b2cf_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e5f40458f8f0c607c55b7fbc87686fcbdf1b85a5cbe63dc6ac97af1984465556 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5f40458f8f0c607c55b7fbc87686fcbdf1b85a5cbe63dc6ac97af1984465556->enter($__internal_e5f40458f8f0c607c55b7fbc87686fcbdf1b85a5cbe63dc6ac97af1984465556_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
</div class=\"row\">
  <div class=\"col-md-6 col-md-offset-3 format-forms color-azul\">
    ";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
    <div class=\"row line\">
      <div class=\"col-md-10 col-md-offset-1\">
        <legend>Forgoot password</legend>
      </div>
    </div>
    <div class=\"row line\">
      <div class=\"col-md-9 col-md-offset-1\">
        ";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'row');
        echo "
      </div>
      ";
        // line 17
        if (($context["errormail"] ?? $this->getContext($context, "errormail"))) {
            // line 18
            echo "      <div class=\"col-md-1\">*</div>
      ";
        } else {
            // line 20
            echo "      <div class=\"col-md-1 windowform\">*<span>The Email no exist</span></div>
      ";
        }
        // line 22
        echo "    </div>
    <div class=\"row line\">
      <div class=\"col-md-9 col-md-offset-1\">
        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'row');
        echo "
      </div>
      <div class=\"col-md-1\">*</div>
    </div>
    <div class=\"row line\">
      <div class=\"col-md-9 col-md-offset-1\">
        ";
        // line 31
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'row');
        echo "
      </div>
      <div class=\"col-md-1\">*</div>
    </div>
    <div class=\"row line\">
      <div class=\"col-md-9 col-md-offset-1\">
        <button type=\"submit\" class=\"btn btn-primary btn-md\">Change password</button>
      </div>
    </div>

    ";
        // line 41
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
  </div>
</div>

";
        
        $__internal_e5f40458f8f0c607c55b7fbc87686fcbdf1b85a5cbe63dc6ac97af1984465556->leave($__internal_e5f40458f8f0c607c55b7fbc87686fcbdf1b85a5cbe63dc6ac97af1984465556_prof);

    }

    public function getTemplateName()
    {
        return "default/user/form_forgot.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 41,  86 => 31,  77 => 25,  72 => 22,  68 => 20,  64 => 18,  62 => 17,  57 => 15,  46 => 7,  40 => 4,  34 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/default/formInsert.html.twig #}
{% extends 'default/header/header_all.html.twig' %}
{% block body %}
{{ form_start(form) }}
</div class=\"row\">
  <div class=\"col-md-6 col-md-offset-3 format-forms color-azul\">
    {{ form_start(form) }}
    <div class=\"row line\">
      <div class=\"col-md-10 col-md-offset-1\">
        <legend>Forgoot password</legend>
      </div>
    </div>
    <div class=\"row line\">
      <div class=\"col-md-9 col-md-offset-1\">
        {{ form_row(form.email) }}
      </div>
      {% if errormail %}
      <div class=\"col-md-1\">*</div>
      {% else %}
      <div class=\"col-md-1 windowform\">*<span>The Email no exist</span></div>
      {% endif %}
    </div>
    <div class=\"row line\">
      <div class=\"col-md-9 col-md-offset-1\">
        {{ form_row(form.plainPassword.first) }}
      </div>
      <div class=\"col-md-1\">*</div>
    </div>
    <div class=\"row line\">
      <div class=\"col-md-9 col-md-offset-1\">
        {{ form_row(form.plainPassword.second) }}
      </div>
      <div class=\"col-md-1\">*</div>
    </div>
    <div class=\"row line\">
      <div class=\"col-md-9 col-md-offset-1\">
        <button type=\"submit\" class=\"btn btn-primary btn-md\">Change password</button>
      </div>
    </div>

    {{ form_end(form) }}
  </div>
</div>

{% endblock %}
", "default/user/form_forgot.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/user/form_forgot.html.twig");
    }
}
