<?php

/* base.html.twig */
class __TwigTemplate_3a393e58010eed31cd72f7b5fd1f3c2072ca05419185f9be7a326e176d45d80d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'jumbotron' => array($this, 'block_jumbotron'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_035826a465552cfd30c880b380e8346d22e27fcb447ef35853a8aa1de6126fd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_035826a465552cfd30c880b380e8346d22e27fcb447ef35853a8aa1de6126fd8->enter($__internal_035826a465552cfd30c880b380e8346d22e27fcb447ef35853a8aa1de6126fd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/estilos.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/ventana.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("tinymce/js/tinymce/skins/lightgray/content.min.css"), "html", null, true);
        echo "\" />
    <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.4.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/functions.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("tinymce/js/tinymce/tinymce.js"), "html", null, true);
        echo "\"></script>
    <script>tinymce.init({selector: 'textarea'});
    </script>
</head>
<body>
    <nav class=\"navbar navbar-default navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navy-1\"
                        aria-expanded=\"false\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/portfolis\">Portfolis</a>
            </div>
            <!-- Iniciar menu -->
            <div class=\"collapse navbar-collapse\" id=\"navy-1\">
                <ul class=\"nav navbar-nav tt-wrapper\">
                    <li><a href=\"/portfolis\" >Home</a></li>
                        ";
        // line 39
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 40
            echo "                        <li><a href=\"/myportfolis\" >My Porfolis</a></li>
                        <li><a href=\"/createportfoli\" >Create Porfoli</a></li>
                        ";
        } else {
            // line 43
            echo "                        <li class=\"window menu\"><span>You must be logged to wiew portfolis</span><a class=\"color-red\" href=\"#\" >My Porfolis</a></li>
                        <li class=\"window menu\"><span>You must be logged to create portfolis</span><a class=\"color-red\" href=\"#\">Create Porfoli</a></li>
                        ";
        }
        // line 46
        echo "                    <li><a href=\"/contact\" >Contact</a></li>
                </ul>
                <ul class=\"nav navbar-nav navbar-right\">
                    ";
        // line 49
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 50
            echo "                        <li><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\">";
            echo twig_escape_filter($this->env, ($context["user"] ?? $this->getContext($context, "user")), "html", null, true);
            echo "</a>
                            <ul class=\"dropdown-menu\" role=\"menu\">
                                <li ><a href=\"/edituser\">Edit ";
            // line 52
            echo twig_escape_filter($this->env, ($context["user"] ?? $this->getContext($context, "user")), "html", null, true);
            echo "</a></li>
                                <li class=\"divider\"></li>
                                <li ><a href=\"/logout\">Logout</a></li>
                            </ul>
                        </li>
                    ";
        } else {
            // line 58
            echo "                        <li><a href=\"/\">Log in</a></li>
                        <li><a href=\"/register\">Register</a></li>
                        ";
        }
        // line 61
        echo "                    <li>
                        <form action=\"/searchportfolis\" class=\"navbar-form\" role=\"search\" method=\"get\">
                            <div class=\"form-group\">
                                <input type=\"text\" id=\"tag\" name=\"tag\" class=\"form-control\" placeholder=\"Search\">
                            </div>
                            <button type=\"submit\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-search\"></span></button>
                        </form>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <header>
    ";
        // line 74
        $this->displayBlock('jumbotron', $context, $blocks);
        // line 75
        echo "</header>
<section class=\"body\">
    <div class=\"row\">
        <div class=\"col-md-12\">
        ";
        // line 79
        $this->displayBlock('body', $context, $blocks);
        // line 80
        echo "    </div>
</div>
</section>
<footer>
    <div class=\"row\">
            <div class=\"copyright\">Ⓒ Copyright | All rights Reserved | Sergi Martinez & Enrique Flo</div>
    </div>
</footer>
</body>
</html>
";
        
        $__internal_035826a465552cfd30c880b380e8346d22e27fcb447ef35853a8aa1de6126fd8->leave($__internal_035826a465552cfd30c880b380e8346d22e27fcb447ef35853a8aa1de6126fd8_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_09813fd3f4a43bd2e8307c0c39a835ae53cbc92f49be95d2c2b2328a0ee7972d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09813fd3f4a43bd2e8307c0c39a835ae53cbc92f49be95d2c2b2328a0ee7972d->enter($__internal_09813fd3f4a43bd2e8307c0c39a835ae53cbc92f49be95d2c2b2328a0ee7972d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_09813fd3f4a43bd2e8307c0c39a835ae53cbc92f49be95d2c2b2328a0ee7972d->leave($__internal_09813fd3f4a43bd2e8307c0c39a835ae53cbc92f49be95d2c2b2328a0ee7972d_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0f1e1889ba980ca26f43488baf6746122991f26a9a17a4fd835ee3814401c7db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f1e1889ba980ca26f43488baf6746122991f26a9a17a4fd835ee3814401c7db->enter($__internal_0f1e1889ba980ca26f43488baf6746122991f26a9a17a4fd835ee3814401c7db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_0f1e1889ba980ca26f43488baf6746122991f26a9a17a4fd835ee3814401c7db->leave($__internal_0f1e1889ba980ca26f43488baf6746122991f26a9a17a4fd835ee3814401c7db_prof);

    }

    // line 74
    public function block_jumbotron($context, array $blocks = array())
    {
        $__internal_b55cbb5d58465b678afbc85f359c0885c0d20a8571f234cc27ae57d65d97afb1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b55cbb5d58465b678afbc85f359c0885c0d20a8571f234cc27ae57d65d97afb1->enter($__internal_b55cbb5d58465b678afbc85f359c0885c0d20a8571f234cc27ae57d65d97afb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "jumbotron"));

        
        $__internal_b55cbb5d58465b678afbc85f359c0885c0d20a8571f234cc27ae57d65d97afb1->leave($__internal_b55cbb5d58465b678afbc85f359c0885c0d20a8571f234cc27ae57d65d97afb1_prof);

    }

    // line 79
    public function block_body($context, array $blocks = array())
    {
        $__internal_dd155846e19d535328cfcb25496e02e4e03981c3cbc7cbfe4dfdb84193f7c4c4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd155846e19d535328cfcb25496e02e4e03981c3cbc7cbfe4dfdb84193f7c4c4->enter($__internal_dd155846e19d535328cfcb25496e02e4e03981c3cbc7cbfe4dfdb84193f7c4c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_dd155846e19d535328cfcb25496e02e4e03981c3cbc7cbfe4dfdb84193f7c4c4->leave($__internal_dd155846e19d535328cfcb25496e02e4e03981c3cbc7cbfe4dfdb84193f7c4c4_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 79,  208 => 74,  197 => 6,  185 => 5,  168 => 80,  166 => 79,  160 => 75,  158 => 74,  143 => 61,  138 => 58,  129 => 52,  123 => 50,  121 => 49,  116 => 46,  111 => 43,  106 => 40,  104 => 39,  79 => 17,  75 => 16,  71 => 15,  67 => 14,  63 => 13,  59 => 12,  55 => 11,  51 => 10,  47 => 9,  43 => 8,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
    {% block stylesheets %}{% endblock %}
    <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/bootstrap.css')}}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/jquery-ui.css')}}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/estilos.css')}}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/ventana.css')}}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('tinymce/js/tinymce/skins/lightgray/content.min.css')}}\" />
    <script src=\"{{ asset('js/jquery-2.2.4.js')}}\"></script>
    <script src=\"{{ asset('js/bootstrap.js')}}\"></script>
    <script src=\"{{ asset('js/jquery-ui.js')}}\"></script>
    <script src=\"{{ asset('js/functions.js')}}\"></script>
    <script src=\"{{ asset('tinymce/js/tinymce/tinymce.js')}}\"></script>
    <script>tinymce.init({selector: 'textarea'});
    </script>
</head>
<body>
    <nav class=\"navbar navbar-default navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navy-1\"
                        aria-expanded=\"false\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/portfolis\">Portfolis</a>
            </div>
            <!-- Iniciar menu -->
            <div class=\"collapse navbar-collapse\" id=\"navy-1\">
                <ul class=\"nav navbar-nav tt-wrapper\">
                    <li><a href=\"/portfolis\" >Home</a></li>
                        {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                        <li><a href=\"/myportfolis\" >My Porfolis</a></li>
                        <li><a href=\"/createportfoli\" >Create Porfoli</a></li>
                        {% else %}
                        <li class=\"window menu\"><span>You must be logged to wiew portfolis</span><a class=\"color-red\" href=\"#\" >My Porfolis</a></li>
                        <li class=\"window menu\"><span>You must be logged to create portfolis</span><a class=\"color-red\" href=\"#\">Create Porfoli</a></li>
                        {% endif %}
                    <li><a href=\"/contact\" >Contact</a></li>
                </ul>
                <ul class=\"nav navbar-nav navbar-right\">
                    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                        <li><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\">{{(user)}}</a>
                            <ul class=\"dropdown-menu\" role=\"menu\">
                                <li ><a href=\"/edituser\">Edit {{(user)}}</a></li>
                                <li class=\"divider\"></li>
                                <li ><a href=\"/logout\">Logout</a></li>
                            </ul>
                        </li>
                    {% else %}
                        <li><a href=\"/\">Log in</a></li>
                        <li><a href=\"/register\">Register</a></li>
                        {% endif %}
                    <li>
                        <form action=\"/searchportfolis\" class=\"navbar-form\" role=\"search\" method=\"get\">
                            <div class=\"form-group\">
                                <input type=\"text\" id=\"tag\" name=\"tag\" class=\"form-control\" placeholder=\"Search\">
                            </div>
                            <button type=\"submit\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-search\"></span></button>
                        </form>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <header>
    {% block jumbotron %}{% endblock %}
</header>
<section class=\"body\">
    <div class=\"row\">
        <div class=\"col-md-12\">
        {% block body %}{% endblock %}
    </div>
</div>
</section>
<footer>
    <div class=\"row\">
            <div class=\"copyright\">Ⓒ Copyright | All rights Reserved | Sergi Martinez & Enrique Flo</div>
    </div>
</footer>
</body>
</html>
", "base.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/base.html.twig");
    }
}
