<?php

/* default/header/header_all.html.twig */
class __TwigTemplate_bd3c792755bec8c56c9bbe15d8a352167cd93ca15ce0d79cd4c0fd781e6e5df6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/header/header_all.html.twig", 1);
        $this->blocks = array(
            'jumbotron' => array($this, 'block_jumbotron'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_173930eba7622ca3bb1c0bb0387309b0854e4bca592de5379751c29904bf3ae7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_173930eba7622ca3bb1c0bb0387309b0854e4bca592de5379751c29904bf3ae7->enter($__internal_173930eba7622ca3bb1c0bb0387309b0854e4bca592de5379751c29904bf3ae7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/header/header_all.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_173930eba7622ca3bb1c0bb0387309b0854e4bca592de5379751c29904bf3ae7->leave($__internal_173930eba7622ca3bb1c0bb0387309b0854e4bca592de5379751c29904bf3ae7_prof);

    }

    // line 2
    public function block_jumbotron($context, array $blocks = array())
    {
        $__internal_70153016229894058a201484d66b25e2b3c99e2c2413596a4d6cb6f6ef9739bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70153016229894058a201484d66b25e2b3c99e2c2413596a4d6cb6f6ef9739bd->enter($__internal_70153016229894058a201484d66b25e2b3c99e2c2413596a4d6cb6f6ef9739bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "jumbotron"));

        // line 3
        echo "<!-- Jumbotron -->
<div class=\"jumbotron color-azul\">
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-md-8 col-md-offset-2\">
        <center><h1>Portfolis</h1></center>
      </div>
    </div>
    <div class=\"row\">
      <div class=\"col-md-2  col-md-offset-5\">
        ";
        // line 13
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 14
            echo "        <a href=\"/createportfoli\" class=\"btn btn-primary\">Create portfoli</a>
        ";
        } else {
            // line 16
            echo "        <a href=\"#\" class=\"btn btn-primary window jumbo1\" disabled>
                <span>You must be logged to create portfolis</span>
                      Create portfoli</a>
        ";
        }
        // line 20
        echo "      </div>
      </div>
    </div>
  </div>
      </div>
<!-- End Jumbotron -->
";
        
        $__internal_70153016229894058a201484d66b25e2b3c99e2c2413596a4d6cb6f6ef9739bd->leave($__internal_70153016229894058a201484d66b25e2b3c99e2c2413596a4d6cb6f6ef9739bd_prof);

    }

    // line 27
    public function block_body($context, array $blocks = array())
    {
        $__internal_8ca192842ffe1590c56bd14799dba72896f55039b56ea38386d74cbe1a4b78fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ca192842ffe1590c56bd14799dba72896f55039b56ea38386d74cbe1a4b78fd->enter($__internal_8ca192842ffe1590c56bd14799dba72896f55039b56ea38386d74cbe1a4b78fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_8ca192842ffe1590c56bd14799dba72896f55039b56ea38386d74cbe1a4b78fd->leave($__internal_8ca192842ffe1590c56bd14799dba72896f55039b56ea38386d74cbe1a4b78fd_prof);

    }

    public function getTemplateName()
    {
        return "default/header/header_all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 27,  65 => 20,  59 => 16,  55 => 14,  53 => 13,  41 => 3,  35 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block jumbotron %}
<!-- Jumbotron -->
<div class=\"jumbotron color-azul\">
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-md-8 col-md-offset-2\">
        <center><h1>Portfolis</h1></center>
      </div>
    </div>
    <div class=\"row\">
      <div class=\"col-md-2  col-md-offset-5\">
        {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
        <a href=\"/createportfoli\" class=\"btn btn-primary\">Create portfoli</a>
        {% else %}
        <a href=\"#\" class=\"btn btn-primary window jumbo1\" disabled>
                <span>You must be logged to create portfolis</span>
                      Create portfoli</a>
        {% endif %}
      </div>
      </div>
    </div>
  </div>
      </div>
<!-- End Jumbotron -->
{% endblock %}
{% block body %}{% endblock %}
", "default/header/header_all.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/header/header_all.html.twig");
    }
}
