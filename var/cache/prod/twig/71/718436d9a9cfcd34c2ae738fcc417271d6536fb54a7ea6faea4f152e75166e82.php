<?php

/* default/cardTemplates/programmerCard.html.twig */
class __TwigTemplate_544ffec869f28f80d3311a3d8a487023ec154cf8914b93f2a8d4d7477be40a43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/cardTemplates/programmerCard.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascript' => array($this, 'block_javascript'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ba0830cbb8eedbac899dcdb667e3d691b78259a4c1b51b54532ed0fdde25af85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba0830cbb8eedbac899dcdb667e3d691b78259a4c1b51b54532ed0fdde25af85->enter($__internal_ba0830cbb8eedbac899dcdb667e3d691b78259a4c1b51b54532ed0fdde25af85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/cardTemplates/programmerCard.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ba0830cbb8eedbac899dcdb667e3d691b78259a4c1b51b54532ed0fdde25af85->leave($__internal_ba0830cbb8eedbac899dcdb667e3d691b78259a4c1b51b54532ed0fdde25af85_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_503a862d4aa302d39786197ce6073789fbd974ed6d6a24a627fc4f529380bbd5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_503a862d4aa302d39786197ce6073789fbd974ed6d6a24a627fc4f529380bbd5->enter($__internal_503a862d4aa302d39786197ce6073789fbd974ed6d6a24a627fc4f529380bbd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/estilos.css"), "html", null, true);
        echo "\" />
";
        
        $__internal_503a862d4aa302d39786197ce6073789fbd974ed6d6a24a627fc4f529380bbd5->leave($__internal_503a862d4aa302d39786197ce6073789fbd974ed6d6a24a627fc4f529380bbd5_prof);

    }

    // line 7
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_86b0972800e79b7642f8dbe5ead335ba7d5dafac3d29023ca8c106173568367e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86b0972800e79b7642f8dbe5ead335ba7d5dafac3d29023ca8c106173568367e->enter($__internal_86b0972800e79b7642f8dbe5ead335ba7d5dafac3d29023ca8c106173568367e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 8
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/slider.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_86b0972800e79b7642f8dbe5ead335ba7d5dafac3d29023ca8c106173568367e->leave($__internal_86b0972800e79b7642f8dbe5ead335ba7d5dafac3d29023ca8c106173568367e_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_9bc5ee2bcc7cc44077e49897d7546a4a77bba5da82cb6053b650728f41e45bae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9bc5ee2bcc7cc44077e49897d7546a4a77bba5da82cb6053b650728f41e45bae->enter($__internal_9bc5ee2bcc7cc44077e49897d7546a4a77bba5da82cb6053b650728f41e45bae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "
    <div class=\"row\">
        <div class=\"col-md-2 col-md-offset-1\">
            <h3>Cards</h3>
            <div class=\"panel-group\" id=\"acordeon\">

                ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sub"] ?? $this->getContext($context, "sub")));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 19
            echo "                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#acordeon\" href=\"#collapse";
            // line 22
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "</a>
                            </h4>
                        </div>
                        <div id=\"collapse";
            // line 25
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "\" class=\"panel-collapse collapse\">
                            ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "cards", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
                // line 27
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["card"], "subCategories", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["subcategories"]) {
                    // line 28
                    echo "                                    ";
                    if (($this->getAttribute($context["subcategories"], "nameSubcategory", array()) == $context["s"])) {
                        // line 29
                        echo "                                        <div class=\"panel-body\"><a href=\"/card/";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "id", array()), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "title", array()), "html", null, true);
                        echo "</a></div>
                                        ";
                    }
                    // line 31
                    echo "                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcategories'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 32
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "            </div>
        </div>
        <div class=\"col-md-8\">
            <div class=\"row\">
                <div class=\"col-md-12 format-forms color-azul\">
                    <div class=\"row\">
                        <div class=\"col-md-11\">
                            <h1>";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "title", array()), "html", null, true);
        echo "</h1>
                            ";
        // line 44
        echo $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "description", array());
        echo "
                            ";
        // line 45
        if ( !(null === $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "link", array()))) {
            // line 46
            echo "                                <div class=\"row\">
                                    <div class=\"col-md-11\">
                                        <h5>Link: <i><a href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "link", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "link", array()), "html", null, true);
            echo "</a></i></h5>
                                    </div>
                                </div>
                            ";
        }
        // line 52
        echo "                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-10 col-md-offset-1\">
                            ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "articles", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 57
            echo "                                <article>
                                    <h3>";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "title", array()), "html", null, true);
            echo "</h3>
                                    ";
            // line 59
            $context["splitbyCode"] = twig_split_filter($this->env, $this->getAttribute($context["article"], "content", array()), "{code}");
            // line 60
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["splitbyCode"] ?? $this->getContext($context, "splitbyCode"))) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 61
                echo "                                        ";
                if ((($context["i"] % 2) == 1)) {
                    // line 62
                    echo "                                            </p><pre class=\"code\">";
                    echo $this->getAttribute(($context["splitbyCode"] ?? $this->getContext($context, "splitbyCode")), $context["i"], array(), "array");
                    echo "</pre><p>
                                            ";
                } else {
                    // line 64
                    echo "                                            <p>";
                    echo $this->getAttribute(($context["splitbyCode"] ?? $this->getContext($context, "splitbyCode")), $context["i"], array(), "array");
                    echo "
                                            ";
                }
                // line 66
                echo "                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "                                    <div class=\"row\">
                                        <div class=\"col-md-8 col-md-offset-2\">
                                            <div id=\"myCarousel";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
            echo "\" class=\"carousel slide\" data-ride=\"carousel\">
                                                <!-- Wrapper for slides -->
                                                <div class=\"carousel-inner\">
                                                    <!-- Items -->
                                                    ";
            // line 73
            $context["i"] = 0;
            // line 74
            echo "                                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["article"], "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 75
                echo "                                                        ";
                if ((($context["i"] ?? $this->getContext($context, "i")) == 0)) {
                    // line 76
                    echo "                                                            <div class=\"item active\">
                                                            ";
                } else {
                    // line 78
                    echo "                                                                <div class=\"item\">
                                                                ";
                }
                // line 80
                echo "                                                                <img class=\"imgSlider\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/articles/" . $this->getAttribute($context["image"], "image", array()))), "html", null, true);
                echo "\">
                                                                <div class=\"carousel-caption\">
                                                                    <h3>";
                // line 82
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo "</h3>
                                                                    <a hred=\"#\" class=\"btn btn-primary btn-xs\" data-toggle=\"modal\" data-target=\"#myModal";
                // line 83
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                echo "\">Read me</a>
                                                                </div>
                                                            </div>
                                                            ";
                // line 86
                $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
                // line 87
                echo "                                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 88
            echo "                                                        <!-- End Items -->
                                                    </div>
                                                    <ul class=\"nav nav-pills nav-justified\">
                                                        ";
            // line 91
            $context["i"] = 0;
            // line 92
            echo "                                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["article"], "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 93
                echo "                                                            ";
                if ((($context["i"] ?? $this->getContext($context, "i")) == 0)) {
                    // line 94
                    echo "                                                                <li data-target=\"#myCarousel";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
                    echo "\" data-slide-to=\"";
                    echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                    echo "\" class=\"active\">
                                                                ";
                } else {
                    // line 96
                    echo "                                                                <li data-target=\"#myCarousel";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
                    echo "\" data-slide-to=\"";
                    echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                    echo "\">
                                                                ";
                }
                // line 98
                echo "                                                                <a href=\"#\">
                                                                    <small><img width=\"25\" class=\"barra\" src=\"";
                // line 99
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/articles/" . $this->getAttribute($context["image"], "image", array()))), "html", null, true);
                echo "\"></small>
                                                                </a>
                                                            </li>
                                                            ";
                // line 102
                $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
                // line 103
                echo "                                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 104
            echo "                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                </article>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 110
        echo "                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    ";
        // line 117
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "articles", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 118
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["article"], "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 119
                echo "            <div class=\"modal fade\" id=\"myModal";
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                echo "\" role=\"dialog\">
                <div class=\"modal-dialog tam-modal\">

                    <!-- Modal content-->
                    <div class=\"modal-content\">
                        <div class=\"modal-header centro\">
                            <h2 class=\"modal-title\">";
                // line 125
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo "</h2>
                        </div>
                        <div class=\"modal-body\">
                            <div class=\"row\">
                                <div class=\"col-md-8\">
                                    <img width=\"700\" src=\"";
                // line 130
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/articles/" . $this->getAttribute($context["image"], "image", array()))), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo "\"
                                         alt=\"";
                // line 131
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo " not found\">
                                </div>
                                <div class=\"col-md-4\">
                                    ";
                // line 134
                echo $this->getAttribute($context["image"], "description", array());
                echo "
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 143
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_9bc5ee2bcc7cc44077e49897d7546a4a77bba5da82cb6053b650728f41e45bae->leave($__internal_9bc5ee2bcc7cc44077e49897d7546a4a77bba5da82cb6053b650728f41e45bae_prof);

    }

    public function getTemplateName()
    {
        return "default/cardTemplates/programmerCard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  395 => 143,  380 => 134,  374 => 131,  368 => 130,  360 => 125,  350 => 119,  345 => 118,  341 => 117,  332 => 110,  321 => 104,  315 => 103,  313 => 102,  307 => 99,  304 => 98,  296 => 96,  288 => 94,  285 => 93,  280 => 92,  278 => 91,  273 => 88,  267 => 87,  265 => 86,  259 => 83,  255 => 82,  249 => 80,  245 => 78,  241 => 76,  238 => 75,  233 => 74,  231 => 73,  224 => 69,  220 => 67,  214 => 66,  208 => 64,  202 => 62,  199 => 61,  194 => 60,  192 => 59,  188 => 58,  185 => 57,  181 => 56,  175 => 52,  166 => 48,  162 => 46,  160 => 45,  156 => 44,  152 => 43,  143 => 36,  135 => 33,  129 => 32,  123 => 31,  115 => 29,  112 => 28,  107 => 27,  103 => 26,  99 => 25,  91 => 22,  86 => 19,  82 => 18,  74 => 12,  68 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}

{% block stylesheets %}
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/estilos.css')}}\" />
{% endblock %}

{% block javascript %}
    <script src=\"{{ asset('js/slider.js')}}\"></script>
{% endblock %}

{% block body %}

    <div class=\"row\">
        <div class=\"col-md-2 col-md-offset-1\">
            <h3>Cards</h3>
            <div class=\"panel-group\" id=\"acordeon\">

                {% for s in sub %}
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#acordeon\" href=\"#collapse{{(s)}}\">{{(s)}}</a>
                            </h4>
                        </div>
                        <div id=\"collapse{{(s)}}\" class=\"panel-collapse collapse\">
                            {% for card in portfoli.cards %}
                                {% for subcategories in card.subCategories %}
                                    {% if subcategories.nameSubcategory == s %}
                                        <div class=\"panel-body\"><a href=\"/card/{{(card.id)}}\">{{(card.title)}}</a></div>
                                        {% endif %}
                                    {% endfor %}
                                {% endfor %}
                        </div>
                    </div>
                {% endfor %}
            </div>
        </div>
        <div class=\"col-md-8\">
            <div class=\"row\">
                <div class=\"col-md-12 format-forms color-azul\">
                    <div class=\"row\">
                        <div class=\"col-md-11\">
                            <h1>{{card.title}}</h1>
                            {{card.description|raw}}
                            {% if card.link is not null %}
                                <div class=\"row\">
                                    <div class=\"col-md-11\">
                                        <h5>Link: <i><a href=\"{{card.link}}\">{{card.link}}</a></i></h5>
                                    </div>
                                </div>
                            {% endif %}
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-10 col-md-offset-1\">
                            {% for article in card.articles %}
                                <article>
                                    <h3>{{article.title}}</h3>
                                    {% set splitbyCode = article.content|split(\"{code}\") %}
                                    {% for i in 0..splitbyCode|length-1 %}
                                        {% if (i%2==1) %}
                                            </p><pre class=\"code\">{{splitbyCode[i]|raw}}</pre><p>
                                            {% else %}
                                            <p>{{splitbyCode[i]|raw}}
                                            {% endif %}
                                        {% endfor %}
                                    <div class=\"row\">
                                        <div class=\"col-md-8 col-md-offset-2\">
                                            <div id=\"myCarousel{{(article.id)}}\" class=\"carousel slide\" data-ride=\"carousel\">
                                                <!-- Wrapper for slides -->
                                                <div class=\"carousel-inner\">
                                                    <!-- Items -->
                                                    {% set i=0 %}
                                                    {% for image in article.images %}
                                                        {% if i==0 %}
                                                            <div class=\"item active\">
                                                            {% else %}
                                                                <div class=\"item\">
                                                                {% endif %}
                                                                <img class=\"imgSlider\" src=\"{{asset('img/articles/'~image.image)}}\">
                                                                <div class=\"carousel-caption\">
                                                                    <h3>{{(image.title)}}</h3>
                                                                    <a hred=\"#\" class=\"btn btn-primary btn-xs\" data-toggle=\"modal\" data-target=\"#myModal{{(image.id)}}\">Read me</a>
                                                                </div>
                                                            </div>
                                                            {% set i=i+1 %}
                                                        {% endfor %}
                                                        <!-- End Items -->
                                                    </div>
                                                    <ul class=\"nav nav-pills nav-justified\">
                                                        {% set i=0 %}
                                                        {% for image in article.images %}
                                                            {% if i==0 %}
                                                                <li data-target=\"#myCarousel{{(article.id)}}\" data-slide-to=\"{{(i)}}\" class=\"active\">
                                                                {% else %}
                                                                <li data-target=\"#myCarousel{{(article.id)}}\" data-slide-to=\"{{(i)}}\">
                                                                {% endif %}
                                                                <a href=\"#\">
                                                                    <small><img width=\"25\" class=\"barra\" src=\"{{asset('img/articles/'~image.image)}}\"></small>
                                                                </a>
                                                            </li>
                                                            {% set i=i+1 %}
                                                        {% endfor %}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                </article>
                            {% endfor %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    {% for article in card.articles %}
        {% for image in article.images %}
            <div class=\"modal fade\" id=\"myModal{{(image.id)}}\" role=\"dialog\">
                <div class=\"modal-dialog tam-modal\">

                    <!-- Modal content-->
                    <div class=\"modal-content\">
                        <div class=\"modal-header centro\">
                            <h2 class=\"modal-title\">{{(image.title)}}</h2>
                        </div>
                        <div class=\"modal-body\">
                            <div class=\"row\">
                                <div class=\"col-md-8\">
                                    <img width=\"700\" src=\"{{ asset('img/articles/'~image.image)}}\" title=\"{{(image.title)}}\"
                                         alt=\"{{(image.title)}} not found\">
                                </div>
                                <div class=\"col-md-4\">
                                    {{(image.description|raw)}}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        {% endfor %}
    {% endfor %}
{% endblock %}
", "default/cardTemplates/programmerCard.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/cardTemplates/programmerCard.html.twig");
    }
}
