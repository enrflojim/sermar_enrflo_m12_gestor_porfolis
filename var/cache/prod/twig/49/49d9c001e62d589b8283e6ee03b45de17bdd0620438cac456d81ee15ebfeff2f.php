<?php

/* default/varios/contact.html.twig */
class __TwigTemplate_4a2471fdbcac177d32d8a0cc1ceba5e618cbebffe7a574fe14cb04789b8b8402 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_all.html.twig", "default/varios/contact.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_all.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ecfc42cb13ea17e8fb920dc4db0bc4ba9c6ac964e98c4a247b4225d5051e8ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ecfc42cb13ea17e8fb920dc4db0bc4ba9c6ac964e98c4a247b4225d5051e8ad->enter($__internal_3ecfc42cb13ea17e8fb920dc4db0bc4ba9c6ac964e98c4a247b4225d5051e8ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/varios/contact.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3ecfc42cb13ea17e8fb920dc4db0bc4ba9c6ac964e98c4a247b4225d5051e8ad->leave($__internal_3ecfc42cb13ea17e8fb920dc4db0bc4ba9c6ac964e98c4a247b4225d5051e8ad_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_94855c6d420f48123126d4468c3e33464cb4ad4a344ab5ef65a58ece51771ad2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94855c6d420f48123126d4468c3e33464cb4ad4a344ab5ef65a58ece51771ad2->enter($__internal_94855c6d420f48123126d4468c3e33464cb4ad4a344ab5ef65a58ece51771ad2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-10 col-md-offset-1 format-forms color-azul\">
              <div class=\"row\">
                  <div class=\"col-md-11\">
                  <h1>";
        // line 8
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</h1>
                  <p>You can contact me using your email or this webpage.</p>
                  ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
                  ";
        // line 11
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
                </div>
              </div>
            </div>
        </div>
    </div>
";
        
        $__internal_94855c6d420f48123126d4468c3e33464cb4ad4a344ab5ef65a58ece51771ad2->leave($__internal_94855c6d420f48123126d4468c3e33464cb4ad4a344ab5ef65a58ece51771ad2_prof);

    }

    public function getTemplateName()
    {
        return "default/varios/contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 11,  52 => 10,  47 => 8,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_all.html.twig' %}
{% block body %}
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-10 col-md-offset-1 format-forms color-azul\">
              <div class=\"row\">
                  <div class=\"col-md-11\">
                  <h1>{{title}}</h1>
                  <p>You can contact me using your email or this webpage.</p>
                  {{ form_start(form) }}
                  {{ form_end(form) }}
                </div>
              </div>
            </div>
        </div>
    </div>
{% endblock %}
", "default/varios/contact.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/varios/contact.html.twig");
    }
}
