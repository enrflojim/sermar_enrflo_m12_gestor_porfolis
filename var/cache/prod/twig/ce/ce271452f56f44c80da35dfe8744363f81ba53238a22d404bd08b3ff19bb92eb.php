<?php

/* default/images/list_images.html.twig */
class __TwigTemplate_e348994e0421f59ef52786dddde6bb3902b0793806b90281f787ab3be2cac03c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/images/list_images.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bf0d6f600610e361caf05a60826424285e266ff7f6137ade4e0d42c857699059 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf0d6f600610e361caf05a60826424285e266ff7f6137ade4e0d42c857699059->enter($__internal_bf0d6f600610e361caf05a60826424285e266ff7f6137ade4e0d42c857699059_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/images/list_images.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bf0d6f600610e361caf05a60826424285e266ff7f6137ade4e0d42c857699059->leave($__internal_bf0d6f600610e361caf05a60826424285e266ff7f6137ade4e0d42c857699059_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_8e577f56cadc8e11c8e991535444b984f7e2dc06c1d5db116626f5790378efed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e577f56cadc8e11c8e991535444b984f7e2dc06c1d5db116626f5790378efed->enter($__internal_8e577f56cadc8e11c8e991535444b984f7e2dc06c1d5db116626f5790378efed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div id=\"formu\" class=\"format\">
        <div class=\"row format-add color-black\">
            <div class=\"col-md-2\"><h5>Card</h5></div>
            <div class=\"col-md-2\"><h5>Article</h5></div>
            <div class=\"col-md-2\"><h5>Title</h5></div>
            <div class=\"col-md-3\"><h5>Description</h5></div>
            <div class=\"col-md-1\"><h5>Image</h5></div>
            <div class=\"col-md-1\"><h5>Edit image</h5></div>
            <div class=\"col-md-1\"><h5>Delete image</h5></div>
        </div>
        ";
        // line 13
        if ((twig_length_filter($this->env, $this->getAttribute(($context["article"] ?? $this->getContext($context, "article")), "images", array())) == 0)) {
            // line 14
            echo "            <div class=\"row\">
                <div class=\"col-md-4 colmd-offset-1\">
                    <h3>Any Images in this Article</h3>
                </div>
            </div>
        ";
        } else {
            // line 20
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["article"] ?? $this->getContext($context, "article")), "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 21
                echo "                <div class=\"row\">
                    <div class=\"col-md-2\"><h5>";
                // line 22
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["image"], "idArticle", array()), "idCard", array()), "title", array()), "html", null, true);
                echo "</h5></div>
                    <div class=\"col-md-2\"><h5>";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["image"], "idArticle", array()), "title", array()), "html", null, true);
                echo "</h5></div>
                    <div class=\"col-md-2\"><h5>";
                // line 24
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo "</h5></div>
                    <div class=\"col-md-3\"><h5>";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "description", array()), "html", null, true);
                echo "</h5></div>
                    <div class=\"col-md-1\"><img src=\"";
                // line 26
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/articles/" . $this->getAttribute($context["image"], "image", array()))), "html", null, true);
                echo "\" width=\"30\"></div>
                    <div class=\"col-md-1\">
                        <a href=\"/editimage/";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/icons/edit.png"), "html", null, true);
                echo "\" alt=\"edit\" tittle=\"edit\" width=\"30\"></a>
                    </div>
                    <div class=\"col-md-1\">
                        <a href=\"/deleteimage/";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                echo "\"  onclick=\"return confirm('Are you sure delete image ?')\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/icons/delete.png"), "html", null, true);
                echo "\" alt=\"delete\" tittle=\"delete\" width=\"30\"></a>
                    </div>
                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 35
            echo "        ";
        }
        // line 36
        echo "        <div class=\"row\">
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/portfoli/";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["article"] ?? $this->getContext($context, "article")), "idCard", array()), "idPortfoli", array()), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">Portfoli</a>
            </div>
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/cards/";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["article"] ?? $this->getContext($context, "article")), "idCard", array()), "idPortfoli", array()), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">List cards</a>
            </div>
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/articles/";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["article"] ?? $this->getContext($context, "article")), "idCard", array()), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">List article</a>
            </div>
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/formimage/";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute(($context["article"] ?? $this->getContext($context, "article")), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">Create image</a>
            </div>
        </div>
        <hr>
    </div>
";
        
        $__internal_8e577f56cadc8e11c8e991535444b984f7e2dc06c1d5db116626f5790378efed->leave($__internal_8e577f56cadc8e11c8e991535444b984f7e2dc06c1d5db116626f5790378efed_prof);

    }

    public function getTemplateName()
    {
        return "default/images/list_images.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 47,  130 => 44,  124 => 41,  118 => 38,  114 => 36,  111 => 35,  99 => 31,  91 => 28,  86 => 26,  82 => 25,  78 => 24,  74 => 23,  70 => 22,  67 => 21,  62 => 20,  54 => 14,  52 => 13,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}
{% block body %}
    <div id=\"formu\" class=\"format\">
        <div class=\"row format-add color-black\">
            <div class=\"col-md-2\"><h5>Card</h5></div>
            <div class=\"col-md-2\"><h5>Article</h5></div>
            <div class=\"col-md-2\"><h5>Title</h5></div>
            <div class=\"col-md-3\"><h5>Description</h5></div>
            <div class=\"col-md-1\"><h5>Image</h5></div>
            <div class=\"col-md-1\"><h5>Edit image</h5></div>
            <div class=\"col-md-1\"><h5>Delete image</h5></div>
        </div>
        {% if article.images|length==0%}
            <div class=\"row\">
                <div class=\"col-md-4 colmd-offset-1\">
                    <h3>Any Images in this Article</h3>
                </div>
            </div>
        {% else %}
            {% for image in article.images %}
                <div class=\"row\">
                    <div class=\"col-md-2\"><h5>{{(image.idArticle.idCard.title)}}</h5></div>
                    <div class=\"col-md-2\"><h5>{{(image.idArticle.title)}}</h5></div>
                    <div class=\"col-md-2\"><h5>{{(image.title)}}</h5></div>
                    <div class=\"col-md-3\"><h5>{{(image.description)}}</h5></div>
                    <div class=\"col-md-1\"><img src=\"{{ asset('img/articles/'~image.image) }}\" width=\"30\"></div>
                    <div class=\"col-md-1\">
                        <a href=\"/editimage/{{(image.id)}}\"><img src=\"{{ asset(\"img/icons/edit.png\")}}\" alt=\"edit\" tittle=\"edit\" width=\"30\"></a>
                    </div>
                    <div class=\"col-md-1\">
                        <a href=\"/deleteimage/{{(image.id)}}\"  onclick=\"return confirm('Are you sure delete image ?')\"><img src=\"{{ asset(\"img/icons/delete.png\")}}\" alt=\"delete\" tittle=\"delete\" width=\"30\"></a>
                    </div>
                </div>
            {% endfor %}
        {% endif %}
        <div class=\"row\">
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/portfoli/{{(article.idCard.idPortfoli.id)}}\" class=\"btn btn-primary btn-md\">Portfoli</a>
            </div>
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/cards/{{(article.idCard.idPortfoli.id)}}\" class=\"btn btn-primary btn-md\">List cards</a>
            </div>
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/articles/{{(article.idCard.id)}}\" class=\"btn btn-primary btn-md\">List article</a>
            </div>
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/formimage/{{(article.id)}}\" class=\"btn btn-primary btn-md\">Create image</a>
            </div>
        </div>
        <hr>
    </div>
{% endblock %}
", "default/images/list_images.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/images/list_images.html.twig");
    }
}
