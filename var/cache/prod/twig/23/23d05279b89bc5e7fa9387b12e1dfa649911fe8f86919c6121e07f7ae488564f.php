<?php

/* default/articles/list_article.html.twig */
class __TwigTemplate_76c3616839b9c3a8db249e80dc525744a0b06d23fc736f3cbe5d512ddfb82567 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/articles/list_article.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d5bf5a4a1fba46e71eb05ef21cd65a718d3eecc4d34b66cf1f2c2602e848be4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5bf5a4a1fba46e71eb05ef21cd65a718d3eecc4d34b66cf1f2c2602e848be4a->enter($__internal_d5bf5a4a1fba46e71eb05ef21cd65a718d3eecc4d34b66cf1f2c2602e848be4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/articles/list_article.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d5bf5a4a1fba46e71eb05ef21cd65a718d3eecc4d34b66cf1f2c2602e848be4a->leave($__internal_d5bf5a4a1fba46e71eb05ef21cd65a718d3eecc4d34b66cf1f2c2602e848be4a_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_9ba7fabb347c29d22beab3009e99853bbc464f0d76004a038a6c852e8c6e406c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ba7fabb347c29d22beab3009e99853bbc464f0d76004a038a6c852e8c6e406c->enter($__internal_9ba7fabb347c29d22beab3009e99853bbc464f0d76004a038a6c852e8c6e406c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<div id=\"formu\" class=\"format\">
  <div class=\"row format-add color-black\">
    <div class=\"col-md-2\"><h5>Card</h5></div>
    <div class=\"col-md-2\"><h5>Title</h5></div>
    <div class=\"col-md-5\"><h5>Description</h5></div>
    <div class=\"col-md-1\"><h5>Add image</h5></div>
    <div class=\"col-md-1\"><h5>Edit article</h5></div>
    <div class=\"col-md-1\"><h5>Delete article</h5></div>
  </div>
  ";
        // line 12
        if ((twig_length_filter($this->env, $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "articles", array())) == 0)) {
            // line 13
            echo "  <div class=\"row\">
    <div class=\"col-md-4 colmd-offset-1\">
      <h3>Any articles in this card</h3>
    </div>
  </div>
  ";
        } else {
            // line 19
            echo "  ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "articles", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
                // line 20
                echo "    <div class=\"row\">
      <div class=\"col-md-2\"><h5>";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["article"], "idCard", array()), "title", array()), "html", null, true);
                echo "</h5></div>
      <div class=\"col-md-2\"><h5>";
                // line 22
                echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "title", array()), "html", null, true);
                echo "</h5></div>
\t    <div class=\"col-md-5\"><h5>";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "content", array()), "html", null, true);
                echo "</h5></div>
      <div class=\"col-md-1\">
        <a href=\"/image/";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/icons/add.png"), "html", null, true);
                echo "\" alt=\"add\" tittle=\"add\" width=\"30\"></a>
      </div>
      <div class=\"col-md-1\">
        <a href=\"/editarticle/";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/icons/edit.png"), "html", null, true);
                echo "\" alt=\"edit\" tittle=\"edit\" width=\"30\"></a>
      </div>
      <div class=\"col-md-1\">
        <a href=\"/deletearticle/";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
                echo "\"  onclick=\"return confirm('Are you sure delete article ?')\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/icons/delete.png"), "html", null, true);
                echo "\" alt=\"delete\" tittle=\"delete\" width=\"30\"></a>
      </div>
    </div>
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 35
            echo "  ";
        }
        // line 36
        echo "  <div class=\"row\">
    <div class=\"col-md-1 col-md-offset-1\">
      <a href=\"/portfoli/";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "idPortfoli", array()), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">Portfoli</a>
    </div>
    <div class=\"col-md-1 col-md-offset-1\">
      <a href=\"/cards/";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "idPortfoli", array()), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">List cards</a>
    </div>
    <div class=\"col-md-1 col-md-offset-1\">
      <a href=\"/formarticle/";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">Create Article</a>
    </div>
  </div>
  <hr>
</div>
";
        
        $__internal_9ba7fabb347c29d22beab3009e99853bbc464f0d76004a038a6c852e8c6e406c->leave($__internal_9ba7fabb347c29d22beab3009e99853bbc464f0d76004a038a6c852e8c6e406c_prof);

    }

    public function getTemplateName()
    {
        return "default/articles/list_article.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 44,  123 => 41,  117 => 38,  113 => 36,  110 => 35,  98 => 31,  90 => 28,  82 => 25,  77 => 23,  73 => 22,  69 => 21,  66 => 20,  61 => 19,  53 => 13,  51 => 12,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}
{% block body %}
<div id=\"formu\" class=\"format\">
  <div class=\"row format-add color-black\">
    <div class=\"col-md-2\"><h5>Card</h5></div>
    <div class=\"col-md-2\"><h5>Title</h5></div>
    <div class=\"col-md-5\"><h5>Description</h5></div>
    <div class=\"col-md-1\"><h5>Add image</h5></div>
    <div class=\"col-md-1\"><h5>Edit article</h5></div>
    <div class=\"col-md-1\"><h5>Delete article</h5></div>
  </div>
  {% if card.articles|length==0%}
  <div class=\"row\">
    <div class=\"col-md-4 colmd-offset-1\">
      <h3>Any articles in this card</h3>
    </div>
  </div>
  {% else %}
  {% for article in card.articles %}
    <div class=\"row\">
      <div class=\"col-md-2\"><h5>{{(article.idCard.title)}}</h5></div>
      <div class=\"col-md-2\"><h5>{{(article.title)}}</h5></div>
\t    <div class=\"col-md-5\"><h5>{{(article.content)}}</h5></div>
      <div class=\"col-md-1\">
        <a href=\"/image/{{(article.id)}}\"><img src=\"{{ asset(\"img/icons/add.png\")}}\" alt=\"add\" tittle=\"add\" width=\"30\"></a>
      </div>
      <div class=\"col-md-1\">
        <a href=\"/editarticle/{{(article.id)}}\"><img src=\"{{ asset(\"img/icons/edit.png\")}}\" alt=\"edit\" tittle=\"edit\" width=\"30\"></a>
      </div>
      <div class=\"col-md-1\">
        <a href=\"/deletearticle/{{(article.id)}}\"  onclick=\"return confirm('Are you sure delete article ?')\"><img src=\"{{ asset(\"img/icons/delete.png\")}}\" alt=\"delete\" tittle=\"delete\" width=\"30\"></a>
      </div>
    </div>
  {% endfor %}
  {% endif %}
  <div class=\"row\">
    <div class=\"col-md-1 col-md-offset-1\">
      <a href=\"/portfoli/{{(card.idPortfoli.id)}}\" class=\"btn btn-primary btn-md\">Portfoli</a>
    </div>
    <div class=\"col-md-1 col-md-offset-1\">
      <a href=\"/cards/{{(card.idPortfoli.id)}}\" class=\"btn btn-primary btn-md\">List cards</a>
    </div>
    <div class=\"col-md-1 col-md-offset-1\">
      <a href=\"/formarticle/{{(card.id)}}\" class=\"btn btn-primary btn-md\">Create Article</a>
    </div>
  </div>
  <hr>
</div>
{% endblock %}
", "default/articles/list_article.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/articles/list_article.html.twig");
    }
}
