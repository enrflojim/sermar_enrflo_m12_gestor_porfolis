<?php

/* default/varios/contactme.html.twig */
class __TwigTemplate_a75fdfb7a1af1762b43d8cd140ce80b4d43bd404cf750527d048720cb9b2f353 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_all.html.twig", "default/varios/contactme.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_all.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_657b8bf33eb005eadda9bda7e642a7d11a01db9b59d511c51022b02018c86e8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_657b8bf33eb005eadda9bda7e642a7d11a01db9b59d511c51022b02018c86e8b->enter($__internal_657b8bf33eb005eadda9bda7e642a7d11a01db9b59d511c51022b02018c86e8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/varios/contactme.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_657b8bf33eb005eadda9bda7e642a7d11a01db9b59d511c51022b02018c86e8b->leave($__internal_657b8bf33eb005eadda9bda7e642a7d11a01db9b59d511c51022b02018c86e8b_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_2c22289956a2e4b31caa618fbb7040d1411089151fb3b132795c3999b75a7c25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c22289956a2e4b31caa618fbb7040d1411089151fb3b132795c3999b75a7c25->enter($__internal_2c22289956a2e4b31caa618fbb7040d1411089151fb3b132795c3999b75a7c25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-10 col-md-offset-1 format-forms color-azul\">
              <div class=\"row\">
                <div class=\"col-md-11\">
                  <h1>";
        // line 8
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</h1>
                  <p>You can contact me using your email or this webpage.</p>
                  <fieldset>
                      <legend>My data:</legend>
                      Full name: ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "name", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "surname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "lastname", array()), "html", null, true);
        echo "<br/>
                      Date of birth: ";
        // line 13
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "date", array()), "Y-m-d"), "html", null, true);
        echo "<br/>
                      Phone number: ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "phone", array()), "html", null, true);
        echo "<br/>
                      Email: ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "email", array()), "html", null, true);
        echo "<br/>
                      City: ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "city", array()), "html", null, true);
        echo "<br/>
                      Country ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "country", array()), "html", null, true);
        echo "<br/>
                  </fieldset>
                  <hr>
                  ";
        // line 20
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
                  ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
                </div>
              </div>
            </div>
        </div>
    </div>
";
        
        $__internal_2c22289956a2e4b31caa618fbb7040d1411089151fb3b132795c3999b75a7c25->leave($__internal_2c22289956a2e4b31caa618fbb7040d1411089151fb3b132795c3999b75a7c25_prof);

    }

    public function getTemplateName()
    {
        return "default/varios/contactme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 21,  84 => 20,  78 => 17,  74 => 16,  70 => 15,  66 => 14,  62 => 13,  54 => 12,  47 => 8,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_all.html.twig' %}
{% block body %}
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-10 col-md-offset-1 format-forms color-azul\">
              <div class=\"row\">
                <div class=\"col-md-11\">
                  <h1>{{title}}</h1>
                  <p>You can contact me using your email or this webpage.</p>
                  <fieldset>
                      <legend>My data:</legend>
                      Full name: {{portfoli.idUser.name}} {{portfoli.idUser.surname}} {{portfoli.idUser.lastname}}<br/>
                      Date of birth: {{portfoli.idUser.date|date('Y-m-d')}}<br/>
                      Phone number: {{portfoli.idUser.phone}}<br/>
                      Email: {{portfoli.idUser.email}}<br/>
                      City: {{portfoli.idUser.city}}<br/>
                      Country {{portfoli.idUser.country}}<br/>
                  </fieldset>
                  <hr>
                  {{ form_start(form) }}
                  {{ form_end(form) }}
                </div>
              </div>
            </div>
        </div>
    </div>
{% endblock %}
", "default/varios/contactme.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/varios/contactme.html.twig");
    }
}
