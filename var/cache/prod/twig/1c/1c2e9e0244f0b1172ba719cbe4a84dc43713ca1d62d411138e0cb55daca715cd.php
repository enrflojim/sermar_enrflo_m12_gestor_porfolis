<?php

/* default/cards/create_card.html.twig */
class __TwigTemplate_8fd43ee833af5b17833eaa8d2feaa97b23d420dc33590c8a66fdb41668c19470 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/cards/create_card.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d1a9caea7c686ebc5da5f516e66a254a61f4c7798861159e13d69247fecdf55 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d1a9caea7c686ebc5da5f516e66a254a61f4c7798861159e13d69247fecdf55->enter($__internal_2d1a9caea7c686ebc5da5f516e66a254a61f4c7798861159e13d69247fecdf55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/cards/create_card.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2d1a9caea7c686ebc5da5f516e66a254a61f4c7798861159e13d69247fecdf55->leave($__internal_2d1a9caea7c686ebc5da5f516e66a254a61f4c7798861159e13d69247fecdf55_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_8bdd86cac199ec59a90e0be5712f349a6d4d26f8a325185670ba72c9c8f8a4eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8bdd86cac199ec59a90e0be5712f349a6d4d26f8a325185670ba72c9c8f8a4eb->enter($__internal_8bdd86cac199ec59a90e0be5712f349a6d4d26f8a325185670ba72c9c8f8a4eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "\t<div id=\"formu\" class=\"row\">
        \t<div class=\"col-md-8 col-md-offset-2 format-forms color-azul\">
\t\t\t<div class=\"format-form\">
\t\t\t";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
\t\t\t<fieldset>
\t\t\t\t<legend>";
        // line 8
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</legend>
\t\t\t\t<div class=\"row line\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "title", array()), 'row');
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row line\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t";
        // line 17
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "subCategories", array()), 'row');
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t</div>
\t\t\t\t";
        // line 21
        if (($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "isInformatic", array()) == 1)) {
            // line 22
            echo "\t\t\t\t<div class=\"row line\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1 font-black\">
\t\t\t\t\t\t<h5><i>Para introducir codigo utiliza las etiquetas [code] al principio y al final</i></h5>
\t\t\t\t\t</div>
\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t";
        }
        // line 29
        echo "\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t";
        // line 31
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "description", array()), 'row');
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row line\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t";
        // line 37
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "link", array()), 'row');
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<hr>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-1 col-md-offset-1\">
\t\t\t\t\t\t<a href=\"/portfoli/";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
        echo "\"  class=\"btn btn-primary btn-md\" >Portfoli</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-1 col-md-offset-1\">
\t\t\t\t\t\t<a href=\"/cards/";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
        echo "\"  class=\"btn btn-primary btn-md\" >List cards</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-1 col-md-offset-1\">
\t\t\t\t\t\t<input type=\"submit\" class=\"btn btn-primary btn-md\" value=\"";
        // line 49
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</fieldset>
\t\t\t";
        // line 53
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
\t\t\t</div>
\t\t<div>
\t</div>
";
        
        $__internal_8bdd86cac199ec59a90e0be5712f349a6d4d26f8a325185670ba72c9c8f8a4eb->leave($__internal_8bdd86cac199ec59a90e0be5712f349a6d4d26f8a325185670ba72c9c8f8a4eb_prof);

    }

    public function getTemplateName()
    {
        return "default/cards/create_card.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 53,  117 => 49,  111 => 46,  105 => 43,  96 => 37,  87 => 31,  83 => 29,  74 => 22,  72 => 21,  65 => 17,  56 => 11,  50 => 8,  45 => 6,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}
{% block body %}
\t<div id=\"formu\" class=\"row\">
        \t<div class=\"col-md-8 col-md-offset-2 format-forms color-azul\">
\t\t\t<div class=\"format-form\">
\t\t\t{{ form_start(form) }}
\t\t\t<fieldset>
\t\t\t\t<legend>{{(title)}}</legend>
\t\t\t\t<div class=\"row line\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t{{ form_row(form.title) }}
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row line\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t{{ form_row(form.subCategories) }}
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t</div>
\t\t\t\t{% if portfoli.isInformatic == 1 %}
\t\t\t\t<div class=\"row line\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1 font-black\">
\t\t\t\t\t\t<h5><i>Para introducir codigo utiliza las etiquetas [code] al principio y al final</i></h5>
\t\t\t\t\t</div>
\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t{% endif %}
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t{{ form_row(form.description) }}
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row line\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t{{ form_row(form.link) }}
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<hr>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-1 col-md-offset-1\">
\t\t\t\t\t\t<a href=\"/portfoli/{{(portfoli.id)}}\"  class=\"btn btn-primary btn-md\" >Portfoli</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-1 col-md-offset-1\">
\t\t\t\t\t\t<a href=\"/cards/{{(portfoli.id)}}\"  class=\"btn btn-primary btn-md\" >List cards</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-1 col-md-offset-1\">
\t\t\t\t\t\t<input type=\"submit\" class=\"btn btn-primary btn-md\" value=\"{{(title)}}\"/>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</fieldset>
\t\t\t{{ form_end(form) }}
\t\t\t</div>
\t\t<div>
\t</div>
{% endblock %}
", "default/cards/create_card.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/cards/create_card.html.twig");
    }
}
