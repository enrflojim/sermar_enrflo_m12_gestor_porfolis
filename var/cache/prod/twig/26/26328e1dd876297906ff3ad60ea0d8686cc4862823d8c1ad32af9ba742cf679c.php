<?php

/* default/cardTemplates/allCard.html.twig */
class __TwigTemplate_0c489da25e6fbf885401a0f293c0fcddd1fc399b6c31bac03cc82d417a0373f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/cardTemplates/allCard.html.twig", 1);
        $this->blocks = array(
            'javascript' => array($this, 'block_javascript'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a38fd0257c099baeeb12b211cf6827dbb34bc8a7d7cd84bd3cf9c0b925888884 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a38fd0257c099baeeb12b211cf6827dbb34bc8a7d7cd84bd3cf9c0b925888884->enter($__internal_a38fd0257c099baeeb12b211cf6827dbb34bc8a7d7cd84bd3cf9c0b925888884_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/cardTemplates/allCard.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a38fd0257c099baeeb12b211cf6827dbb34bc8a7d7cd84bd3cf9c0b925888884->leave($__internal_a38fd0257c099baeeb12b211cf6827dbb34bc8a7d7cd84bd3cf9c0b925888884_prof);

    }

    // line 3
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_56aabb024cab891bb9e00c6d3b74cdcfc04e481c6ce4860fbf5a67b458d18ae5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56aabb024cab891bb9e00c6d3b74cdcfc04e481c6ce4860fbf5a67b458d18ae5->enter($__internal_56aabb024cab891bb9e00c6d3b74cdcfc04e481c6ce4860fbf5a67b458d18ae5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 4
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/slider.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_56aabb024cab891bb9e00c6d3b74cdcfc04e481c6ce4860fbf5a67b458d18ae5->leave($__internal_56aabb024cab891bb9e00c6d3b74cdcfc04e481c6ce4860fbf5a67b458d18ae5_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_4fcab23da029565dbd6a22b0e3fca46f4612d91e7ab7043e33adbd493e083a91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fcab23da029565dbd6a22b0e3fca46f4612d91e7ab7043e33adbd493e083a91->enter($__internal_4fcab23da029565dbd6a22b0e3fca46f4612d91e7ab7043e33adbd493e083a91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <div class=\"row\">
        <div class=\"col-md-2 col-md-offset-1\">
            <h3>Cards</h3>
            <div class=\"panel-group\" id=\"acordeon\">
                ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sub"] ?? $this->getContext($context, "sub")));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 13
            echo "                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#acordeon\" href=\"#collapse";
            // line 16
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "</a>
                            </h4>
                        </div>
                        <div id=\"collapse";
            // line 19
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "\" class=\"panel-collapse collapse\">
                            ";
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "cards", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
                // line 21
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["card"], "subCategories", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["subcategories"]) {
                    // line 22
                    echo "                                    ";
                    if (($this->getAttribute($context["subcategories"], "nameSubcategory", array()) == $context["s"])) {
                        // line 23
                        echo "                                        <div class=\"panel-body\"><a href=\"/card/";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "id", array()), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "title", array()), "html", null, true);
                        echo "</a></div>
                                        ";
                    }
                    // line 25
                    echo "                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcategories'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 26
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "            </div>
        </div>
        <div class=\"col-md-8\">
            <div class=\"row\">
                <div class=\"col-md-12 format-forms color-azul\">
                    <div class=\"row\">
                        <div class=\"col-md-11\">
                            <h1>";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "title", array()), "html", null, true);
        echo "</h1>
                            ";
        // line 38
        echo $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "description", array());
        echo "
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-10 col-md-offset-1\">
                            ";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "articles", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 44
            echo "                                <article>
                                    <h3>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "title", array()), "html", null, true);
            echo "</h3>
                                    <p>";
            // line 46
            echo $this->getAttribute($context["article"], "content", array());
            echo "</p>
                                    <div class=\"row\">
                                        <div class=\"col-md-8 col-md-offset-2\">
                                            <div id=\"myCarousel";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
            echo "\" class=\"carousel slide\" data-ride=\"carousel\">
                                                <!-- Wrapper for slides -->
                                                <div class=\"carousel-inner\">
                                                    <!-- Items -->
                                                    ";
            // line 53
            $context["i"] = 0;
            // line 54
            echo "                                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["article"], "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 55
                echo "                                                        ";
                if ((($context["i"] ?? $this->getContext($context, "i")) == 0)) {
                    // line 56
                    echo "                                                            <div class=\"item active\">
                                                            ";
                } else {
                    // line 58
                    echo "                                                                <div class=\"item\">
                                                                ";
                }
                // line 60
                echo "                                                                <img width=\"500\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/articles/" . $this->getAttribute($context["image"], "image", array()))), "html", null, true);
                echo "\">
                                                                <div class=\"carousel-caption\">
                                                                    <h3>";
                // line 62
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo "</h3>
                                                                    <a hred=\"#\" class=\"btn btn-primary btn-xs\" data-toggle=\"modal\" data-target=\"#myModal";
                // line 63
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                echo "\">Read me</a>
                                                                </div>
                                                            </div>
                                                            ";
                // line 66
                $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
                // line 67
                echo "                                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo "                                                        <!-- End Items -->
                                                    </div>
                                                    <ul class=\"nav nav-pills nav-justified\">
                                                        ";
            // line 71
            $context["i"] = 0;
            // line 72
            echo "                                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["article"], "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 73
                echo "                                                            ";
                if ((($context["i"] ?? $this->getContext($context, "i")) == 0)) {
                    // line 74
                    echo "                                                                <li data-target=\"#myCarousel";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
                    echo "\" data-slide-to=\"";
                    echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                    echo "\" class=\"active\">
                                                                ";
                } else {
                    // line 76
                    echo "                                                                <li data-target=\"#myCarousel";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
                    echo "\" data-slide-to=\"";
                    echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                    echo "\">
                                                                ";
                }
                // line 78
                echo "                                                                <a href=\"#\">
                                                                    <small><img width=\"25\" class=\"barra\" src=\"";
                // line 79
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/articles/" . $this->getAttribute($context["image"], "image", array()))), "html", null, true);
                echo "\"></small>
                                                                </a>
                                                            </li>
                                                            ";
                // line 82
                $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
                // line 83
                echo "                                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 84
            echo "                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                </article>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 91
        echo "                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    ";
        // line 98
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "articles", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 99
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["article"], "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 100
                echo "    <div class=\"modal fade\" id=\"myModal";
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                echo "\" role=\"dialog\">
        <div class=\"modal-dialog tam-modal\">

            <!-- Modal content-->
            <div class=\"modal-content\">
                <div class=\"modal-header centro\">
                    <h2 class=\"modal-title\">";
                // line 106
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo "</h2>
                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-8\">
                            <img width=\"700\" src=\"";
                // line 111
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/articles/" . $this->getAttribute($context["image"], "image", array()))), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo "\"
                                 alt=\"";
                // line 112
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo " not found\">
                        </div>
                        <div class=\"col-md-4\">
                            ";
                // line 115
                echo $this->getAttribute($context["image"], "description", array());
                echo "
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 124
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_4fcab23da029565dbd6a22b0e3fca46f4612d91e7ab7043e33adbd493e083a91->leave($__internal_4fcab23da029565dbd6a22b0e3fca46f4612d91e7ab7043e33adbd493e083a91_prof);

    }

    public function getTemplateName()
    {
        return "default/cardTemplates/allCard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  334 => 124,  319 => 115,  313 => 112,  307 => 111,  299 => 106,  289 => 100,  284 => 99,  280 => 98,  271 => 91,  259 => 84,  253 => 83,  251 => 82,  245 => 79,  242 => 78,  234 => 76,  226 => 74,  223 => 73,  218 => 72,  216 => 71,  211 => 68,  205 => 67,  203 => 66,  197 => 63,  193 => 62,  187 => 60,  183 => 58,  179 => 56,  176 => 55,  171 => 54,  169 => 53,  162 => 49,  156 => 46,  152 => 45,  149 => 44,  145 => 43,  137 => 38,  133 => 37,  124 => 30,  116 => 27,  110 => 26,  104 => 25,  96 => 23,  93 => 22,  88 => 21,  84 => 20,  80 => 19,  72 => 16,  67 => 13,  63 => 12,  57 => 8,  51 => 7,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}

{% block javascript %}
    <script src=\"{{ asset('js/slider.js')}}\"></script>
{% endblock %}

{% block body %}
    <div class=\"row\">
        <div class=\"col-md-2 col-md-offset-1\">
            <h3>Cards</h3>
            <div class=\"panel-group\" id=\"acordeon\">
                {% for s in sub %}
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#acordeon\" href=\"#collapse{{(s)}}\">{{(s)}}</a>
                            </h4>
                        </div>
                        <div id=\"collapse{{(s)}}\" class=\"panel-collapse collapse\">
                            {% for card in portfoli.cards %}
                                {% for subcategories in card.subCategories %}
                                    {% if subcategories.nameSubcategory == s %}
                                        <div class=\"panel-body\"><a href=\"/card/{{(card.id)}}\">{{(card.title)}}</a></div>
                                        {% endif %}
                                    {% endfor %}
                                {% endfor %}
                        </div>
                    </div>
                {% endfor %}
            </div>
        </div>
        <div class=\"col-md-8\">
            <div class=\"row\">
                <div class=\"col-md-12 format-forms color-azul\">
                    <div class=\"row\">
                        <div class=\"col-md-11\">
                            <h1>{{card.title}}</h1>
                            {{card.description|raw}}
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-10 col-md-offset-1\">
                            {% for article in card.articles %}
                                <article>
                                    <h3>{{article.title}}</h3>
                                    <p>{{article.content|raw}}</p>
                                    <div class=\"row\">
                                        <div class=\"col-md-8 col-md-offset-2\">
                                            <div id=\"myCarousel{{(article.id)}}\" class=\"carousel slide\" data-ride=\"carousel\">
                                                <!-- Wrapper for slides -->
                                                <div class=\"carousel-inner\">
                                                    <!-- Items -->
                                                    {% set i=0 %}
                                                    {% for image in article.images %}
                                                        {% if i==0 %}
                                                            <div class=\"item active\">
                                                            {% else %}
                                                                <div class=\"item\">
                                                                {% endif %}
                                                                <img width=\"500\" src=\"{{asset('img/articles/'~image.image)}}\">
                                                                <div class=\"carousel-caption\">
                                                                    <h3>{{(image.title)}}</h3>
                                                                    <a hred=\"#\" class=\"btn btn-primary btn-xs\" data-toggle=\"modal\" data-target=\"#myModal{{(image.id)}}\">Read me</a>
                                                                </div>
                                                            </div>
                                                            {% set i=i+1 %}
                                                        {% endfor %}
                                                        <!-- End Items -->
                                                    </div>
                                                    <ul class=\"nav nav-pills nav-justified\">
                                                        {% set i=0 %}
                                                        {% for image in article.images %}
                                                            {% if i==0 %}
                                                                <li data-target=\"#myCarousel{{(article.id)}}\" data-slide-to=\"{{(i)}}\" class=\"active\">
                                                                {% else %}
                                                                <li data-target=\"#myCarousel{{(article.id)}}\" data-slide-to=\"{{(i)}}\">
                                                                {% endif %}
                                                                <a href=\"#\">
                                                                    <small><img width=\"25\" class=\"barra\" src=\"{{asset('img/articles/'~image.image)}}\"></small>
                                                                </a>
                                                            </li>
                                                            {% set i=i+1 %}
                                                        {% endfor %}
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                </article>
                            {% endfor %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    {% for article in card.articles %}
    {% for image in article.images %}
    <div class=\"modal fade\" id=\"myModal{{(image.id)}}\" role=\"dialog\">
        <div class=\"modal-dialog tam-modal\">

            <!-- Modal content-->
            <div class=\"modal-content\">
                <div class=\"modal-header centro\">
                    <h2 class=\"modal-title\">{{(image.title)}}</h2>
                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-8\">
                            <img width=\"700\" src=\"{{ asset('img/articles/'~image.image)}}\" title=\"{{(image.title)}}\"
                                 alt=\"{{(image.title)}} not found\">
                        </div>
                        <div class=\"col-md-4\">
                            {{(image.description|raw)}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    {% endfor %}
    {% endfor %}
{% endblock %}
", "default/cardTemplates/allCard.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/cardTemplates/allCard.html.twig");
    }
}
