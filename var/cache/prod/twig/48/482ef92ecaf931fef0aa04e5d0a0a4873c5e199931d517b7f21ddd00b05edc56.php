<?php

/* default/header/header_portfoli.html.twig */
class __TwigTemplate_ae5f25d55c422c192e024b23bf35fb77ebabaf096bef946f759fe300d48defdc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/header/header_portfoli.html.twig", 1);
        $this->blocks = array(
            'jumbotron' => array($this, 'block_jumbotron'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3a9a710305426dd0c48711991e750465f9d3f1068f4d12086ddf9052422226b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a9a710305426dd0c48711991e750465f9d3f1068f4d12086ddf9052422226b9->enter($__internal_3a9a710305426dd0c48711991e750465f9d3f1068f4d12086ddf9052422226b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/header/header_portfoli.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3a9a710305426dd0c48711991e750465f9d3f1068f4d12086ddf9052422226b9->leave($__internal_3a9a710305426dd0c48711991e750465f9d3f1068f4d12086ddf9052422226b9_prof);

    }

    // line 2
    public function block_jumbotron($context, array $blocks = array())
    {
        $__internal_0fcff752ab371ec0abd2c839ba7218f3e576412093dbc3c2bdd9b1338306347c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0fcff752ab371ec0abd2c839ba7218f3e576412093dbc3c2bdd9b1338306347c->enter($__internal_0fcff752ab371ec0abd2c839ba7218f3e576412093dbc3c2bdd9b1338306347c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "jumbotron"));

        // line 3
        echo "    <!-- Jumbotron -->
    <div class=\"jumbotron color-azul\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-8 col-md-offset-2 centro\">
                    <h1 class=\"cg\">";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "title", array()), "html", null, true);
        echo "</h1>
                </div>
            </div>
            ";
        // line 11
        if ((($context["rute"] ?? $this->getContext($context, "rute")) == "user")) {
            // line 12
            echo "                <div class=\"row\">
                    <div class=\"col-md-2 col-md-offset-1 centro\">
                        <a href=\"/portfoli/";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary\">Home portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/cards/";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary div-tam\">List Card</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/editportfoli/";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary\">Edit portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/deleteportfoli/";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" onclick=\"return confirm('Are you sure delete portfoli ?')\" class=\"btn btn-primary\">Delete Portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/contactme/";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary\">contact me</a>
                    </div>
                </div>
            ";
        } else {
            // line 30
            echo "                <div class=\"row\">
                    <div class=\"col-md-2 col-md-offset-4 centro\">
                        <a href=\"/portfoli/";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary\">Home portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/contactme/";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary\">contact me</a>
                    </div>
                </div>
            ";
        }
        // line 39
        echo "        </div>
    </div>
    <!-- End Jumbotron -->
";
        
        $__internal_0fcff752ab371ec0abd2c839ba7218f3e576412093dbc3c2bdd9b1338306347c->leave($__internal_0fcff752ab371ec0abd2c839ba7218f3e576412093dbc3c2bdd9b1338306347c_prof);

    }

    // line 43
    public function block_body($context, array $blocks = array())
    {
        $__internal_43cefd0193bcfed22a890b058c560321ab491db1faca177e210b8b01dc569f0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43cefd0193bcfed22a890b058c560321ab491db1faca177e210b8b01dc569f0d->enter($__internal_43cefd0193bcfed22a890b058c560321ab491db1faca177e210b8b01dc569f0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_43cefd0193bcfed22a890b058c560321ab491db1faca177e210b8b01dc569f0d->leave($__internal_43cefd0193bcfed22a890b058c560321ab491db1faca177e210b8b01dc569f0d_prof);

    }

    public function getTemplateName()
    {
        return "default/header/header_portfoli.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 43,  108 => 39,  101 => 35,  95 => 32,  91 => 30,  84 => 26,  78 => 23,  72 => 20,  66 => 17,  60 => 14,  56 => 12,  54 => 11,  48 => 8,  41 => 3,  35 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block jumbotron %}
    <!-- Jumbotron -->
    <div class=\"jumbotron color-azul\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-8 col-md-offset-2 centro\">
                    <h1 class=\"cg\">{{(portfoli.title)}}</h1>
                </div>
            </div>
            {% if rute==\"user\" %}
                <div class=\"row\">
                    <div class=\"col-md-2 col-md-offset-1 centro\">
                        <a href=\"/portfoli/{{(portfoli.id)}}\" class=\"btn btn-primary\">Home portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/cards/{{(portfoli.id)}}\" class=\"btn btn-primary div-tam\">List Card</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/editportfoli/{{(portfoli.id)}}\" class=\"btn btn-primary\">Edit portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/deleteportfoli/{{(portfoli.id)}}\" onclick=\"return confirm('Are you sure delete portfoli ?')\" class=\"btn btn-primary\">Delete Portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/contactme/{{(portfoli.id)}}\" class=\"btn btn-primary\">contact me</a>
                    </div>
                </div>
            {% else %}
                <div class=\"row\">
                    <div class=\"col-md-2 col-md-offset-4 centro\">
                        <a href=\"/portfoli/{{(portfoli.id)}}\" class=\"btn btn-primary\">Home portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/contactme/{{(portfoli.id)}}\" class=\"btn btn-primary\">contact me</a>
                    </div>
                </div>
            {% endif %}
        </div>
    </div>
    <!-- End Jumbotron -->
{% endblock %}
{% block body %}{% endblock %}
", "default/header/header_portfoli.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/header/header_portfoli.html.twig");
    }
}
