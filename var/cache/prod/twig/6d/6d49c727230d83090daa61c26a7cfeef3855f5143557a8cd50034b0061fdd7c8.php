<?php

/* default/articles/create_article.html.twig */
class __TwigTemplate_cf2ae9dc0dba17b00cb85270f2f7eae806d2fe352c0afe84d4aae35fc8467e07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/articles/create_article.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5e7c47755c634ab1cc23ad3f8e7233dd119638415c38813cd99ff9637c16c5f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e7c47755c634ab1cc23ad3f8e7233dd119638415c38813cd99ff9637c16c5f7->enter($__internal_5e7c47755c634ab1cc23ad3f8e7233dd119638415c38813cd99ff9637c16c5f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/articles/create_article.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5e7c47755c634ab1cc23ad3f8e7233dd119638415c38813cd99ff9637c16c5f7->leave($__internal_5e7c47755c634ab1cc23ad3f8e7233dd119638415c38813cd99ff9637c16c5f7_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_b41c8337cec650d8c7c77980447ff563029c09a6cc29146227c64f714751ddab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b41c8337cec650d8c7c77980447ff563029c09a6cc29146227c64f714751ddab->enter($__internal_b41c8337cec650d8c7c77980447ff563029c09a6cc29146227c64f714751ddab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "\t<div id=\"formu\" class=\"row\">
        \t<div class=\"col-md-8 col-md-offset-2 format-forms color-azul\">
\t\t\t<div class=\"format-form\">
\t\t\t";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
\t\t\t<fieldset>
\t\t\t\t<legend>";
        // line 8
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</legend>
\t\t\t\t<div class=\"row line\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "title", array()), 'row');
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t";
        // line 17
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "content", array()), 'row');
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t</div>
\t\t\t\t<hr>
\t\t\t\t<div class=\"row\">
\t\t\t    <div class=\"col-md-1 col-md-offset-1\">
\t\t\t      <a href=\"/portfoli/";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "idPortfoli", array()), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">Portfoli</a>
\t\t\t    </div>
\t\t\t    <div class=\"col-md-1 col-md-offset-1\">
\t\t\t      <a href=\"/cards/";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "idPortfoli", array()), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">List cards</a>
\t\t\t    </div>
\t\t\t\t\t<div class=\"col-md-1 col-md-offset-1\">
\t\t\t      <a href=\"/articles/";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">List article</a>
\t\t\t    </div>
\t\t\t\t\t<div class=\"col-md-1 col-md-offset-1\">
\t\t\t      <input type=\"submit\" class=\"btn btn-primary btn-md\" value=\"";
        // line 33
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "\"/>
\t\t\t    </div>
\t\t\t  </div>
\t\t\t</fieldset>
\t\t\t";
        // line 37
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
\t\t\t</div>
\t\t<div>
\t</div>
";
        
        $__internal_b41c8337cec650d8c7c77980447ff563029c09a6cc29146227c64f714751ddab->leave($__internal_b41c8337cec650d8c7c77980447ff563029c09a6cc29146227c64f714751ddab_prof);

    }

    public function getTemplateName()
    {
        return "default/articles/create_article.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 37,  93 => 33,  87 => 30,  81 => 27,  75 => 24,  65 => 17,  56 => 11,  50 => 8,  45 => 6,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}
{% block body %}
\t<div id=\"formu\" class=\"row\">
        \t<div class=\"col-md-8 col-md-offset-2 format-forms color-azul\">
\t\t\t<div class=\"format-form\">
\t\t\t{{ form_start(form) }}
\t\t\t<fieldset>
\t\t\t\t<legend>{{(title)}}</legend>
\t\t\t\t<div class=\"row line\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t{{ form_row(form.title) }}
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-1\">
\t\t\t\t\t\t{{ form_row(form.content) }}
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t</div>
\t\t\t\t<hr>
\t\t\t\t<div class=\"row\">
\t\t\t    <div class=\"col-md-1 col-md-offset-1\">
\t\t\t      <a href=\"/portfoli/{{(card.idPortfoli.id)}}\" class=\"btn btn-primary btn-md\">Portfoli</a>
\t\t\t    </div>
\t\t\t    <div class=\"col-md-1 col-md-offset-1\">
\t\t\t      <a href=\"/cards/{{(card.idPortfoli.id)}}\" class=\"btn btn-primary btn-md\">List cards</a>
\t\t\t    </div>
\t\t\t\t\t<div class=\"col-md-1 col-md-offset-1\">
\t\t\t      <a href=\"/articles/{{(card.id)}}\" class=\"btn btn-primary btn-md\">List article</a>
\t\t\t    </div>
\t\t\t\t\t<div class=\"col-md-1 col-md-offset-1\">
\t\t\t      <input type=\"submit\" class=\"btn btn-primary btn-md\" value=\"{{(title)}}\"/>
\t\t\t    </div>
\t\t\t  </div>
\t\t\t</fieldset>
\t\t\t{{ form_end(form) }}
\t\t\t</div>
\t\t<div>
\t</div>
{% endblock %}
", "default/articles/create_article.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/articles/create_article.html.twig");
    }
}
