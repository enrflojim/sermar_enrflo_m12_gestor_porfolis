<?php

/* default/portfolis/create_portfoli.html.twig */
class __TwigTemplate_e6489638af30f4ffd0bab6f397f5fe6cbcf131f7591a33852bdbdb9976389131 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_all.html.twig", "default/portfolis/create_portfoli.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_all.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_217b60f2a7db43199bbefa9aec55c81aa4d5c01060ad3c079f6df022e24f96d8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_217b60f2a7db43199bbefa9aec55c81aa4d5c01060ad3c079f6df022e24f96d8->enter($__internal_217b60f2a7db43199bbefa9aec55c81aa4d5c01060ad3c079f6df022e24f96d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/portfolis/create_portfoli.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_217b60f2a7db43199bbefa9aec55c81aa4d5c01060ad3c079f6df022e24f96d8->leave($__internal_217b60f2a7db43199bbefa9aec55c81aa4d5c01060ad3c079f6df022e24f96d8_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_d422fb7d89aef922c6859b5f5ede00cfa3db4a39a00d1bb73a485307004f7bb1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d422fb7d89aef922c6859b5f5ede00cfa3db4a39a00d1bb73a485307004f7bb1->enter($__internal_d422fb7d89aef922c6859b5f5ede00cfa3db4a39a00d1bb73a485307004f7bb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<div id=\"formu\" class=\"row\">
    <div class=\"col-md-6 col-md-offset-3 format-forms color-azul\">
\t\t\t";
        // line 5
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
\t\t\t<fieldset>
\t\t\t\t<legend>";
        // line 7
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</legend>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t  \t\t<div class=\"col-md-9 col-md-offset-1 mostrar\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/images/asd.jpg"), "html", null, true);
        echo "\" width=\"148\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t  \t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<label for=\"form_imgPortfoli\" class=\"btn btn-default largo\">
\t\t\t\t\t\t\t\t\t";
        // line 18
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "imgPortfoli", array()), 'row');
        echo "
\t\t\t\t\t\t\t\t\tAdd image portfoli
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t\t\t";
        // line 27
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "title", array()), 'row');
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-1\">*</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-5\">
\t\t\t\t\t\t\t\t";
        // line 33
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "isPremium", array()), 'row');
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        // line 35
        if (twig_test_empty(($context["free"] ?? $this->getContext($context, "free")))) {
            // line 36
            echo "\t\t\t\t\t\t\t<div class=\"col-md-2\">*</div>
                    \t\t";
        } else {
            // line 38
            echo "\t\t\t\t\t\t\t<div class=\"col-md-2 windowform\">* <span>Only one free portfoli</span></div>
                    \t\t";
        }
        // line 40
        echo "\t\t\t\t\t\t</div>
            <div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-5\">
\t\t\t\t\t\t\t\t";
        // line 43
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "isInformatic", array()), 'row');
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t\t";
        // line 48
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "idCategory", array()), 'row');
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</fieldset>
\t\t\t";
        // line 55
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
\t\t<div>
\t</div>
";
        
        $__internal_d422fb7d89aef922c6859b5f5ede00cfa3db4a39a00d1bb73a485307004f7bb1->leave($__internal_d422fb7d89aef922c6859b5f5ede00cfa3db4a39a00d1bb73a485307004f7bb1_prof);

    }

    public function getTemplateName()
    {
        return "default/portfolis/create_portfoli.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 55,  115 => 48,  107 => 43,  102 => 40,  98 => 38,  94 => 36,  92 => 35,  87 => 33,  78 => 27,  66 => 18,  57 => 12,  49 => 7,  44 => 5,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_all.html.twig' %}
{% block body %}
<div id=\"formu\" class=\"row\">
    <div class=\"col-md-6 col-md-offset-3 format-forms color-azul\">
\t\t\t{{ form_start(form) }}
\t\t\t<fieldset>
\t\t\t\t<legend>{{(title)}}</legend>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t  \t\t<div class=\"col-md-9 col-md-offset-1 mostrar\">
\t\t\t\t\t\t\t\t<img src=\"{{asset(\"img/images/asd.jpg\")}}\" width=\"148\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t  \t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<label for=\"form_imgPortfoli\" class=\"btn btn-default largo\">
\t\t\t\t\t\t\t\t\t{{ form_row(form.imgPortfoli) }}
\t\t\t\t\t\t\t\t\tAdd image portfoli
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t\t\t{{ form_row(form.title) }}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-1\">*</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-5\">
\t\t\t\t\t\t\t\t{{ form_row(form.isPremium) }}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t{% if free is empty %}
\t\t\t\t\t\t\t<div class=\"col-md-2\">*</div>
                    \t\t{% else %}
\t\t\t\t\t\t\t<div class=\"col-md-2 windowform\">* <span>Only one free portfoli</span></div>
                    \t\t{% endif %}
\t\t\t\t\t\t</div>
            <div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-5\">
\t\t\t\t\t\t\t\t{{ form_row(form.isInformatic) }}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t\t{{ form_row(form.idCategory) }}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</fieldset>
\t\t\t{{ form_end(form) }}
\t\t<div>
\t</div>
{% endblock %}
", "default/portfolis/create_portfoli.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/portfolis/create_portfoli.html.twig");
    }
}
