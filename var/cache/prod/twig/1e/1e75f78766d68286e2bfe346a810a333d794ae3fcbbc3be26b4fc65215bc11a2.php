<?php

/* default/portfolis/portfolisList.html.twig */
class __TwigTemplate_42de07c688a8e55241cbbe66b9806f40b9b80bcc5f491467665cc96680e7a93c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_all.html.twig", "default/portfolis/portfolisList.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_all.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed9295282a1b96e12b63674e2fbc57fb23287953a5e7b210975196dddff2c4c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed9295282a1b96e12b63674e2fbc57fb23287953a5e7b210975196dddff2c4c2->enter($__internal_ed9295282a1b96e12b63674e2fbc57fb23287953a5e7b210975196dddff2c4c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/portfolis/portfolisList.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ed9295282a1b96e12b63674e2fbc57fb23287953a5e7b210975196dddff2c4c2->leave($__internal_ed9295282a1b96e12b63674e2fbc57fb23287953a5e7b210975196dddff2c4c2_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_09589794f95971597f63bee1e9a727378598585645caa017a5d0aaf24ecc1a4c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09589794f95971597f63bee1e9a727378598585645caa017a5d0aaf24ecc1a4c->enter($__internal_09589794f95971597f63bee1e9a727378598585645caa017a5d0aaf24ecc1a4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"row\">
  <div class=\"col-md-10 col-md-offset-1\">
    ";
        // line 6
        $context["counter"] = 0;
        // line 7
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["portfolis"] ?? $this->getContext($context, "portfolis")));
        foreach ($context['_seq'] as $context["_key"] => $context["portfoli"]) {
            // line 8
            echo "      ";
            if (((($context["counter"] ?? $this->getContext($context, "counter")) % 3) == 0)) {
                // line 9
                echo "      <div class=\"row\">
      ";
            }
            // line 11
            echo "          <div class=\"col-md-4\">
            <div class=\"row format\">
              <div class=\"col-md-7\">
                <img src=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/portfolis/" . $this->getAttribute($context["portfoli"], "imgPortfoli", array()))), "html", null, true);
            echo "\" width=\"170\" height=\"130\" class=\"esq\">
              </div>
              <div class=\"col-md-5\">
                <h4>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["portfoli"], "title", array()), "html", null, true);
            echo "</h4>
                <h6 class=\"sub-text\">";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["portfoli"], "dateString", array()), "html", null, true);
            echo "</h6>
                <a href=\"/portfoli/";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["portfoli"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary btn-sm\">Read more</a>
              </div>
            </div>
          </div>
      ";
            // line 23
            if ((((($context["counter"] ?? $this->getContext($context, "counter")) % 3) == 2) || (($context["counter"] ?? $this->getContext($context, "counter")) == twig_length_filter($this->env, ($context["portfolis"] ?? $this->getContext($context, "portfolis")))))) {
                // line 24
                echo "      </div>
      ";
            }
            // line 26
            echo "      ";
            $context["counter"] = (($context["counter"] ?? $this->getContext($context, "counter")) + 1);
            // line 27
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['portfoli'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "  </div>
</div>
";
        
        $__internal_09589794f95971597f63bee1e9a727378598585645caa017a5d0aaf24ecc1a4c->leave($__internal_09589794f95971597f63bee1e9a727378598585645caa017a5d0aaf24ecc1a4c_prof);

    }

    public function getTemplateName()
    {
        return "default/portfolis/portfolisList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 28,  93 => 27,  90 => 26,  86 => 24,  84 => 23,  77 => 19,  73 => 18,  69 => 17,  63 => 14,  58 => 11,  54 => 9,  51 => 8,  46 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_all.html.twig' %}

{% block body %}
<div class=\"row\">
  <div class=\"col-md-10 col-md-offset-1\">
    {% set counter = 0 %}
    {% for portfoli in portfolis %}
      {% if(counter%3==0) %}
      <div class=\"row\">
      {% endif %}
          <div class=\"col-md-4\">
            <div class=\"row format\">
              <div class=\"col-md-7\">
                <img src=\"{{ asset('img/portfolis/'~portfoli.imgPortfoli) }}\" width=\"170\" height=\"130\" class=\"esq\">
              </div>
              <div class=\"col-md-5\">
                <h4>{{ (portfoli.title) }}</h4>
                <h6 class=\"sub-text\">{{ (portfoli.dateString) }}</h6>
                <a href=\"/portfoli/{{ (portfoli.id) }}\" class=\"btn btn-primary btn-sm\">Read more</a>
              </div>
            </div>
          </div>
      {% if(counter%3==2 or counter==portfolis|length) %}
      </div>
      {% endif %}
      {% set counter = counter + 1 %}
    {% endfor %}
  </div>
</div>
{% endblock %}
", "default/portfolis/portfolisList.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/portfolis/portfolisList.html.twig");
    }
}
