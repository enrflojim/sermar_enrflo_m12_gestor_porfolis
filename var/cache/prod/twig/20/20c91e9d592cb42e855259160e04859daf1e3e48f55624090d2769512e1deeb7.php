<?php

/* default/images/create_images.html.twig */
class __TwigTemplate_247e85fdd18a4cf10d7e9d53e5974f1b36c6f6a4f396fd8eb286eadab05757de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/images/create_images.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1e3b7333853aedf19c92983465be5ff38dae53a625b995b169eef76d90c830fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e3b7333853aedf19c92983465be5ff38dae53a625b995b169eef76d90c830fd->enter($__internal_1e3b7333853aedf19c92983465be5ff38dae53a625b995b169eef76d90c830fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/images/create_images.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1e3b7333853aedf19c92983465be5ff38dae53a625b995b169eef76d90c830fd->leave($__internal_1e3b7333853aedf19c92983465be5ff38dae53a625b995b169eef76d90c830fd_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_207e070d0105a90d7d34178c38aa9855e9a2516c0a9a582bc40404dd8f6b1a8f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_207e070d0105a90d7d34178c38aa9855e9a2516c0a9a582bc40404dd8f6b1a8f->enter($__internal_207e070d0105a90d7d34178c38aa9855e9a2516c0a9a582bc40404dd8f6b1a8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div id=\"formu\" class=\"row\">
        <div class=\"col-md-8 col-md-offset-2 format-forms color-azul\">
            <div class=\"format-form\">
                ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
                <fieldset>
                    <legend>";
        // line 8
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</legend>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <div class=\"row line\">
                                <div class=\"col-md-8 col-md-offset-1 mostrar\">
                                    <img src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/images/asd.jpg"), "html", null, true);
        echo "\" width=\"148\">
                                </div>
                            </div>
                            <div class=\"row line\">
                                <div class=\"col-md-10\">
                                    <label for=\"form_image\" class=\"btn btn-default largo\">
                                        ";
        // line 19
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "image", array()), 'row');
        echo "
                                        Add image user *
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class=\"col-md-9\">
                            <div class=\"row line\">
                                <div class=\"col-md-9\">
                                    ";
        // line 28
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "title", array()), 'row');
        echo "
                                </div>
                                <div class=\"col-md-2\">*</div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-md-9\">
                                    ";
        // line 34
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "description", array()), 'row');
        echo "
                                </div>
                                <div class=\"col-md-2\">*</div>
                            </div>
                        </div>
                    </div>
            </div>
            <hr>
            <div class=\"row\">
                <div class=\"col-md-1 col-md-offset-1\">
                    <a href=\"/portfoli/";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["article"] ?? $this->getContext($context, "article")), "idCard", array()), "idPortfoli", array()), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">Portfoli</a>
                </div>
                <div class=\"col-md-1 col-md-offset-1\">
                    <a href=\"/cards/";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["article"] ?? $this->getContext($context, "article")), "idCard", array()), "idPortfoli", array()), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">List cards</a>
                </div>
                <div class=\"col-md-1 col-md-offset-1\">
                    <a href=\"/articles/";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["article"] ?? $this->getContext($context, "article")), "idCard", array()), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">List article</a>
                </div>
                <div class=\"col-md-1 col-md-offset-1\">
                    <a href=\"/image/";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute(($context["article"] ?? $this->getContext($context, "article")), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">List Images</a>
                </div>
                <div class=\"col-md-1 col-md-offset-1\">
                    <input type=\"submit\" class=\"btn btn-primary btn-md\" value=\"";
        // line 56
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "\"/>
                </div>
            </div>
            </fieldset>
            ";
        // line 60
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
        </div>
        <div>
        </div>
    ";
        
        $__internal_207e070d0105a90d7d34178c38aa9855e9a2516c0a9a582bc40404dd8f6b1a8f->leave($__internal_207e070d0105a90d7d34178c38aa9855e9a2516c0a9a582bc40404dd8f6b1a8f_prof);

    }

    public function getTemplateName()
    {
        return "default/images/create_images.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 60,  125 => 56,  119 => 53,  113 => 50,  107 => 47,  101 => 44,  88 => 34,  79 => 28,  67 => 19,  58 => 13,  50 => 8,  45 => 6,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}
{% block body %}
    <div id=\"formu\" class=\"row\">
        <div class=\"col-md-8 col-md-offset-2 format-forms color-azul\">
            <div class=\"format-form\">
                {{ form_start(form) }}
                <fieldset>
                    <legend>{{(title)}}</legend>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <div class=\"row line\">
                                <div class=\"col-md-8 col-md-offset-1 mostrar\">
                                    <img src=\"{{asset(\"img/images/asd.jpg\")}}\" width=\"148\">
                                </div>
                            </div>
                            <div class=\"row line\">
                                <div class=\"col-md-10\">
                                    <label for=\"form_image\" class=\"btn btn-default largo\">
                                        {{ form_row(form.image) }}
                                        Add image user *
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class=\"col-md-9\">
                            <div class=\"row line\">
                                <div class=\"col-md-9\">
                                    {{ form_row(form.title) }}
                                </div>
                                <div class=\"col-md-2\">*</div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-md-9\">
                                    {{ form_row(form.description) }}
                                </div>
                                <div class=\"col-md-2\">*</div>
                            </div>
                        </div>
                    </div>
            </div>
            <hr>
            <div class=\"row\">
                <div class=\"col-md-1 col-md-offset-1\">
                    <a href=\"/portfoli/{{(article.idCard.idPortfoli.id)}}\" class=\"btn btn-primary btn-md\">Portfoli</a>
                </div>
                <div class=\"col-md-1 col-md-offset-1\">
                    <a href=\"/cards/{{(article.idCard.idPortfoli.id)}}\" class=\"btn btn-primary btn-md\">List cards</a>
                </div>
                <div class=\"col-md-1 col-md-offset-1\">
                    <a href=\"/articles/{{(article.idCard.id)}}\" class=\"btn btn-primary btn-md\">List article</a>
                </div>
                <div class=\"col-md-1 col-md-offset-1\">
                    <a href=\"/image/{{(article.id)}}\" class=\"btn btn-primary btn-md\">List Images</a>
                </div>
                <div class=\"col-md-1 col-md-offset-1\">
                    <input type=\"submit\" class=\"btn btn-primary btn-md\" value=\"{{(title)}}\"/>
                </div>
            </div>
            </fieldset>
            {{ form_end(form) }}
        </div>
        <div>
        </div>
    {% endblock %}
", "default/images/create_images.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/images/create_images.html.twig");
    }
}
