<?php

/* default/portfolis/portfoli.html.twig */
class __TwigTemplate_a660108e8fbd3e41df07795c509de7a23277272ecf7bf63847122c95226e036a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/portfolis/portfoli.html.twig", 1);
        $this->blocks = array(
            'stylesheet' => array($this, 'block_stylesheet'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d647f6a494e093860fd6f310012c0f3a156c1a51d88765ac5fc6d9cbb6e84c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3d647f6a494e093860fd6f310012c0f3a156c1a51d88765ac5fc6d9cbb6e84c9->enter($__internal_3d647f6a494e093860fd6f310012c0f3a156c1a51d88765ac5fc6d9cbb6e84c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/portfolis/portfoli.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3d647f6a494e093860fd6f310012c0f3a156c1a51d88765ac5fc6d9cbb6e84c9->leave($__internal_3d647f6a494e093860fd6f310012c0f3a156c1a51d88765ac5fc6d9cbb6e84c9_prof);

    }

    // line 3
    public function block_stylesheet($context, array $blocks = array())
    {
        $__internal_26c1f3220d859990e937e1252d5100c7fb689a39c8d395794ca2c1987b1ee3bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_26c1f3220d859990e937e1252d5100c7fb689a39c8d395794ca2c1987b1ee3bb->enter($__internal_26c1f3220d859990e937e1252d5100c7fb689a39c8d395794ca2c1987b1ee3bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheet"));

        // line 4
        echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/listCards.css"), "html", null, true);
        echo "\" />
";
        
        $__internal_26c1f3220d859990e937e1252d5100c7fb689a39c8d395794ca2c1987b1ee3bb->leave($__internal_26c1f3220d859990e937e1252d5100c7fb689a39c8d395794ca2c1987b1ee3bb_prof);

    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        $__internal_5c60905872efe62adbc751906a9c0338a13f8f163d9939ee822544073505e629 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c60905872efe62adbc751906a9c0338a13f8f163d9939ee822544073505e629->enter($__internal_5c60905872efe62adbc751906a9c0338a13f8f163d9939ee822544073505e629_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 9
        echo "    <div class=\"row\">
        <div class=\"col-md-2 col-md-offset-1\">
            <h3>Cards</h3>
            <div class=\"panel-group\" id=\"acordeon\">
                ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sub"] ?? $this->getContext($context, "sub")));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 14
            echo "                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#acordeon\" href=\"#collapse";
            // line 17
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "</a>
                            </h4>
                        </div>
                        <div id=\"collapse";
            // line 20
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "\" class=\"panel-collapse collapse\">
                            ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "cards", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
                // line 22
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["card"], "subCategories", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["subcategories"]) {
                    // line 23
                    echo "                                    ";
                    if (($this->getAttribute($context["subcategories"], "nameSubcategory", array()) == $context["s"])) {
                        // line 24
                        echo "                                        <div class=\"panel-body\"><a href=\"/card/";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "id", array()), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "title", array()), "html", null, true);
                        echo "</a></div>
                                        ";
                    }
                    // line 26
                    echo "                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcategories'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 27
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "            </div>
        </div>
        <div class=\"col-md-8\">
            ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "cards", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
            // line 35
            echo "                ";
            $context["date"] = twig_split_filter($this->env, $this->getAttribute($context["card"], "dateStringCard", array()), ",");
            // line 36
            echo "                <div class=\"row\">
                    <div class=\"col-md-12 card\">
                        <div class=\"row\">
                            <div class=\"col-md-2\">
                                <div class=\"centro tope format-date\">
                                    ";
            // line 41
            $context["i"] = 0;
            // line 42
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["date"] ?? $this->getContext($context, "date")));
            foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
                // line 43
                echo "                                        ";
                if ((($context["i"] ?? $this->getContext($context, "i")) == 0)) {
                    // line 44
                    echo "                                            <div class=\"date-card-form\">";
                    echo twig_escape_filter($this->env, $context["d"], "html", null, true);
                    echo "</div>
                                        ";
                } else {
                    // line 46
                    echo "                                            <div class=\"date-card-form-year\">";
                    echo twig_escape_filter($this->env, $context["d"], "html", null, true);
                    echo "</div>
                                        ";
                }
                // line 48
                echo "                                        ";
                $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
                // line 49
                echo "                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "                                </div>
                            </div>
                            <div class=\"col-md-10 card\">
                                <h2>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "title", array()), "html", null, true);
            echo "</h2>
                            </div>
                        </div>
                        <hr class=\"remarcar\">
                        <p><span class=\"glyphicon glyphicon-user\"></span> by
                            <i>";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["card"], "idPortfoli", array()), "idUser", array()), "name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["card"], "idPortfoli", array()), "idUser", array()), "surname", array()), "html", null, true);
            echo "</i>
                        </p>
                        ";
            // line 60
            echo $this->getAttribute($context["card"], "description", array());
            echo "
                        <a class=\"col-md-offset-10 btn btn-primary\" href=\"/card/";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "id", array()), "html", null, true);
            echo "\">Read more</a>
                        <br/>
                        <br/>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "        </div>
    </div>
";
        
        $__internal_5c60905872efe62adbc751906a9c0338a13f8f163d9939ee822544073505e629->leave($__internal_5c60905872efe62adbc751906a9c0338a13f8f163d9939ee822544073505e629_prof);

    }

    public function getTemplateName()
    {
        return "default/portfolis/portfoli.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  210 => 67,  198 => 61,  194 => 60,  187 => 58,  179 => 53,  174 => 50,  168 => 49,  165 => 48,  159 => 46,  153 => 44,  150 => 43,  145 => 42,  143 => 41,  136 => 36,  133 => 35,  129 => 34,  124 => 31,  116 => 28,  110 => 27,  104 => 26,  96 => 24,  93 => 23,  88 => 22,  84 => 21,  80 => 20,  72 => 17,  67 => 14,  63 => 13,  57 => 9,  51 => 8,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}

{% block stylesheet %}
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/listCards.css')}}\" />
{% endblock %}


{% block body %}
    <div class=\"row\">
        <div class=\"col-md-2 col-md-offset-1\">
            <h3>Cards</h3>
            <div class=\"panel-group\" id=\"acordeon\">
                {% for s in sub %}
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#acordeon\" href=\"#collapse{{(s)}}\">{{(s)}}</a>
                            </h4>
                        </div>
                        <div id=\"collapse{{(s)}}\" class=\"panel-collapse collapse\">
                            {% for card in portfoli.cards %}
                                {% for subcategories in card.subCategories %}
                                    {% if subcategories.nameSubcategory == s %}
                                        <div class=\"panel-body\"><a href=\"/card/{{(card.id)}}\">{{(card.title)}}</a></div>
                                        {% endif %}
                                    {% endfor %}
                                {% endfor %}
                        </div>
                    </div>
                {% endfor %}
            </div>
        </div>
        <div class=\"col-md-8\">
            {% for card in portfoli.cards %}
                {% set date = card.dateStringCard|split(',') %}
                <div class=\"row\">
                    <div class=\"col-md-12 card\">
                        <div class=\"row\">
                            <div class=\"col-md-2\">
                                <div class=\"centro tope format-date\">
                                    {% set i = 0 %}
                                    {% for d in date %}
                                        {% if i == 0 %}
                                            <div class=\"date-card-form\">{{(d)}}</div>
                                        {% else %}
                                            <div class=\"date-card-form-year\">{{(d)}}</div>
                                        {% endif %}
                                        {% set i = i+1 %}
                                    {% endfor %}
                                </div>
                            </div>
                            <div class=\"col-md-10 card\">
                                <h2>{{card.title}}</h2>
                            </div>
                        </div>
                        <hr class=\"remarcar\">
                        <p><span class=\"glyphicon glyphicon-user\"></span> by
                            <i>{{(card.idPortfoli.idUser.name)}} {{(card.idPortfoli.idUser.surname)}}</i>
                        </p>
                        {{(card.description|raw)}}
                        <a class=\"col-md-offset-10 btn btn-primary\" href=\"/card/{{card.id}}\">Read more</a>
                        <br/>
                        <br/>
                    </div>
                </div>
            {% endfor %}
        </div>
    </div>
{% endblock %}
", "default/portfolis/portfoli.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/portfolis/portfoli.html.twig");
    }
}
