<?php

/* default/portfolis/edit_portfoli.html.twig */
class __TwigTemplate_4f589c14ca237e92f78a17ddb9ecf8b78422afb9be4ec567862b6fa9047bad62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/portfolis/edit_portfoli.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_45a4764db70730ed28c15c151cc54d94557be503bec92b247efde67584f534fa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_45a4764db70730ed28c15c151cc54d94557be503bec92b247efde67584f534fa->enter($__internal_45a4764db70730ed28c15c151cc54d94557be503bec92b247efde67584f534fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/portfolis/edit_portfoli.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_45a4764db70730ed28c15c151cc54d94557be503bec92b247efde67584f534fa->leave($__internal_45a4764db70730ed28c15c151cc54d94557be503bec92b247efde67584f534fa_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_26a98ed45718fdcfddd3cdf38e71b105ce9a2e7210e0aefe0ba14525dbd95d15 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_26a98ed45718fdcfddd3cdf38e71b105ce9a2e7210e0aefe0ba14525dbd95d15->enter($__internal_26a98ed45718fdcfddd3cdf38e71b105ce9a2e7210e0aefe0ba14525dbd95d15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<div id=\"formu\" class=\"row\">
    <div class=\"col-md-4 col-md-offset-4 format-forms color-azul\">
      ";
        // line 5
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
      <fieldset>
\t\t\t\t<legend>";
        // line 7
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</legend>
        <div class=\"row line\">
          <div class=\"col-md-12\">
            <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/portfolis/" . $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "imgPortfoli", array()))), "html", null, true);
        echo "\" width=\"340\" height=\"230\">
          </div>
        </div>
        <div class=\"row line\">
          <div class=\"col-md-12\">
            <label for=\"form_imgPortfoli\" class=\"btn btn-default largo\">
              ";
        // line 16
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "imgPortfoli", array()), 'row');
        echo "
              Edit image portfoli
            </label>
          </div>
        </div>
        <hr>
        <div class=\"row line\">
          <div class=\"col-md-12\">
            <button type=\"submit\" class=\"btn btn-primary btn-md largo\">Edit image portfoli</button>
          </div>
        </div>
      </fieldset>
  \t\t";
        // line 28
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    </div>
</div>
";
        
        $__internal_26a98ed45718fdcfddd3cdf38e71b105ce9a2e7210e0aefe0ba14525dbd95d15->leave($__internal_26a98ed45718fdcfddd3cdf38e71b105ce9a2e7210e0aefe0ba14525dbd95d15_prof);

    }

    public function getTemplateName()
    {
        return "default/portfolis/edit_portfoli.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 28,  64 => 16,  55 => 10,  49 => 7,  44 => 5,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}
{% block body %}
<div id=\"formu\" class=\"row\">
    <div class=\"col-md-4 col-md-offset-4 format-forms color-azul\">
      {{ form_start(form) }}
      <fieldset>
\t\t\t\t<legend>{{(title)}}</legend>
        <div class=\"row line\">
          <div class=\"col-md-12\">
            <img src=\"{{ asset('img/portfolis/'~portfoli.imgPortfoli) }}\" width=\"340\" height=\"230\">
          </div>
        </div>
        <div class=\"row line\">
          <div class=\"col-md-12\">
            <label for=\"form_imgPortfoli\" class=\"btn btn-default largo\">
              {{ form_row(form.imgPortfoli) }}
              Edit image portfoli
            </label>
          </div>
        </div>
        <hr>
        <div class=\"row line\">
          <div class=\"col-md-12\">
            <button type=\"submit\" class=\"btn btn-primary btn-md largo\">Edit image portfoli</button>
          </div>
        </div>
      </fieldset>
  \t\t{{ form_end(form) }}
    </div>
</div>
{% endblock %}
", "default/portfolis/edit_portfoli.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/portfolis/edit_portfoli.html.twig");
    }
}
