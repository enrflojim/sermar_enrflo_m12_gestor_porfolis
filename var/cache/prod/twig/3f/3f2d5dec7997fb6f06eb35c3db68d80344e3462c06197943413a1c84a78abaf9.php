<?php

/* default/cards/list_cards.html.twig */
class __TwigTemplate_680029cc39e5f853d9ee61c98a2d598cc19d43f99ceb1ce646000ba97d1d7292 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/cards/list_cards.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9106b4f0bd7d64c0cfc2114e79c7a18665690c3fa09a804176580d0931b94722 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9106b4f0bd7d64c0cfc2114e79c7a18665690c3fa09a804176580d0931b94722->enter($__internal_9106b4f0bd7d64c0cfc2114e79c7a18665690c3fa09a804176580d0931b94722_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/cards/list_cards.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9106b4f0bd7d64c0cfc2114e79c7a18665690c3fa09a804176580d0931b94722->leave($__internal_9106b4f0bd7d64c0cfc2114e79c7a18665690c3fa09a804176580d0931b94722_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_519922e5e1cc26b5ad3c2d4c8c77a763b10a8ca9d3428baa340d3ac2d6af9fbc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_519922e5e1cc26b5ad3c2d4c8c77a763b10a8ca9d3428baa340d3ac2d6af9fbc->enter($__internal_519922e5e1cc26b5ad3c2d4c8c77a763b10a8ca9d3428baa340d3ac2d6af9fbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div id=\"formu\" class=\"format\">
        <div class=\"row format-add color-black\">
            <div class=\"col-md-1\"><h5>Id</h5></div>
            <div class=\"col-md-3\"><h5>Title</h5></div>
            <div class=\"col-md-2\"><h5>Category</h5></div>
            <div class=\"col-md-2\"><h5>Add article</h5></div>
            <div class=\"col-md-2\"><h5>Edit card</h5></div>
            <div class=\"col-md-2 \"><h5>Delete card</h5></div>
        </div>
        ";
        // line 12
        if ((twig_length_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "cards", array())) == 0)) {
            // line 13
            echo "            <div class=\"row\">
                <div class=\"col-md-4 colmd-offset-1\">
                    <h3>Any cards in this portfoli</h3>
                </div>
            </div>
        ";
        } else {
            // line 19
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "cards", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
                // line 20
                echo "                <div class=\"row\">
                    <div class=\"col-md-1\"><h5>";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "id", array()), "html", null, true);
                echo "</h5></div>
                    <div class=\"col-md-3\"><h5>";
                // line 22
                echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "title", array()), "html", null, true);
                echo "</h5></div>
                    <div class=\"col-md-2\">
                        ";
                // line 24
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["card"], "subCategories", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 25
                    echo "                            <h5>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "nameSubcategory", array()), "html", null, true);
                    echo "</h5>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 27
                echo "                    </div>
                    <div class=\"col-md-2\">
                        <a href=\"/articles/";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/icons/add.png"), "html", null, true);
                echo "\" alt=\"add\" tittle=\"add\" width=\"30\"></a>
                    </div>
                    <div class=\"col-md-2\">
                        <a href=\"/editcard/";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/icons/edit.png"), "html", null, true);
                echo "\" alt=\"edit\" tittle=\"edit\" width=\"30\"></a>
                    </div>
                    <div class=\"col-md-2\">
                        <a href=\"/deletecard/";
                // line 35
                echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "id", array()), "html", null, true);
                echo "\"  onclick=\"return confirm('Are you sure delete card ?')\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/icons/delete.png"), "html", null, true);
                echo "\" alt=\"delete\" tittle=\"delete\" width=\"30\"></a>
                    </div>
                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "        ";
        }
        // line 40
        echo "        <div class=\"row\">
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/portfoli/";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
        echo "\"  class=\"btn btn-primary btn-md\" >Portfoli</a>
            </div>
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/formcard/";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
        echo "\" class=\"btn btn-primary btn-md\">Create Card</a>
            </div>
        </div>
        <hr>
    </div>
";
        
        $__internal_519922e5e1cc26b5ad3c2d4c8c77a763b10a8ca9d3428baa340d3ac2d6af9fbc->leave($__internal_519922e5e1cc26b5ad3c2d4c8c77a763b10a8ca9d3428baa340d3ac2d6af9fbc_prof);

    }

    public function getTemplateName()
    {
        return "default/cards/list_cards.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 45,  130 => 42,  126 => 40,  123 => 39,  111 => 35,  103 => 32,  95 => 29,  91 => 27,  82 => 25,  78 => 24,  73 => 22,  69 => 21,  66 => 20,  61 => 19,  53 => 13,  51 => 12,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}
{% block body %}
    <div id=\"formu\" class=\"format\">
        <div class=\"row format-add color-black\">
            <div class=\"col-md-1\"><h5>Id</h5></div>
            <div class=\"col-md-3\"><h5>Title</h5></div>
            <div class=\"col-md-2\"><h5>Category</h5></div>
            <div class=\"col-md-2\"><h5>Add article</h5></div>
            <div class=\"col-md-2\"><h5>Edit card</h5></div>
            <div class=\"col-md-2 \"><h5>Delete card</h5></div>
        </div>
        {% if portfoli.cards|length==0%}
            <div class=\"row\">
                <div class=\"col-md-4 colmd-offset-1\">
                    <h3>Any cards in this portfoli</h3>
                </div>
            </div>
        {% else %}
            {% for card in portfoli.cards %}
                <div class=\"row\">
                    <div class=\"col-md-1\"><h5>{{(card.id)}}</h5></div>
                    <div class=\"col-md-3\"><h5>{{(card.title)}}</h5></div>
                    <div class=\"col-md-2\">
                        {%for category in card.subCategories%}
                            <h5>{{(category.nameSubcategory)}}</h5>
                        {% endfor %}
                    </div>
                    <div class=\"col-md-2\">
                        <a href=\"/articles/{{(card.id)}}\"><img src=\"{{ asset(\"img/icons/add.png\")}}\" alt=\"add\" tittle=\"add\" width=\"30\"></a>
                    </div>
                    <div class=\"col-md-2\">
                        <a href=\"/editcard/{{(card.id)}}\"><img src=\"{{ asset(\"img/icons/edit.png\")}}\" alt=\"edit\" tittle=\"edit\" width=\"30\"></a>
                    </div>
                    <div class=\"col-md-2\">
                        <a href=\"/deletecard/{{(card.id)}}\"  onclick=\"return confirm('Are you sure delete card ?')\"><img src=\"{{ asset(\"img/icons/delete.png\")}}\" alt=\"delete\" tittle=\"delete\" width=\"30\"></a>
                    </div>
                </div>
            {% endfor %}
        {% endif %}
        <div class=\"row\">
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/portfoli/{{((portfoli.id))}}\"  class=\"btn btn-primary btn-md\" >Portfoli</a>
            </div>
            <div class=\"col-md-1 col-md-offset-1\">
                <a href=\"/formcard/{{((portfoli.id))}}\" class=\"btn btn-primary btn-md\">Create Card</a>
            </div>
        </div>
        <hr>
    </div>
{% endblock %}
", "default/cards/list_cards.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/cards/list_cards.html.twig");
    }
}
