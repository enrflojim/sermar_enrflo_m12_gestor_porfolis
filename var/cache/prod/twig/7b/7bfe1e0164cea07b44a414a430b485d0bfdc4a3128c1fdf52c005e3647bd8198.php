<?php

/* default/user/form_update.html.twig */
class __TwigTemplate_bea21a864e2a5033dc2b5e730da4e10ccd2f995c7737e0049c58e78c910f8957 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("default/header/header_all.html.twig", "default/user/form_update.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_all.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf48a60830c068e6b0d36bc91bfbde8fecae05b161034f85554954f057536ce2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf48a60830c068e6b0d36bc91bfbde8fecae05b161034f85554954f057536ce2->enter($__internal_cf48a60830c068e6b0d36bc91bfbde8fecae05b161034f85554954f057536ce2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/user/form_update.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cf48a60830c068e6b0d36bc91bfbde8fecae05b161034f85554954f057536ce2->leave($__internal_cf48a60830c068e6b0d36bc91bfbde8fecae05b161034f85554954f057536ce2_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_56a9c46723460f267999330fad4b09bbf471319cb0e14b5399406719539ea435 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56a9c46723460f267999330fad4b09bbf471319cb0e14b5399406719539ea435->enter($__internal_56a9c46723460f267999330fad4b09bbf471319cb0e14b5399406719539ea435_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div id=\"formu\" class=\"row\">
    <div class=\"col-md-8 col-md-offset-2 color-azul format-forms\">
\t\t\t";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
\t\t\t\t<fieldset>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t\t<h1>";
        // line 10
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</h1>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t\t<p>* required</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<hr>
          <div class=\"row\">
              <div class=\"col-md-4\">
                <div class=\"row\">
                    <div class=\"col-md-11\">
                        <div class=\"mostrar\"><img width=\"240\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(($context["img"] ?? $this->getContext($context, "img"))), "html", null, true);
        echo "\"
                             class=\"circle\" alt=\"Smiley face\"> </div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-md-11\">
                        <label for=\"form_linkImage\" class=\"btn btn-default largo\">
                          ";
        // line 28
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "linkImage", array()), 'row');
        echo "
                          Add image user *
                        </label>
                    </div>
                </div>
              </div>
              <div class=\"col-md-8\">
        \t\t\t\t\t<div class=\"row\">
        \t\t\t\t\t\t<div class=\"col-md-9\">
        \t\t\t\t\t\t\t<legend>Personal Dates</legend>
        \t\t\t\t\t\t</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t\t\t\t\t";
        // line 42
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "name", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t\t\t\t\t";
        // line 48
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "surname", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 54
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "lastname", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 60
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 66
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "gender", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t\t\t\t<legend>Contact</legend>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 75
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "phone", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 81
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
                    ";
        // line 83
        if ((($context["errormail"] ?? $this->getContext($context, "errormail")) == false)) {
            // line 84
            echo "        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
                    ";
        } else {
            // line 86
            echo "                    <div class=\"col-md-1 windowform\">*<span>The Email is repeated</span></div>
                    ";
        }
        // line 88
        echo "        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t\t\t\t<legend>Location</legend>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 94
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "type", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 100
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "street", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 106
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "number", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 111
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "floor", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 116
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "door", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>

        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 122
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "cp", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 128
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "city", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 134
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "country", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t\t\t\t<legend>Access</legend>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t";
        // line 143
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "username", array()), 'row');
        echo "
        \t\t\t\t\t\t</div>
                    ";
        // line 145
        if ((($context["erroruser"] ?? $this->getContext($context, "erroruser")) == false)) {
            // line 146
            echo "        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
                    ";
        } else {
            // line 148
            echo "                    <div class=\"col-md-1 windowform\">*<span>The username is repeated</span></div>
                    ";
        }
        // line 150
        echo "        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-4\">
        \t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary btn-md largo\">Register!</button>
        \t\t\t\t\t\t</div>
        \t\t\t\t\t</div>
              </div>
          </div>
\t\t\t\t</fieldset>
\t\t\t";
        // line 159
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
\t\t<div>
</div>
";
        
        $__internal_56a9c46723460f267999330fad4b09bbf471319cb0e14b5399406719539ea435->leave($__internal_56a9c46723460f267999330fad4b09bbf471319cb0e14b5399406719539ea435_prof);

    }

    public function getTemplateName()
    {
        return "default/user/form_update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 159,  260 => 150,  256 => 148,  252 => 146,  250 => 145,  245 => 143,  233 => 134,  224 => 128,  215 => 122,  206 => 116,  198 => 111,  190 => 106,  181 => 100,  172 => 94,  164 => 88,  160 => 86,  156 => 84,  154 => 83,  149 => 81,  140 => 75,  128 => 66,  119 => 60,  110 => 54,  101 => 48,  92 => 42,  75 => 28,  65 => 21,  51 => 10,  44 => 6,  40 => 4,  34 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/default/formInsert.html.twig #}
{% extends 'default/header/header_all.html.twig' %}
{% block body %}
<div id=\"formu\" class=\"row\">
    <div class=\"col-md-8 col-md-offset-2 color-azul format-forms\">
\t\t\t{{ form_start(form) }}
\t\t\t\t<fieldset>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t\t<h1>{{(title)}}</h1>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t\t<p>* required</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<hr>
          <div class=\"row\">
              <div class=\"col-md-4\">
                <div class=\"row\">
                    <div class=\"col-md-11\">
                        <div class=\"mostrar\"><img width=\"240\" src=\"{{asset(img) }}\"
                             class=\"circle\" alt=\"Smiley face\"> </div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-md-11\">
                        <label for=\"form_linkImage\" class=\"btn btn-default largo\">
                          {{ form_row(form.linkImage) }}
                          Add image user *
                        </label>
                    </div>
                </div>
              </div>
              <div class=\"col-md-8\">
        \t\t\t\t\t<div class=\"row\">
        \t\t\t\t\t\t<div class=\"col-md-9\">
        \t\t\t\t\t\t\t<legend>Personal Dates</legend>
        \t\t\t\t\t\t</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t\t\t\t\t{{ form_row(form.name) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t\t\t\t\t{{ form_row(form.surname) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.lastname) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.date) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.gender) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t\t\t\t<legend>Contact</legend>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.phone) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.email) }}
        \t\t\t\t\t\t</div>
                    {% if errormail == false %}
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
                    {% else %}
                    <div class=\"col-md-1 windowform\">*<span>The Email is repeated</span></div>
                    {% endif %}
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t\t\t\t<legend>Location</legend>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.type) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.street) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.number) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.floor) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.door) }}
        \t\t\t\t\t\t</div>

        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.cp) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.city) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.country) }}
        \t\t\t\t\t\t</div>
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t\t\t\t<legend>Access</legend>
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-9 col-md-offset-1\">
        \t\t\t    \t\t{{ form_row(form.username) }}
        \t\t\t\t\t\t</div>
                    {% if erroruser == false %}
        \t\t\t\t\t\t<div class=\"col-md-1\">*</div>
                    {% else %}
                    <div class=\"col-md-1 windowform\">*<span>The username is repeated</span></div>
                    {% endif %}
        \t\t\t\t\t</div>
        \t\t\t\t\t<div class=\"row line\">
        \t\t\t\t\t\t<div class=\"col-md-4\">
        \t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary btn-md largo\">Register!</button>
        \t\t\t\t\t\t</div>
        \t\t\t\t\t</div>
              </div>
          </div>
\t\t\t\t</fieldset>
\t\t\t{{ form_end(form) }}
\t\t<div>
</div>
{% endblock %}
", "default/user/form_update.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/user/form_update.html.twig");
    }
}
