<?php

/* default/user/login.html.twig */
class __TwigTemplate_57357df64670aca9908ceca4e9aba8fbaea4e8da1efb6ee92b37a707c617e5b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_all.html.twig", "default/user/login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_all.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d98c7fd4ce0b8516ddea608b1cf792415f1e88c9886bc90fb676c915571b4a26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d98c7fd4ce0b8516ddea608b1cf792415f1e88c9886bc90fb676c915571b4a26->enter($__internal_d98c7fd4ce0b8516ddea608b1cf792415f1e88c9886bc90fb676c915571b4a26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/user/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d98c7fd4ce0b8516ddea608b1cf792415f1e88c9886bc90fb676c915571b4a26->leave($__internal_d98c7fd4ce0b8516ddea608b1cf792415f1e88c9886bc90fb676c915571b4a26_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_fe98c56be587965de2ee62fbdc2cdadfcf667edce721979755b340e760e119cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe98c56be587965de2ee62fbdc2cdadfcf667edce721979755b340e760e119cc->enter($__internal_fe98c56be587965de2ee62fbdc2cdadfcf667edce721979755b340e760e119cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<form action=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">
<div class=\"row\">
  <div class=\"col-md-6 col-md-offset-3 color-azul line format-forms\">
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<h1>Logged User</h1>
\t\t</div>
\t</div>
  <hr>
\t<div class=\"row\">
\t\t<div class=\"col-md-4\">
\t\t\t<label for=\"username\">Username:</label>
\t\t</div>
\t\t<div class=\"col-md-8\">
\t\t\t<input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" />
\t\t</div>
\t</div>
    <div class=\"row\">
\t\t<div class=\"col-md-4\">
\t\t\t<label for=\"password\">Password:</label>
\t\t</div>
\t\t<div class=\"col-md-8\">
\t\t\t<input type=\"password\" id=\"password\" name=\"_password\" />
\t\t</div>
\t</div>
    ";
        // line 33
        echo "\t<div class=\"row\">
    <div class=\"col-md-3\">
      <a href=\"/forgotpass\" class=\"btn btn-primary btn-md\">Forggot password</a>
    </div>
\t\t<div class=\"col-md-3\">
\t\t\t<button type=\"submit\" class=\"btn btn-primary btn-md\">Logged user</button>
\t\t</div>
\t</div>
  ";
        // line 41
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 42
            echo "      <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
  ";
        }
        // line 44
        echo "  </div>
</div>
</form>

";
        
        $__internal_fe98c56be587965de2ee62fbdc2cdadfcf667edce721979755b340e760e119cc->leave($__internal_fe98c56be587965de2ee62fbdc2cdadfcf667edce721979755b340e760e119cc_prof);

    }

    public function getTemplateName()
    {
        return "default/user/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 44,  84 => 42,  82 => 41,  72 => 33,  58 => 17,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_all.html.twig' %}
{% block body %}
<form action=\"{{ path('login') }}\" method=\"post\">
<div class=\"row\">
  <div class=\"col-md-6 col-md-offset-3 color-azul line format-forms\">
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<h1>Logged User</h1>
\t\t</div>
\t</div>
  <hr>
\t<div class=\"row\">
\t\t<div class=\"col-md-4\">
\t\t\t<label for=\"username\">Username:</label>
\t\t</div>
\t\t<div class=\"col-md-8\">
\t\t\t<input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ (last_username) }}\" />
\t\t</div>
\t</div>
    <div class=\"row\">
\t\t<div class=\"col-md-4\">
\t\t\t<label for=\"password\">Password:</label>
\t\t</div>
\t\t<div class=\"col-md-8\">
\t\t\t<input type=\"password\" id=\"password\" name=\"_password\" />
\t\t</div>
\t</div>
    {#
        If you want to control the URL the user
        is redirected to on success (more details below)
        <input type=\"hidden\" name=\"_target_path\" value=\"/account\" />
    #}
\t<div class=\"row\">
    <div class=\"col-md-3\">
      <a href=\"/forgotpass\" class=\"btn btn-primary btn-md\">Forggot password</a>
    </div>
\t\t<div class=\"col-md-3\">
\t\t\t<button type=\"submit\" class=\"btn btn-primary btn-md\">Logged user</button>
\t\t</div>
\t</div>
  {% if error %}
      <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
  {% endif %}
  </div>
</div>
</form>

{% endblock %}
", "default/user/login.html.twig", "/var/www/html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/user/login.html.twig");
    }
}
