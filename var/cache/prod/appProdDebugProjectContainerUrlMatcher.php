<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // articles
        if (0 === strpos($pathinfo, '/articles') && preg_match('#^/articles/(?P<card>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'articles')), array (  '_controller' => 'AppBundle\\Controller\\ArticleController::listArticlesAction',));
        }

        // formarticle
        if (0 === strpos($pathinfo, '/formarticle') && preg_match('#^/formarticle/(?P<card>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'formarticle')), array (  '_controller' => 'AppBundle\\Controller\\ArticleController::createArticlesAction',));
        }

        // editarticle
        if (0 === strpos($pathinfo, '/editarticle') && preg_match('#^/editarticle/(?P<article>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editarticle')), array (  '_controller' => 'AppBundle\\Controller\\ArticleController::editArticlesAction',));
        }

        // deletearticle
        if (0 === strpos($pathinfo, '/deletearticle') && preg_match('#^/deletearticle/(?P<article>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'deletearticle')), array (  '_controller' => 'AppBundle\\Controller\\ArticleController::deleteArticleAction',));
        }

        // cards
        if (0 === strpos($pathinfo, '/cards') && preg_match('#^/cards/(?P<portfoli>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'cards')), array (  '_controller' => 'AppBundle\\Controller\\CardsController::listCardsAction',));
        }

        // formcard
        if (0 === strpos($pathinfo, '/formcard') && preg_match('#^/formcard/(?P<portfoli>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'formcard')), array (  '_controller' => 'AppBundle\\Controller\\CardsController::createCardAction',));
        }

        // editcard
        if (0 === strpos($pathinfo, '/editcard') && preg_match('#^/editcard/(?P<card>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editcard')), array (  '_controller' => 'AppBundle\\Controller\\CardsController::editCardAction',));
        }

        // deletecard
        if (0 === strpos($pathinfo, '/deletecard') && preg_match('#^/deletecard/(?P<card>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'deletecard')), array (  '_controller' => 'AppBundle\\Controller\\CardsController::deleteArticleAction',));
        }

        if (0 === strpos($pathinfo, '/c')) {
            // card
            if (0 === strpos($pathinfo, '/card') && preg_match('#^/card/(?P<card>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'card')), array (  '_controller' => 'AppBundle\\Controller\\CardsController::test1Action',));
            }

            if (0 === strpos($pathinfo, '/contact')) {
                // contactme
                if (0 === strpos($pathinfo, '/contactme') && preg_match('#^/contactme/(?P<portfoli>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'contactme')), array (  '_controller' => 'AppBundle\\Controller\\ContactController::contactMeFormAction',));
                }

                // contact
                if ($pathinfo === '/contact') {
                    return array (  '_controller' => 'AppBundle\\Controller\\ContactController::contactFormAction',  '_route' => 'contact',);
                }

            }

        }

        // accesdenied
        if ($pathinfo === '/accesdenied') {
            return array (  '_controller' => 'AppBundle\\Controller\\ErrorController::accessDeniedction',  '_route' => 'accesdenied',);
        }

        // notfound
        if ($pathinfo === '/notfound') {
            return array (  '_controller' => 'AppBundle\\Controller\\ErrorController::norFoundAction',  '_route' => 'notfound',);
        }

        // image
        if (0 === strpos($pathinfo, '/image') && preg_match('#^/image/(?P<article>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'image')), array (  '_controller' => 'AppBundle\\Controller\\ImageController::listImagesAction',));
        }

        // formimage
        if (0 === strpos($pathinfo, '/formimage') && preg_match('#^/formimage/(?P<article>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'formimage')), array (  '_controller' => 'AppBundle\\Controller\\ImageController::createImagesAction',));
        }

        // editimage
        if (0 === strpos($pathinfo, '/editimage') && preg_match('#^/editimage/(?P<image>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editimage')), array (  '_controller' => 'AppBundle\\Controller\\ImageController::editImagesAction',));
        }

        // deleteimage
        if (0 === strpos($pathinfo, '/deleteimage') && preg_match('#^/deleteimage/(?P<image>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'deleteimage')), array (  '_controller' => 'AppBundle\\Controller\\ImageController::deleteImagesAction',));
        }

        // portfolis
        if ($pathinfo === '/portfolis') {
            return array (  '_controller' => 'AppBundle\\Controller\\PortfolisController::portfolisAction',  '_route' => 'portfolis',);
        }

        // searchportfolis
        if ($pathinfo === '/searchportfolis') {
            return array (  '_controller' => 'AppBundle\\Controller\\PortfolisController::searchPortfolisAction',  '_route' => 'searchportfolis',);
        }

        // portfoli
        if (0 === strpos($pathinfo, '/portfoli') && preg_match('#^/portfoli/(?P<portfoli>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'portfoli')), array (  '_controller' => 'AppBundle\\Controller\\PortfolisController::portfoliAction',));
        }

        // createportfoli
        if ($pathinfo === '/createportfoli') {
            return array (  '_controller' => 'AppBundle\\Controller\\PortfolisController::createPortfoliAction',  '_route' => 'createportfoli',);
        }

        // editeportfoli
        if (0 === strpos($pathinfo, '/editportfoli') && preg_match('#^/editportfoli/(?P<portfoli>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editeportfoli')), array (  '_controller' => 'AppBundle\\Controller\\PortfolisController::editPortfoliAction',));
        }

        // myportfolis
        if ($pathinfo === '/myportfolis') {
            return array (  '_controller' => 'AppBundle\\Controller\\PortfolisController::myPortfolisAction',  '_route' => 'myportfolis',);
        }

        // deleteportfoli
        if (0 === strpos($pathinfo, '/deleteportfoli') && preg_match('#^/deleteportfoli/(?P<portfoli>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'deleteportfoli')), array (  '_controller' => 'AppBundle\\Controller\\PortfolisController::deletePortfoliAction',));
        }

        // register
        if ($pathinfo === '/register') {
            return array (  '_controller' => 'AppBundle\\Controller\\UserController::registroAction',  '_route' => 'register',);
        }

        // login
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'login');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\UserController::loginAction',  '_route' => 'login',);
        }

        // edituser
        if ($pathinfo === '/edituser') {
            return array (  '_controller' => 'AppBundle\\Controller\\UserController::editUserAction',  '_route' => 'edituser',);
        }

        // forgotpass
        if ($pathinfo === '/forgotpass') {
            return array (  '_controller' => 'AppBundle\\Controller\\UserController::forgotUserAction',  '_route' => 'forgotpass',);
        }

        // logout
        if ($pathinfo === '/logout') {
            return array('_route' => 'logout');
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
