<?php

/* default/varios/contact.html.twig */
class __TwigTemplate_aa021be4e47c0e8f323253a7106cc327c1250681260a59cc378fa44f197a07e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_all.html.twig", "default/varios/contact.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_all.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_710893850586b26096da75d42779703d429af4bf7fe8bb543a6a09aaa2caa51c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_710893850586b26096da75d42779703d429af4bf7fe8bb543a6a09aaa2caa51c->enter($__internal_710893850586b26096da75d42779703d429af4bf7fe8bb543a6a09aaa2caa51c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/varios/contact.html.twig"));

        $__internal_e1d20855e83396c6d9f4b7f046727cadd8408908f1d92e6a9fcbe54709d75987 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1d20855e83396c6d9f4b7f046727cadd8408908f1d92e6a9fcbe54709d75987->enter($__internal_e1d20855e83396c6d9f4b7f046727cadd8408908f1d92e6a9fcbe54709d75987_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/varios/contact.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_710893850586b26096da75d42779703d429af4bf7fe8bb543a6a09aaa2caa51c->leave($__internal_710893850586b26096da75d42779703d429af4bf7fe8bb543a6a09aaa2caa51c_prof);

        
        $__internal_e1d20855e83396c6d9f4b7f046727cadd8408908f1d92e6a9fcbe54709d75987->leave($__internal_e1d20855e83396c6d9f4b7f046727cadd8408908f1d92e6a9fcbe54709d75987_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_79b4c1d4084df04f69e2cdec70458663ddb4c97b789f2c5d10ae30dc147dce32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79b4c1d4084df04f69e2cdec70458663ddb4c97b789f2c5d10ae30dc147dce32->enter($__internal_79b4c1d4084df04f69e2cdec70458663ddb4c97b789f2c5d10ae30dc147dce32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6e53c48141d6770d0c8d96980a324bb7c602273a408d69413322770115a85e56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e53c48141d6770d0c8d96980a324bb7c602273a408d69413322770115a85e56->enter($__internal_6e53c48141d6770d0c8d96980a324bb7c602273a408d69413322770115a85e56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-10 col-md-offset-1 format-forms color-azul\">
              <div class=\"row\">
                  <div class=\"col-md-11\">
                  <h1>";
        // line 8
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</h1>
                  <p>You can contact me using your email or this webpage.</p>
                  ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
                  ";
        // line 11
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
                </div>
              </div>
            </div>
        </div>
    </div>
";
        
        $__internal_6e53c48141d6770d0c8d96980a324bb7c602273a408d69413322770115a85e56->leave($__internal_6e53c48141d6770d0c8d96980a324bb7c602273a408d69413322770115a85e56_prof);

        
        $__internal_79b4c1d4084df04f69e2cdec70458663ddb4c97b789f2c5d10ae30dc147dce32->leave($__internal_79b4c1d4084df04f69e2cdec70458663ddb4c97b789f2c5d10ae30dc147dce32_prof);

    }

    public function getTemplateName()
    {
        return "default/varios/contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 11,  61 => 10,  56 => 8,  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_all.html.twig' %}
{% block body %}
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-10 col-md-offset-1 format-forms color-azul\">
              <div class=\"row\">
                  <div class=\"col-md-11\">
                  <h1>{{title}}</h1>
                  <p>You can contact me using your email or this webpage.</p>
                  {{ form_start(form) }}
                  {{ form_end(form) }}
                </div>
              </div>
            </div>
        </div>
    </div>
{% endblock %}
", "default/varios/contact.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/varios/contact.html.twig");
    }
}
