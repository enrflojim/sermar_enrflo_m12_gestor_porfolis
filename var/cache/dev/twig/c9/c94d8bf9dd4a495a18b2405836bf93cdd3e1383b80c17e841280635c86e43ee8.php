<?php

/* default/portfolis/create_portfoli.html.twig */
class __TwigTemplate_29dfa5d7e2872cd932a13ec0c0d66b58c306a295f2baa4a592f8e8fde62ee3a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_all.html.twig", "default/portfolis/create_portfoli.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_all.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_444e9cd6d51eb3d4c4202be3b4a1f621bed065868c24a8fb9f1faea1ef915450 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_444e9cd6d51eb3d4c4202be3b4a1f621bed065868c24a8fb9f1faea1ef915450->enter($__internal_444e9cd6d51eb3d4c4202be3b4a1f621bed065868c24a8fb9f1faea1ef915450_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/portfolis/create_portfoli.html.twig"));

        $__internal_f334a696a77816c6d2c0d74dc2c7dc7945d4d43bd786853b3042f52edf5525d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f334a696a77816c6d2c0d74dc2c7dc7945d4d43bd786853b3042f52edf5525d1->enter($__internal_f334a696a77816c6d2c0d74dc2c7dc7945d4d43bd786853b3042f52edf5525d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/portfolis/create_portfoli.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_444e9cd6d51eb3d4c4202be3b4a1f621bed065868c24a8fb9f1faea1ef915450->leave($__internal_444e9cd6d51eb3d4c4202be3b4a1f621bed065868c24a8fb9f1faea1ef915450_prof);

        
        $__internal_f334a696a77816c6d2c0d74dc2c7dc7945d4d43bd786853b3042f52edf5525d1->leave($__internal_f334a696a77816c6d2c0d74dc2c7dc7945d4d43bd786853b3042f52edf5525d1_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_9725c600197fe07131d828a500eb0898f716335c28593b3e17e2d4e76a71be35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9725c600197fe07131d828a500eb0898f716335c28593b3e17e2d4e76a71be35->enter($__internal_9725c600197fe07131d828a500eb0898f716335c28593b3e17e2d4e76a71be35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_acb8081e9a5cfee0f7f786c86205eeb2245468e28055ac718abe1c9ebd38990c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_acb8081e9a5cfee0f7f786c86205eeb2245468e28055ac718abe1c9ebd38990c->enter($__internal_acb8081e9a5cfee0f7f786c86205eeb2245468e28055ac718abe1c9ebd38990c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<div id=\"formu\" class=\"row\">
    <div class=\"col-md-6 col-md-offset-3 format-forms color-azul\">
\t\t\t";
        // line 5
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
\t\t\t<fieldset>
\t\t\t\t<legend>";
        // line 7
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</legend>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t  \t\t<div class=\"col-md-9 col-md-offset-1 mostrar\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/images/asd.jpg"), "html", null, true);
        echo "\" width=\"148\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t  \t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<label for=\"form_imgPortfoli\" class=\"btn btn-default largo\">
\t\t\t\t\t\t\t\t\t";
        // line 18
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "imgPortfoli", array()), 'row');
        echo "
\t\t\t\t\t\t\t\t\tAdd image portfoli
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t\t\t";
        // line 27
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "title", array()), 'row');
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-1\">*</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-5\">
\t\t\t\t\t\t\t\t";
        // line 33
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "isPremium", array()), 'row');
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        // line 35
        if (twig_test_empty(($context["free"] ?? $this->getContext($context, "free")))) {
            // line 36
            echo "\t\t\t\t\t\t\t<div class=\"col-md-2\">*</div>
                    \t\t";
        } else {
            // line 38
            echo "\t\t\t\t\t\t\t<div class=\"col-md-2 windowform\">* <span>Only one free portfoli</span></div>
                    \t\t";
        }
        // line 40
        echo "\t\t\t\t\t\t</div>
            <div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-5\">
\t\t\t\t\t\t\t\t";
        // line 43
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "isInformatic", array()), 'row');
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t\t";
        // line 48
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "idCategory", array()), 'row');
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</fieldset>
\t\t\t";
        // line 55
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
\t\t<div>
\t</div>
";
        
        $__internal_acb8081e9a5cfee0f7f786c86205eeb2245468e28055ac718abe1c9ebd38990c->leave($__internal_acb8081e9a5cfee0f7f786c86205eeb2245468e28055ac718abe1c9ebd38990c_prof);

        
        $__internal_9725c600197fe07131d828a500eb0898f716335c28593b3e17e2d4e76a71be35->leave($__internal_9725c600197fe07131d828a500eb0898f716335c28593b3e17e2d4e76a71be35_prof);

    }

    public function getTemplateName()
    {
        return "default/portfolis/create_portfoli.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 55,  124 => 48,  116 => 43,  111 => 40,  107 => 38,  103 => 36,  101 => 35,  96 => 33,  87 => 27,  75 => 18,  66 => 12,  58 => 7,  53 => 5,  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_all.html.twig' %}
{% block body %}
<div id=\"formu\" class=\"row\">
    <div class=\"col-md-6 col-md-offset-3 format-forms color-azul\">
\t\t\t{{ form_start(form) }}
\t\t\t<fieldset>
\t\t\t\t<legend>{{(title)}}</legend>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t  \t\t<div class=\"col-md-9 col-md-offset-1 mostrar\">
\t\t\t\t\t\t\t\t<img src=\"{{asset(\"img/images/asd.jpg\")}}\" width=\"148\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t  \t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<label for=\"form_imgPortfoli\" class=\"btn btn-default largo\">
\t\t\t\t\t\t\t\t\t{{ form_row(form.imgPortfoli) }}
\t\t\t\t\t\t\t\t\tAdd image portfoli
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t\t\t{{ form_row(form.title) }}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-1\">*</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-5\">
\t\t\t\t\t\t\t\t{{ form_row(form.isPremium) }}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t{% if free is empty %}
\t\t\t\t\t\t\t<div class=\"col-md-2\">*</div>
                    \t\t{% else %}
\t\t\t\t\t\t\t<div class=\"col-md-2 windowform\">* <span>Only one free portfoli</span></div>
                    \t\t{% endif %}
\t\t\t\t\t\t</div>
            <div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-5\">
\t\t\t\t\t\t\t\t{{ form_row(form.isInformatic) }}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row line\">
\t\t\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t\t{{ form_row(form.idCategory) }}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-2\">*</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</fieldset>
\t\t\t{{ form_end(form) }}
\t\t<div>
\t</div>
{% endblock %}
", "default/portfolis/create_portfoli.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/portfolis/create_portfoli.html.twig");
    }
}
