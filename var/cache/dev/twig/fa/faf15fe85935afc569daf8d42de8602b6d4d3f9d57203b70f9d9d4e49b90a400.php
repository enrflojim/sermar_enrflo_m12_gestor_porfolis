<?php

/* base.html.twig */
class __TwigTemplate_800f016a509a651c87e7a376a40b2261c57ba78cdaf77ab1a4537ad4f7850fe4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'jumbotron' => array($this, 'block_jumbotron'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e643a140746ab93da0e0b60af6a4bd01bfe5b0b08d49bffa64f48526b59f9d65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e643a140746ab93da0e0b60af6a4bd01bfe5b0b08d49bffa64f48526b59f9d65->enter($__internal_e643a140746ab93da0e0b60af6a4bd01bfe5b0b08d49bffa64f48526b59f9d65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_5ff6cbd984daa2a48107122d1e2af589d9a422b1eaf50e56afa84ba7111370b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ff6cbd984daa2a48107122d1e2af589d9a422b1eaf50e56afa84ba7111370b3->enter($__internal_5ff6cbd984daa2a48107122d1e2af589d9a422b1eaf50e56afa84ba7111370b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/estilos.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/ventana.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("tinymce/js/tinymce/skins/lightgray/content.min.css"), "html", null, true);
        echo "\" />
    <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.4.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/functions.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("tinymce/js/tinymce/tinymce.js"), "html", null, true);
        echo "\"></script>
    <script>tinymce.init({selector: 'textarea'});
    </script>
</head>
<body>
    <nav class=\"navbar navbar-default navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navy-1\"
                        aria-expanded=\"false\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/portfolis\">Portfolis</a>
            </div>
            <!-- Iniciar menu -->
            <div class=\"collapse navbar-collapse\" id=\"navy-1\">
                <ul class=\"nav navbar-nav tt-wrapper\">
                    <li><a href=\"/portfolis\" >Home</a></li>
                        ";
        // line 39
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 40
            echo "                        <li><a href=\"/myportfolis\" >My Porfolis</a></li>
                        <li><a href=\"/createportfoli\" >Create Porfoli</a></li>
                        ";
        } else {
            // line 43
            echo "                        <li class=\"window menu\"><span>You must be logged to wiew portfolis</span><a class=\"color-red\" href=\"#\" >My Porfolis</a></li>
                        <li class=\"window menu\"><span>You must be logged to create portfolis</span><a class=\"color-red\" href=\"#\">Create Porfoli</a></li>
                        ";
        }
        // line 46
        echo "                    <li><a href=\"/contact\" >Contact</a></li>
                </ul>
                <ul class=\"nav navbar-nav navbar-right\">
                    ";
        // line 49
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 50
            echo "                        <li><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\">";
            echo twig_escape_filter($this->env, ($context["user"] ?? $this->getContext($context, "user")), "html", null, true);
            echo "</a>
                            <ul class=\"dropdown-menu\" role=\"menu\">
                                <li ><a href=\"/edituser\">Edit ";
            // line 52
            echo twig_escape_filter($this->env, ($context["user"] ?? $this->getContext($context, "user")), "html", null, true);
            echo "</a></li>
                                <li class=\"divider\"></li>
                                <li ><a href=\"/logout\">Logout</a></li>
                            </ul>
                        </li>
                    ";
        } else {
            // line 58
            echo "                        <li><a href=\"/\">Log in</a></li>
                        <li><a href=\"/register\">Register</a></li>
                        ";
        }
        // line 61
        echo "                    <li>
                        <form action=\"/searchportfolis\" class=\"navbar-form\" role=\"search\" method=\"get\">
                            <div class=\"form-group\">
                                <input type=\"text\" id=\"tag\" name=\"tag\" class=\"form-control\" placeholder=\"Search\">
                            </div>
                            <button type=\"submit\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-search\"></span></button>
                        </form>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <header>
    ";
        // line 74
        $this->displayBlock('jumbotron', $context, $blocks);
        // line 75
        echo "</header>
<section class=\"body\">
    <div class=\"row\">
        <div class=\"col-md-12\">
        ";
        // line 79
        $this->displayBlock('body', $context, $blocks);
        // line 80
        echo "    </div>
</div>
</section>
<footer>
    <div class=\"row\">
            <div class=\"copyright\">Ⓒ Copyright | All rights Reserved | Sergi Martinez & Enrique Flo</div>
    </div>
</footer>
</body>
</html>
";
        
        $__internal_e643a140746ab93da0e0b60af6a4bd01bfe5b0b08d49bffa64f48526b59f9d65->leave($__internal_e643a140746ab93da0e0b60af6a4bd01bfe5b0b08d49bffa64f48526b59f9d65_prof);

        
        $__internal_5ff6cbd984daa2a48107122d1e2af589d9a422b1eaf50e56afa84ba7111370b3->leave($__internal_5ff6cbd984daa2a48107122d1e2af589d9a422b1eaf50e56afa84ba7111370b3_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_16e6f59439da3c0ad5c60208aa79023a6b9c047dc657fe497999892266a16224 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16e6f59439da3c0ad5c60208aa79023a6b9c047dc657fe497999892266a16224->enter($__internal_16e6f59439da3c0ad5c60208aa79023a6b9c047dc657fe497999892266a16224_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_7f0ca6e0fd90fc44891a2ea00775ff622c945cbfd1c62f208b7b5c5f9d866b1f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f0ca6e0fd90fc44891a2ea00775ff622c945cbfd1c62f208b7b5c5f9d866b1f->enter($__internal_7f0ca6e0fd90fc44891a2ea00775ff622c945cbfd1c62f208b7b5c5f9d866b1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_7f0ca6e0fd90fc44891a2ea00775ff622c945cbfd1c62f208b7b5c5f9d866b1f->leave($__internal_7f0ca6e0fd90fc44891a2ea00775ff622c945cbfd1c62f208b7b5c5f9d866b1f_prof);

        
        $__internal_16e6f59439da3c0ad5c60208aa79023a6b9c047dc657fe497999892266a16224->leave($__internal_16e6f59439da3c0ad5c60208aa79023a6b9c047dc657fe497999892266a16224_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f7385f023477637f883fa53feeac441cad46d2ed0555b4fa26095161b92361b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7385f023477637f883fa53feeac441cad46d2ed0555b4fa26095161b92361b9->enter($__internal_f7385f023477637f883fa53feeac441cad46d2ed0555b4fa26095161b92361b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_8acb7146ee4e511a3be1e5238c331c13ce0d41369cb52be241adb166ba5efab3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8acb7146ee4e511a3be1e5238c331c13ce0d41369cb52be241adb166ba5efab3->enter($__internal_8acb7146ee4e511a3be1e5238c331c13ce0d41369cb52be241adb166ba5efab3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_8acb7146ee4e511a3be1e5238c331c13ce0d41369cb52be241adb166ba5efab3->leave($__internal_8acb7146ee4e511a3be1e5238c331c13ce0d41369cb52be241adb166ba5efab3_prof);

        
        $__internal_f7385f023477637f883fa53feeac441cad46d2ed0555b4fa26095161b92361b9->leave($__internal_f7385f023477637f883fa53feeac441cad46d2ed0555b4fa26095161b92361b9_prof);

    }

    // line 74
    public function block_jumbotron($context, array $blocks = array())
    {
        $__internal_aa0e0b7bcfb8e013926d2f3274e3d0703b766c5cbbd9dceb3bcdff91c0f6525d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa0e0b7bcfb8e013926d2f3274e3d0703b766c5cbbd9dceb3bcdff91c0f6525d->enter($__internal_aa0e0b7bcfb8e013926d2f3274e3d0703b766c5cbbd9dceb3bcdff91c0f6525d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "jumbotron"));

        $__internal_1daa09fccb2e3fbf4cf06bc829cd6cdefd996016431d881c627c7ca657934287 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1daa09fccb2e3fbf4cf06bc829cd6cdefd996016431d881c627c7ca657934287->enter($__internal_1daa09fccb2e3fbf4cf06bc829cd6cdefd996016431d881c627c7ca657934287_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "jumbotron"));

        
        $__internal_1daa09fccb2e3fbf4cf06bc829cd6cdefd996016431d881c627c7ca657934287->leave($__internal_1daa09fccb2e3fbf4cf06bc829cd6cdefd996016431d881c627c7ca657934287_prof);

        
        $__internal_aa0e0b7bcfb8e013926d2f3274e3d0703b766c5cbbd9dceb3bcdff91c0f6525d->leave($__internal_aa0e0b7bcfb8e013926d2f3274e3d0703b766c5cbbd9dceb3bcdff91c0f6525d_prof);

    }

    // line 79
    public function block_body($context, array $blocks = array())
    {
        $__internal_bd113a9cd0101ef6a1beda016f5a07d5bf67cfd49793a01d60ce151e6e6f251d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd113a9cd0101ef6a1beda016f5a07d5bf67cfd49793a01d60ce151e6e6f251d->enter($__internal_bd113a9cd0101ef6a1beda016f5a07d5bf67cfd49793a01d60ce151e6e6f251d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_5e4408c1b60149979b26523fe002b8c1dc371e6c0297d609aed57cb3e036a423 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e4408c1b60149979b26523fe002b8c1dc371e6c0297d609aed57cb3e036a423->enter($__internal_5e4408c1b60149979b26523fe002b8c1dc371e6c0297d609aed57cb3e036a423_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_5e4408c1b60149979b26523fe002b8c1dc371e6c0297d609aed57cb3e036a423->leave($__internal_5e4408c1b60149979b26523fe002b8c1dc371e6c0297d609aed57cb3e036a423_prof);

        
        $__internal_bd113a9cd0101ef6a1beda016f5a07d5bf67cfd49793a01d60ce151e6e6f251d->leave($__internal_bd113a9cd0101ef6a1beda016f5a07d5bf67cfd49793a01d60ce151e6e6f251d_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  243 => 79,  226 => 74,  209 => 6,  191 => 5,  171 => 80,  169 => 79,  163 => 75,  161 => 74,  146 => 61,  141 => 58,  132 => 52,  126 => 50,  124 => 49,  119 => 46,  114 => 43,  109 => 40,  107 => 39,  82 => 17,  78 => 16,  74 => 15,  70 => 14,  66 => 13,  62 => 12,  58 => 11,  54 => 10,  50 => 9,  46 => 8,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
    {% block stylesheets %}{% endblock %}
    <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/bootstrap.css')}}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/jquery-ui.css')}}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/estilos.css')}}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/ventana.css')}}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('tinymce/js/tinymce/skins/lightgray/content.min.css')}}\" />
    <script src=\"{{ asset('js/jquery-2.2.4.js')}}\"></script>
    <script src=\"{{ asset('js/bootstrap.js')}}\"></script>
    <script src=\"{{ asset('js/jquery-ui.js')}}\"></script>
    <script src=\"{{ asset('js/functions.js')}}\"></script>
    <script src=\"{{ asset('tinymce/js/tinymce/tinymce.js')}}\"></script>
    <script>tinymce.init({selector: 'textarea'});
    </script>
</head>
<body>
    <nav class=\"navbar navbar-default navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navy-1\"
                        aria-expanded=\"false\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/portfolis\">Portfolis</a>
            </div>
            <!-- Iniciar menu -->
            <div class=\"collapse navbar-collapse\" id=\"navy-1\">
                <ul class=\"nav navbar-nav tt-wrapper\">
                    <li><a href=\"/portfolis\" >Home</a></li>
                        {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                        <li><a href=\"/myportfolis\" >My Porfolis</a></li>
                        <li><a href=\"/createportfoli\" >Create Porfoli</a></li>
                        {% else %}
                        <li class=\"window menu\"><span>You must be logged to wiew portfolis</span><a class=\"color-red\" href=\"#\" >My Porfolis</a></li>
                        <li class=\"window menu\"><span>You must be logged to create portfolis</span><a class=\"color-red\" href=\"#\">Create Porfoli</a></li>
                        {% endif %}
                    <li><a href=\"/contact\" >Contact</a></li>
                </ul>
                <ul class=\"nav navbar-nav navbar-right\">
                    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                        <li><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\">{{(user)}}</a>
                            <ul class=\"dropdown-menu\" role=\"menu\">
                                <li ><a href=\"/edituser\">Edit {{(user)}}</a></li>
                                <li class=\"divider\"></li>
                                <li ><a href=\"/logout\">Logout</a></li>
                            </ul>
                        </li>
                    {% else %}
                        <li><a href=\"/\">Log in</a></li>
                        <li><a href=\"/register\">Register</a></li>
                        {% endif %}
                    <li>
                        <form action=\"/searchportfolis\" class=\"navbar-form\" role=\"search\" method=\"get\">
                            <div class=\"form-group\">
                                <input type=\"text\" id=\"tag\" name=\"tag\" class=\"form-control\" placeholder=\"Search\">
                            </div>
                            <button type=\"submit\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-search\"></span></button>
                        </form>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <header>
    {% block jumbotron %}{% endblock %}
</header>
<section class=\"body\">
    <div class=\"row\">
        <div class=\"col-md-12\">
        {% block body %}{% endblock %}
    </div>
</div>
</section>
<footer>
    <div class=\"row\">
            <div class=\"copyright\">Ⓒ Copyright | All rights Reserved | Sergi Martinez & Enrique Flo</div>
    </div>
</footer>
</body>
</html>
", "base.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/base.html.twig");
    }
}
