<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_95ec65c7290c52075ed18a8c3c04926e36b0a7a6cee6d118ce9baeecded57a18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2924c6f90268095e6ad94d02a34fa7dd0223d4464abe92a2b387071abf4d49cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2924c6f90268095e6ad94d02a34fa7dd0223d4464abe92a2b387071abf4d49cd->enter($__internal_2924c6f90268095e6ad94d02a34fa7dd0223d4464abe92a2b387071abf4d49cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        $__internal_966153ef0c9731ccad02f28538f7d6642c88751d4eddf5bf7d09c980cba48832 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_966153ef0c9731ccad02f28538f7d6642c88751d4eddf5bf7d09c980cba48832->enter($__internal_966153ef0c9731ccad02f28538f7d6642c88751d4eddf5bf7d09c980cba48832_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_2924c6f90268095e6ad94d02a34fa7dd0223d4464abe92a2b387071abf4d49cd->leave($__internal_2924c6f90268095e6ad94d02a34fa7dd0223d4464abe92a2b387071abf4d49cd_prof);

        
        $__internal_966153ef0c9731ccad02f28538f7d6642c88751d4eddf5bf7d09c980cba48832->leave($__internal_966153ef0c9731ccad02f28538f7d6642c88751d4eddf5bf7d09c980cba48832_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  31 => 3,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if exception.trace|length %}
{% for trace in exception.trace %}
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

{% endfor %}
{% endif %}
", "@Twig/Exception/traces.txt.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.txt.twig");
    }
}
