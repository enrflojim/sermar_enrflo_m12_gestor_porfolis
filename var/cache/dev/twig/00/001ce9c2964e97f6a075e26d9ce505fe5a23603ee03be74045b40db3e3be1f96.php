<?php

/* default/portfolis/portfoli.html.twig */
class __TwigTemplate_a9398efddd96d6cbd00deb337696f940de9e222f398adc77a6d74aa61bc21ad3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/portfolis/portfoli.html.twig", 1);
        $this->blocks = array(
            'stylesheet' => array($this, 'block_stylesheet'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9eef88e3b70e8b84857eb7e27d1e0573e977e4193af5668fea678f0f56d239b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9eef88e3b70e8b84857eb7e27d1e0573e977e4193af5668fea678f0f56d239b3->enter($__internal_9eef88e3b70e8b84857eb7e27d1e0573e977e4193af5668fea678f0f56d239b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/portfolis/portfoli.html.twig"));

        $__internal_ae1ccd7c4f58d1a377b031fc7cd5ec41ebe1b3a449db86c4ca6a0010fceaf8cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae1ccd7c4f58d1a377b031fc7cd5ec41ebe1b3a449db86c4ca6a0010fceaf8cc->enter($__internal_ae1ccd7c4f58d1a377b031fc7cd5ec41ebe1b3a449db86c4ca6a0010fceaf8cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/portfolis/portfoli.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9eef88e3b70e8b84857eb7e27d1e0573e977e4193af5668fea678f0f56d239b3->leave($__internal_9eef88e3b70e8b84857eb7e27d1e0573e977e4193af5668fea678f0f56d239b3_prof);

        
        $__internal_ae1ccd7c4f58d1a377b031fc7cd5ec41ebe1b3a449db86c4ca6a0010fceaf8cc->leave($__internal_ae1ccd7c4f58d1a377b031fc7cd5ec41ebe1b3a449db86c4ca6a0010fceaf8cc_prof);

    }

    // line 3
    public function block_stylesheet($context, array $blocks = array())
    {
        $__internal_56f0246fc8c920039073f72faf04e2a82d48edea8a060b582114dd1fa78872f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56f0246fc8c920039073f72faf04e2a82d48edea8a060b582114dd1fa78872f1->enter($__internal_56f0246fc8c920039073f72faf04e2a82d48edea8a060b582114dd1fa78872f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheet"));

        $__internal_f11f26720aa69f952a4d7060fe6354f221e2954c3a04c2c3545acbfc76a9c727 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f11f26720aa69f952a4d7060fe6354f221e2954c3a04c2c3545acbfc76a9c727->enter($__internal_f11f26720aa69f952a4d7060fe6354f221e2954c3a04c2c3545acbfc76a9c727_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheet"));

        // line 4
        echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/listCards.css"), "html", null, true);
        echo "\" />
";
        
        $__internal_f11f26720aa69f952a4d7060fe6354f221e2954c3a04c2c3545acbfc76a9c727->leave($__internal_f11f26720aa69f952a4d7060fe6354f221e2954c3a04c2c3545acbfc76a9c727_prof);

        
        $__internal_56f0246fc8c920039073f72faf04e2a82d48edea8a060b582114dd1fa78872f1->leave($__internal_56f0246fc8c920039073f72faf04e2a82d48edea8a060b582114dd1fa78872f1_prof);

    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        $__internal_ed95a82a829ab295955cba7907ace80ce8c94be331e2b9306b21b6e76cf1c392 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed95a82a829ab295955cba7907ace80ce8c94be331e2b9306b21b6e76cf1c392->enter($__internal_ed95a82a829ab295955cba7907ace80ce8c94be331e2b9306b21b6e76cf1c392_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_18e2d18764e46069e334c5b0e972ab8ef548bb3e2c8623c510f92b11119c62ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18e2d18764e46069e334c5b0e972ab8ef548bb3e2c8623c510f92b11119c62ef->enter($__internal_18e2d18764e46069e334c5b0e972ab8ef548bb3e2c8623c510f92b11119c62ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 9
        echo "    <div class=\"row\">
        <div class=\"col-md-2 col-md-offset-1\">
            <h3>Cards</h3>
            <div class=\"panel-group\" id=\"acordeon\">
                ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sub"] ?? $this->getContext($context, "sub")));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 14
            echo "                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#acordeon\" href=\"#collapse";
            // line 17
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "</a>
                            </h4>
                        </div>
                        <div id=\"collapse";
            // line 20
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "\" class=\"panel-collapse collapse\">
                            ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "cards", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
                // line 22
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["card"], "subCategories", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["subcategories"]) {
                    // line 23
                    echo "                                    ";
                    if (($this->getAttribute($context["subcategories"], "nameSubcategory", array()) == $context["s"])) {
                        // line 24
                        echo "                                        <div class=\"panel-body\"><a href=\"/card/";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "id", array()), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "title", array()), "html", null, true);
                        echo "</a></div>
                                        ";
                    }
                    // line 26
                    echo "                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcategories'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 27
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "            </div>
        </div>
        <div class=\"col-md-8\">
            ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "cards", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
            // line 35
            echo "                ";
            $context["date"] = twig_split_filter($this->env, $this->getAttribute($context["card"], "dateStringCard", array()), ",");
            // line 36
            echo "                <div class=\"row\">
                    <div class=\"col-md-12 card\">
                        <div class=\"row\">
                            <div class=\"col-md-2\">
                                <div class=\"centro tope format-date\">
                                    ";
            // line 41
            $context["i"] = 0;
            // line 42
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["date"] ?? $this->getContext($context, "date")));
            foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
                // line 43
                echo "                                        ";
                if ((($context["i"] ?? $this->getContext($context, "i")) == 0)) {
                    // line 44
                    echo "                                            <div class=\"date-card-form\">";
                    echo twig_escape_filter($this->env, $context["d"], "html", null, true);
                    echo "</div>
                                        ";
                } else {
                    // line 46
                    echo "                                            <div class=\"date-card-form-year\">";
                    echo twig_escape_filter($this->env, $context["d"], "html", null, true);
                    echo "</div>
                                        ";
                }
                // line 48
                echo "                                        ";
                $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
                // line 49
                echo "                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "                                </div>
                            </div>
                            <div class=\"col-md-10 card\">
                                <h2>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "title", array()), "html", null, true);
            echo "</h2>
                            </div>
                        </div>
                        <hr class=\"remarcar\">
                        <p><span class=\"glyphicon glyphicon-user\"></span> by
                            <i>";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["card"], "idPortfoli", array()), "idUser", array()), "name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["card"], "idPortfoli", array()), "idUser", array()), "surname", array()), "html", null, true);
            echo "</i>
                        </p>
                        ";
            // line 60
            echo $this->getAttribute($context["card"], "description", array());
            echo "
                        <a class=\"col-md-offset-10 btn btn-primary\" href=\"/card/";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "id", array()), "html", null, true);
            echo "\">Read more</a>
                        <br/>
                        <br/>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "        </div>
    </div>
";
        
        $__internal_18e2d18764e46069e334c5b0e972ab8ef548bb3e2c8623c510f92b11119c62ef->leave($__internal_18e2d18764e46069e334c5b0e972ab8ef548bb3e2c8623c510f92b11119c62ef_prof);

        
        $__internal_ed95a82a829ab295955cba7907ace80ce8c94be331e2b9306b21b6e76cf1c392->leave($__internal_ed95a82a829ab295955cba7907ace80ce8c94be331e2b9306b21b6e76cf1c392_prof);

    }

    public function getTemplateName()
    {
        return "default/portfolis/portfoli.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 67,  213 => 61,  209 => 60,  202 => 58,  194 => 53,  189 => 50,  183 => 49,  180 => 48,  174 => 46,  168 => 44,  165 => 43,  160 => 42,  158 => 41,  151 => 36,  148 => 35,  144 => 34,  139 => 31,  131 => 28,  125 => 27,  119 => 26,  111 => 24,  108 => 23,  103 => 22,  99 => 21,  95 => 20,  87 => 17,  82 => 14,  78 => 13,  72 => 9,  63 => 8,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}

{% block stylesheet %}
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/listCards.css')}}\" />
{% endblock %}


{% block body %}
    <div class=\"row\">
        <div class=\"col-md-2 col-md-offset-1\">
            <h3>Cards</h3>
            <div class=\"panel-group\" id=\"acordeon\">
                {% for s in sub %}
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#acordeon\" href=\"#collapse{{(s)}}\">{{(s)}}</a>
                            </h4>
                        </div>
                        <div id=\"collapse{{(s)}}\" class=\"panel-collapse collapse\">
                            {% for card in portfoli.cards %}
                                {% for subcategories in card.subCategories %}
                                    {% if subcategories.nameSubcategory == s %}
                                        <div class=\"panel-body\"><a href=\"/card/{{(card.id)}}\">{{(card.title)}}</a></div>
                                        {% endif %}
                                    {% endfor %}
                                {% endfor %}
                        </div>
                    </div>
                {% endfor %}
            </div>
        </div>
        <div class=\"col-md-8\">
            {% for card in portfoli.cards %}
                {% set date = card.dateStringCard|split(',') %}
                <div class=\"row\">
                    <div class=\"col-md-12 card\">
                        <div class=\"row\">
                            <div class=\"col-md-2\">
                                <div class=\"centro tope format-date\">
                                    {% set i = 0 %}
                                    {% for d in date %}
                                        {% if i == 0 %}
                                            <div class=\"date-card-form\">{{(d)}}</div>
                                        {% else %}
                                            <div class=\"date-card-form-year\">{{(d)}}</div>
                                        {% endif %}
                                        {% set i = i+1 %}
                                    {% endfor %}
                                </div>
                            </div>
                            <div class=\"col-md-10 card\">
                                <h2>{{card.title}}</h2>
                            </div>
                        </div>
                        <hr class=\"remarcar\">
                        <p><span class=\"glyphicon glyphicon-user\"></span> by
                            <i>{{(card.idPortfoli.idUser.name)}} {{(card.idPortfoli.idUser.surname)}}</i>
                        </p>
                        {{(card.description|raw)}}
                        <a class=\"col-md-offset-10 btn btn-primary\" href=\"/card/{{card.id}}\">Read more</a>
                        <br/>
                        <br/>
                    </div>
                </div>
            {% endfor %}
        </div>
    </div>
{% endblock %}
", "default/portfolis/portfoli.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/portfolis/portfoli.html.twig");
    }
}
