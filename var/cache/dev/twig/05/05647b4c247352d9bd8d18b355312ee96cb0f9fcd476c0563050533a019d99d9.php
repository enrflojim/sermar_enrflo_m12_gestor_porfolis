<?php

/* default/cardTemplates/allCard.html.twig */
class __TwigTemplate_2fa763b3cf39c267158bb73c0697102a509400c2fb26b2c0c48240e19777e531 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/cardTemplates/allCard.html.twig", 1);
        $this->blocks = array(
            'javascript' => array($this, 'block_javascript'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_328cb55fc282435bc446e0ad99d7ba4cae161fe2ab147983f29a2c1a55e69532 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_328cb55fc282435bc446e0ad99d7ba4cae161fe2ab147983f29a2c1a55e69532->enter($__internal_328cb55fc282435bc446e0ad99d7ba4cae161fe2ab147983f29a2c1a55e69532_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/cardTemplates/allCard.html.twig"));

        $__internal_1df84bc6b7775bc774b50a0ceef7a17301deae45a24ca2da5c5b27e6b42a1503 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1df84bc6b7775bc774b50a0ceef7a17301deae45a24ca2da5c5b27e6b42a1503->enter($__internal_1df84bc6b7775bc774b50a0ceef7a17301deae45a24ca2da5c5b27e6b42a1503_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/cardTemplates/allCard.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_328cb55fc282435bc446e0ad99d7ba4cae161fe2ab147983f29a2c1a55e69532->leave($__internal_328cb55fc282435bc446e0ad99d7ba4cae161fe2ab147983f29a2c1a55e69532_prof);

        
        $__internal_1df84bc6b7775bc774b50a0ceef7a17301deae45a24ca2da5c5b27e6b42a1503->leave($__internal_1df84bc6b7775bc774b50a0ceef7a17301deae45a24ca2da5c5b27e6b42a1503_prof);

    }

    // line 3
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_4a758e5a27a1ae2acbd280967afa024ffb2504844c23569617dea62c7bc00a32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a758e5a27a1ae2acbd280967afa024ffb2504844c23569617dea62c7bc00a32->enter($__internal_4a758e5a27a1ae2acbd280967afa024ffb2504844c23569617dea62c7bc00a32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        $__internal_9580b44c3184502d9f5b1c9fd02a98bdc9d46e5f6d72750ad03c64f9add81cfc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9580b44c3184502d9f5b1c9fd02a98bdc9d46e5f6d72750ad03c64f9add81cfc->enter($__internal_9580b44c3184502d9f5b1c9fd02a98bdc9d46e5f6d72750ad03c64f9add81cfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 4
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/slider.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_9580b44c3184502d9f5b1c9fd02a98bdc9d46e5f6d72750ad03c64f9add81cfc->leave($__internal_9580b44c3184502d9f5b1c9fd02a98bdc9d46e5f6d72750ad03c64f9add81cfc_prof);

        
        $__internal_4a758e5a27a1ae2acbd280967afa024ffb2504844c23569617dea62c7bc00a32->leave($__internal_4a758e5a27a1ae2acbd280967afa024ffb2504844c23569617dea62c7bc00a32_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_4ce1f57891b7c590a1d8ae10d01e68e11c7937d4ff1a494aa5ad0663bb475ce1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ce1f57891b7c590a1d8ae10d01e68e11c7937d4ff1a494aa5ad0663bb475ce1->enter($__internal_4ce1f57891b7c590a1d8ae10d01e68e11c7937d4ff1a494aa5ad0663bb475ce1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_267430a278ef58de5b095b60565713d83c297b3fb6f918ab08ef33c17b184bac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_267430a278ef58de5b095b60565713d83c297b3fb6f918ab08ef33c17b184bac->enter($__internal_267430a278ef58de5b095b60565713d83c297b3fb6f918ab08ef33c17b184bac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <div class=\"row\">
        <div class=\"col-md-2 col-md-offset-1\">
            <h3>Cards</h3>
            <div class=\"panel-group\" id=\"acordeon\">
                ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sub"] ?? $this->getContext($context, "sub")));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 13
            echo "                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#acordeon\" href=\"#collapse";
            // line 16
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "</a>
                            </h4>
                        </div>
                        <div id=\"collapse";
            // line 19
            echo twig_escape_filter($this->env, $context["s"], "html", null, true);
            echo "\" class=\"panel-collapse collapse\">
                            ";
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "cards", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
                // line 21
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["card"], "subCategories", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["subcategories"]) {
                    // line 22
                    echo "                                    ";
                    if (($this->getAttribute($context["subcategories"], "nameSubcategory", array()) == $context["s"])) {
                        // line 23
                        echo "                                        <div class=\"panel-body\"><a href=\"/card/";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "id", array()), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["card"], "title", array()), "html", null, true);
                        echo "</a></div>
                                        ";
                    }
                    // line 25
                    echo "                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcategories'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 26
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "            </div>
        </div>
        <div class=\"col-md-8\">
            <div class=\"row\">
                <div class=\"col-md-12 format-forms color-azul\">
                    <div class=\"row\">
                        <div class=\"col-md-11\">
                            <h1>";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "title", array()), "html", null, true);
        echo "</h1>
                            ";
        // line 38
        echo $this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "description", array());
        echo "
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-10 col-md-offset-1\">
                            ";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "articles", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 44
            echo "                                <article>
                                    <h3>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "title", array()), "html", null, true);
            echo "</h3>
                                    <p>";
            // line 46
            echo $this->getAttribute($context["article"], "content", array());
            echo "</p>
                                    <div class=\"row\">
                                        <div class=\"col-md-8 col-md-offset-2\">
                                            <div id=\"myCarousel";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
            echo "\" class=\"carousel slide\" data-ride=\"carousel\">
                                                <!-- Wrapper for slides -->
                                                <div class=\"carousel-inner\">
                                                    <!-- Items -->
                                                    ";
            // line 53
            $context["i"] = 0;
            // line 54
            echo "                                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["article"], "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 55
                echo "                                                        ";
                if ((($context["i"] ?? $this->getContext($context, "i")) == 0)) {
                    // line 56
                    echo "                                                            <div class=\"item active\">
                                                            ";
                } else {
                    // line 58
                    echo "                                                                <div class=\"item\">
                                                                ";
                }
                // line 60
                echo "                                                                <img width=\"500\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/articles/" . $this->getAttribute($context["image"], "image", array()))), "html", null, true);
                echo "\">
                                                                <div class=\"carousel-caption\">
                                                                    <h3>";
                // line 62
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo "</h3>
                                                                    <a hred=\"#\" class=\"btn btn-primary btn-xs\" data-toggle=\"modal\" data-target=\"#myModal";
                // line 63
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                echo "\">Read me</a>
                                                                </div>
                                                            </div>
                                                            ";
                // line 66
                $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
                // line 67
                echo "                                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo "                                                        <!-- End Items -->
                                                    </div>
                                                    <ul class=\"nav nav-pills nav-justified\">
                                                        ";
            // line 71
            $context["i"] = 0;
            // line 72
            echo "                                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["article"], "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 73
                echo "                                                            ";
                if ((($context["i"] ?? $this->getContext($context, "i")) == 0)) {
                    // line 74
                    echo "                                                                <li data-target=\"#myCarousel";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
                    echo "\" data-slide-to=\"";
                    echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                    echo "\" class=\"active\">
                                                                ";
                } else {
                    // line 76
                    echo "                                                                <li data-target=\"#myCarousel";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
                    echo "\" data-slide-to=\"";
                    echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                    echo "\">
                                                                ";
                }
                // line 78
                echo "                                                                <a href=\"#\">
                                                                    <small><img width=\"25\" class=\"barra\" src=\"";
                // line 79
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/articles/" . $this->getAttribute($context["image"], "image", array()))), "html", null, true);
                echo "\"></small>
                                                                </a>
                                                            </li>
                                                            ";
                // line 82
                $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
                // line 83
                echo "                                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 84
            echo "                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                </article>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 91
        echo "                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    ";
        // line 98
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["card"] ?? $this->getContext($context, "card")), "articles", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 99
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["article"], "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 100
                echo "    <div class=\"modal fade\" id=\"myModal";
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                echo "\" role=\"dialog\">
        <div class=\"modal-dialog tam-modal\">

            <!-- Modal content-->
            <div class=\"modal-content\">
                <div class=\"modal-header centro\">
                    <h2 class=\"modal-title\">";
                // line 106
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo "</h2>
                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-8\">
                            <img width=\"700\" src=\"";
                // line 111
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/articles/" . $this->getAttribute($context["image"], "image", array()))), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo "\"
                                 alt=\"";
                // line 112
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "title", array()), "html", null, true);
                echo " not found\">
                        </div>
                        <div class=\"col-md-4\">
                            ";
                // line 115
                echo $this->getAttribute($context["image"], "description", array());
                echo "
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 124
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_267430a278ef58de5b095b60565713d83c297b3fb6f918ab08ef33c17b184bac->leave($__internal_267430a278ef58de5b095b60565713d83c297b3fb6f918ab08ef33c17b184bac_prof);

        
        $__internal_4ce1f57891b7c590a1d8ae10d01e68e11c7937d4ff1a494aa5ad0663bb475ce1->leave($__internal_4ce1f57891b7c590a1d8ae10d01e68e11c7937d4ff1a494aa5ad0663bb475ce1_prof);

    }

    public function getTemplateName()
    {
        return "default/cardTemplates/allCard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  349 => 124,  334 => 115,  328 => 112,  322 => 111,  314 => 106,  304 => 100,  299 => 99,  295 => 98,  286 => 91,  274 => 84,  268 => 83,  266 => 82,  260 => 79,  257 => 78,  249 => 76,  241 => 74,  238 => 73,  233 => 72,  231 => 71,  226 => 68,  220 => 67,  218 => 66,  212 => 63,  208 => 62,  202 => 60,  198 => 58,  194 => 56,  191 => 55,  186 => 54,  184 => 53,  177 => 49,  171 => 46,  167 => 45,  164 => 44,  160 => 43,  152 => 38,  148 => 37,  139 => 30,  131 => 27,  125 => 26,  119 => 25,  111 => 23,  108 => 22,  103 => 21,  99 => 20,  95 => 19,  87 => 16,  82 => 13,  78 => 12,  72 => 8,  63 => 7,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}

{% block javascript %}
    <script src=\"{{ asset('js/slider.js')}}\"></script>
{% endblock %}

{% block body %}
    <div class=\"row\">
        <div class=\"col-md-2 col-md-offset-1\">
            <h3>Cards</h3>
            <div class=\"panel-group\" id=\"acordeon\">
                {% for s in sub %}
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#acordeon\" href=\"#collapse{{(s)}}\">{{(s)}}</a>
                            </h4>
                        </div>
                        <div id=\"collapse{{(s)}}\" class=\"panel-collapse collapse\">
                            {% for card in portfoli.cards %}
                                {% for subcategories in card.subCategories %}
                                    {% if subcategories.nameSubcategory == s %}
                                        <div class=\"panel-body\"><a href=\"/card/{{(card.id)}}\">{{(card.title)}}</a></div>
                                        {% endif %}
                                    {% endfor %}
                                {% endfor %}
                        </div>
                    </div>
                {% endfor %}
            </div>
        </div>
        <div class=\"col-md-8\">
            <div class=\"row\">
                <div class=\"col-md-12 format-forms color-azul\">
                    <div class=\"row\">
                        <div class=\"col-md-11\">
                            <h1>{{card.title}}</h1>
                            {{card.description|raw}}
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-10 col-md-offset-1\">
                            {% for article in card.articles %}
                                <article>
                                    <h3>{{article.title}}</h3>
                                    <p>{{article.content|raw}}</p>
                                    <div class=\"row\">
                                        <div class=\"col-md-8 col-md-offset-2\">
                                            <div id=\"myCarousel{{(article.id)}}\" class=\"carousel slide\" data-ride=\"carousel\">
                                                <!-- Wrapper for slides -->
                                                <div class=\"carousel-inner\">
                                                    <!-- Items -->
                                                    {% set i=0 %}
                                                    {% for image in article.images %}
                                                        {% if i==0 %}
                                                            <div class=\"item active\">
                                                            {% else %}
                                                                <div class=\"item\">
                                                                {% endif %}
                                                                <img width=\"500\" src=\"{{asset('img/articles/'~image.image)}}\">
                                                                <div class=\"carousel-caption\">
                                                                    <h3>{{(image.title)}}</h3>
                                                                    <a hred=\"#\" class=\"btn btn-primary btn-xs\" data-toggle=\"modal\" data-target=\"#myModal{{(image.id)}}\">Read me</a>
                                                                </div>
                                                            </div>
                                                            {% set i=i+1 %}
                                                        {% endfor %}
                                                        <!-- End Items -->
                                                    </div>
                                                    <ul class=\"nav nav-pills nav-justified\">
                                                        {% set i=0 %}
                                                        {% for image in article.images %}
                                                            {% if i==0 %}
                                                                <li data-target=\"#myCarousel{{(article.id)}}\" data-slide-to=\"{{(i)}}\" class=\"active\">
                                                                {% else %}
                                                                <li data-target=\"#myCarousel{{(article.id)}}\" data-slide-to=\"{{(i)}}\">
                                                                {% endif %}
                                                                <a href=\"#\">
                                                                    <small><img width=\"25\" class=\"barra\" src=\"{{asset('img/articles/'~image.image)}}\"></small>
                                                                </a>
                                                            </li>
                                                            {% set i=i+1 %}
                                                        {% endfor %}
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                </article>
                            {% endfor %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    {% for article in card.articles %}
    {% for image in article.images %}
    <div class=\"modal fade\" id=\"myModal{{(image.id)}}\" role=\"dialog\">
        <div class=\"modal-dialog tam-modal\">

            <!-- Modal content-->
            <div class=\"modal-content\">
                <div class=\"modal-header centro\">
                    <h2 class=\"modal-title\">{{(image.title)}}</h2>
                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-8\">
                            <img width=\"700\" src=\"{{ asset('img/articles/'~image.image)}}\" title=\"{{(image.title)}}\"
                                 alt=\"{{(image.title)}} not found\">
                        </div>
                        <div class=\"col-md-4\">
                            {{(image.description|raw)}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    {% endfor %}
    {% endfor %}
{% endblock %}
", "default/cardTemplates/allCard.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/cardTemplates/allCard.html.twig");
    }
}
