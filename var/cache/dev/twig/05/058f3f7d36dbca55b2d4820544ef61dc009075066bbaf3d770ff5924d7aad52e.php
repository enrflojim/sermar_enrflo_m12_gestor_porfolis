<?php

/* default/header/header_portfoli.html.twig */
class __TwigTemplate_cd08b2b01f803758c17d507c36bceb6d683f4f0961696f6f6a3454508fa9d8e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/header/header_portfoli.html.twig", 1);
        $this->blocks = array(
            'jumbotron' => array($this, 'block_jumbotron'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bcad27b0450423255e854b64c612f132c2bdde2d528b3c42dc091b34998208fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bcad27b0450423255e854b64c612f132c2bdde2d528b3c42dc091b34998208fc->enter($__internal_bcad27b0450423255e854b64c612f132c2bdde2d528b3c42dc091b34998208fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/header/header_portfoli.html.twig"));

        $__internal_f8a0ddfa583df4d7eaddc7fd3b32c645b971b3f006164e0cce7dc1c537d11260 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8a0ddfa583df4d7eaddc7fd3b32c645b971b3f006164e0cce7dc1c537d11260->enter($__internal_f8a0ddfa583df4d7eaddc7fd3b32c645b971b3f006164e0cce7dc1c537d11260_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/header/header_portfoli.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bcad27b0450423255e854b64c612f132c2bdde2d528b3c42dc091b34998208fc->leave($__internal_bcad27b0450423255e854b64c612f132c2bdde2d528b3c42dc091b34998208fc_prof);

        
        $__internal_f8a0ddfa583df4d7eaddc7fd3b32c645b971b3f006164e0cce7dc1c537d11260->leave($__internal_f8a0ddfa583df4d7eaddc7fd3b32c645b971b3f006164e0cce7dc1c537d11260_prof);

    }

    // line 2
    public function block_jumbotron($context, array $blocks = array())
    {
        $__internal_587fe4b244f361c9056e79f56e7bd87055980a86dfb7d6e6eccb524ac07ba46e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_587fe4b244f361c9056e79f56e7bd87055980a86dfb7d6e6eccb524ac07ba46e->enter($__internal_587fe4b244f361c9056e79f56e7bd87055980a86dfb7d6e6eccb524ac07ba46e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "jumbotron"));

        $__internal_05c40904b17a606645a5bd2aac86713a25d83dbc3f85562ce636ea5763260cee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_05c40904b17a606645a5bd2aac86713a25d83dbc3f85562ce636ea5763260cee->enter($__internal_05c40904b17a606645a5bd2aac86713a25d83dbc3f85562ce636ea5763260cee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "jumbotron"));

        // line 3
        echo "    <!-- Jumbotron -->
    <div class=\"jumbotron color-azul\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-8 col-md-offset-2 centro\">
                    <h1 class=\"cg\">";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "title", array()), "html", null, true);
        echo "</h1>
                </div>
            </div>
            ";
        // line 11
        if ((($context["rute"] ?? $this->getContext($context, "rute")) == "user")) {
            // line 12
            echo "                <div class=\"row\">
                    <div class=\"col-md-2 col-md-offset-1 centro\">
                        <a href=\"/portfoli/";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary\">Home portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/cards/";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary div-tam\">List Card</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/editportfoli/";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary\">Edit portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/deleteportfoli/";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" onclick=\"return confirm('Are you sure delete portfoli ?')\" class=\"btn btn-primary\">Delete Portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/contactme/";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary\">contact me</a>
                    </div>
                </div>
            ";
        } else {
            // line 30
            echo "                <div class=\"row\">
                    <div class=\"col-md-2 col-md-offset-4 centro\">
                        <a href=\"/portfoli/";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary\">Home portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/contactme/";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary\">contact me</a>
                    </div>
                </div>
            ";
        }
        // line 39
        echo "        </div>
    </div>
    <!-- End Jumbotron -->
";
        
        $__internal_05c40904b17a606645a5bd2aac86713a25d83dbc3f85562ce636ea5763260cee->leave($__internal_05c40904b17a606645a5bd2aac86713a25d83dbc3f85562ce636ea5763260cee_prof);

        
        $__internal_587fe4b244f361c9056e79f56e7bd87055980a86dfb7d6e6eccb524ac07ba46e->leave($__internal_587fe4b244f361c9056e79f56e7bd87055980a86dfb7d6e6eccb524ac07ba46e_prof);

    }

    // line 43
    public function block_body($context, array $blocks = array())
    {
        $__internal_1083e8b8ee0f594feb38b838b889fda727c633a48872b32ca40d55bb254fb584 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1083e8b8ee0f594feb38b838b889fda727c633a48872b32ca40d55bb254fb584->enter($__internal_1083e8b8ee0f594feb38b838b889fda727c633a48872b32ca40d55bb254fb584_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_efcd5b1d0b8a185a5cb7bd5dda084553bcf961c8168551e8ae607556b92bb1eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efcd5b1d0b8a185a5cb7bd5dda084553bcf961c8168551e8ae607556b92bb1eb->enter($__internal_efcd5b1d0b8a185a5cb7bd5dda084553bcf961c8168551e8ae607556b92bb1eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_efcd5b1d0b8a185a5cb7bd5dda084553bcf961c8168551e8ae607556b92bb1eb->leave($__internal_efcd5b1d0b8a185a5cb7bd5dda084553bcf961c8168551e8ae607556b92bb1eb_prof);

        
        $__internal_1083e8b8ee0f594feb38b838b889fda727c633a48872b32ca40d55bb254fb584->leave($__internal_1083e8b8ee0f594feb38b838b889fda727c633a48872b32ca40d55bb254fb584_prof);

    }

    public function getTemplateName()
    {
        return "default/header/header_portfoli.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 43,  117 => 39,  110 => 35,  104 => 32,  100 => 30,  93 => 26,  87 => 23,  81 => 20,  75 => 17,  69 => 14,  65 => 12,  63 => 11,  57 => 8,  50 => 3,  41 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block jumbotron %}
    <!-- Jumbotron -->
    <div class=\"jumbotron color-azul\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-8 col-md-offset-2 centro\">
                    <h1 class=\"cg\">{{(portfoli.title)}}</h1>
                </div>
            </div>
            {% if rute==\"user\" %}
                <div class=\"row\">
                    <div class=\"col-md-2 col-md-offset-1 centro\">
                        <a href=\"/portfoli/{{(portfoli.id)}}\" class=\"btn btn-primary\">Home portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/cards/{{(portfoli.id)}}\" class=\"btn btn-primary div-tam\">List Card</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/editportfoli/{{(portfoli.id)}}\" class=\"btn btn-primary\">Edit portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/deleteportfoli/{{(portfoli.id)}}\" onclick=\"return confirm('Are you sure delete portfoli ?')\" class=\"btn btn-primary\">Delete Portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/contactme/{{(portfoli.id)}}\" class=\"btn btn-primary\">contact me</a>
                    </div>
                </div>
            {% else %}
                <div class=\"row\">
                    <div class=\"col-md-2 col-md-offset-4 centro\">
                        <a href=\"/portfoli/{{(portfoli.id)}}\" class=\"btn btn-primary\">Home portfoli</a>
                    </div>
                    <div class=\"col-md-2 centro\">
                        <a href=\"/contactme/{{(portfoli.id)}}\" class=\"btn btn-primary\">contact me</a>
                    </div>
                </div>
            {% endif %}
        </div>
    </div>
    <!-- End Jumbotron -->
{% endblock %}
{% block body %}{% endblock %}
", "default/header/header_portfoli.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/header/header_portfoli.html.twig");
    }
}
