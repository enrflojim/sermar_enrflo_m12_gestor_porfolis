<?php

/* default/portfolis/portfolisList.html.twig */
class __TwigTemplate_6123b9894f4cbef3d4608ce9c8e1121520c46ac8024e269ca31a696943a39acf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_all.html.twig", "default/portfolis/portfolisList.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_all.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8246fb04a194eaecbfe9c9d1e405bb79272b0eac9c178ccbbea3f056e8081f00 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8246fb04a194eaecbfe9c9d1e405bb79272b0eac9c178ccbbea3f056e8081f00->enter($__internal_8246fb04a194eaecbfe9c9d1e405bb79272b0eac9c178ccbbea3f056e8081f00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/portfolis/portfolisList.html.twig"));

        $__internal_9d5d6cf0cc4d25cd1960d57bd0398e9bb37f6011f47b0a740bef04bbd93969c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d5d6cf0cc4d25cd1960d57bd0398e9bb37f6011f47b0a740bef04bbd93969c7->enter($__internal_9d5d6cf0cc4d25cd1960d57bd0398e9bb37f6011f47b0a740bef04bbd93969c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/portfolis/portfolisList.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8246fb04a194eaecbfe9c9d1e405bb79272b0eac9c178ccbbea3f056e8081f00->leave($__internal_8246fb04a194eaecbfe9c9d1e405bb79272b0eac9c178ccbbea3f056e8081f00_prof);

        
        $__internal_9d5d6cf0cc4d25cd1960d57bd0398e9bb37f6011f47b0a740bef04bbd93969c7->leave($__internal_9d5d6cf0cc4d25cd1960d57bd0398e9bb37f6011f47b0a740bef04bbd93969c7_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_84cca319b9d79b9f3aa337089e5c8df7828b3f8a8406aee5baffa201c781151f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_84cca319b9d79b9f3aa337089e5c8df7828b3f8a8406aee5baffa201c781151f->enter($__internal_84cca319b9d79b9f3aa337089e5c8df7828b3f8a8406aee5baffa201c781151f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_066a6464936a46ebec6d9d2347986a176fdff90ee0770346195bbed462b9245a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_066a6464936a46ebec6d9d2347986a176fdff90ee0770346195bbed462b9245a->enter($__internal_066a6464936a46ebec6d9d2347986a176fdff90ee0770346195bbed462b9245a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"row\">
  <div class=\"col-md-10 col-md-offset-1\">
    ";
        // line 6
        $context["counter"] = 0;
        // line 7
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["portfolis"] ?? $this->getContext($context, "portfolis")));
        foreach ($context['_seq'] as $context["_key"] => $context["portfoli"]) {
            // line 8
            echo "      ";
            if (((($context["counter"] ?? $this->getContext($context, "counter")) % 3) == 0)) {
                // line 9
                echo "      <div class=\"row\">
      ";
            }
            // line 11
            echo "          <div class=\"col-md-4\">
            <div class=\"row format\">
              <div class=\"col-md-7\">
                <img src=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("img/portfolis/" . $this->getAttribute($context["portfoli"], "imgPortfoli", array()))), "html", null, true);
            echo "\" width=\"170\" height=\"130\" class=\"esq\">
              </div>
              <div class=\"col-md-5\">
                <h4>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["portfoli"], "title", array()), "html", null, true);
            echo "</h4>
                <h6 class=\"sub-text\">";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["portfoli"], "dateString", array()), "html", null, true);
            echo "</h6>
                <a href=\"/portfoli/";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["portfoli"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary btn-sm\">Read more</a>
              </div>
            </div>
          </div>
      ";
            // line 23
            if ((((($context["counter"] ?? $this->getContext($context, "counter")) % 3) == 2) || (($context["counter"] ?? $this->getContext($context, "counter")) == twig_length_filter($this->env, ($context["portfolis"] ?? $this->getContext($context, "portfolis")))))) {
                // line 24
                echo "      </div>
      ";
            }
            // line 26
            echo "      ";
            $context["counter"] = (($context["counter"] ?? $this->getContext($context, "counter")) + 1);
            // line 27
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['portfoli'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "  </div>
</div>
";
        
        $__internal_066a6464936a46ebec6d9d2347986a176fdff90ee0770346195bbed462b9245a->leave($__internal_066a6464936a46ebec6d9d2347986a176fdff90ee0770346195bbed462b9245a_prof);

        
        $__internal_84cca319b9d79b9f3aa337089e5c8df7828b3f8a8406aee5baffa201c781151f->leave($__internal_84cca319b9d79b9f3aa337089e5c8df7828b3f8a8406aee5baffa201c781151f_prof);

    }

    public function getTemplateName()
    {
        return "default/portfolis/portfolisList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 28,  102 => 27,  99 => 26,  95 => 24,  93 => 23,  86 => 19,  82 => 18,  78 => 17,  72 => 14,  67 => 11,  63 => 9,  60 => 8,  55 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_all.html.twig' %}

{% block body %}
<div class=\"row\">
  <div class=\"col-md-10 col-md-offset-1\">
    {% set counter = 0 %}
    {% for portfoli in portfolis %}
      {% if(counter%3==0) %}
      <div class=\"row\">
      {% endif %}
          <div class=\"col-md-4\">
            <div class=\"row format\">
              <div class=\"col-md-7\">
                <img src=\"{{ asset('img/portfolis/'~portfoli.imgPortfoli) }}\" width=\"170\" height=\"130\" class=\"esq\">
              </div>
              <div class=\"col-md-5\">
                <h4>{{ (portfoli.title) }}</h4>
                <h6 class=\"sub-text\">{{ (portfoli.dateString) }}</h6>
                <a href=\"/portfoli/{{ (portfoli.id) }}\" class=\"btn btn-primary btn-sm\">Read more</a>
              </div>
            </div>
          </div>
      {% if(counter%3==2 or counter==portfolis|length) %}
      </div>
      {% endif %}
      {% set counter = counter + 1 %}
    {% endfor %}
  </div>
</div>
{% endblock %}
", "default/portfolis/portfolisList.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/portfolis/portfolisList.html.twig");
    }
}
