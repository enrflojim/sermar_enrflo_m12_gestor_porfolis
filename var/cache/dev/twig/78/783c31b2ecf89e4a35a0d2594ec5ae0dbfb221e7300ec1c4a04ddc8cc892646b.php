<?php

/* default/header/header_all.html.twig */
class __TwigTemplate_22108aaacc32b12361fbdd4783a6baf5d04bb6b9c78c6b039b9d8beb436c8ff8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/header/header_all.html.twig", 1);
        $this->blocks = array(
            'jumbotron' => array($this, 'block_jumbotron'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_024aff0e6ade7704a670df8d9edc1c9b5b513e573faf5770a92a9fb06590c694 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_024aff0e6ade7704a670df8d9edc1c9b5b513e573faf5770a92a9fb06590c694->enter($__internal_024aff0e6ade7704a670df8d9edc1c9b5b513e573faf5770a92a9fb06590c694_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/header/header_all.html.twig"));

        $__internal_e10cb9bee2da5bc35eac50416188926903ff8a4dcdcaa2769259d84fc476cfe7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e10cb9bee2da5bc35eac50416188926903ff8a4dcdcaa2769259d84fc476cfe7->enter($__internal_e10cb9bee2da5bc35eac50416188926903ff8a4dcdcaa2769259d84fc476cfe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/header/header_all.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_024aff0e6ade7704a670df8d9edc1c9b5b513e573faf5770a92a9fb06590c694->leave($__internal_024aff0e6ade7704a670df8d9edc1c9b5b513e573faf5770a92a9fb06590c694_prof);

        
        $__internal_e10cb9bee2da5bc35eac50416188926903ff8a4dcdcaa2769259d84fc476cfe7->leave($__internal_e10cb9bee2da5bc35eac50416188926903ff8a4dcdcaa2769259d84fc476cfe7_prof);

    }

    // line 2
    public function block_jumbotron($context, array $blocks = array())
    {
        $__internal_e73dae5f26367b06ed6c8df84ce79f0a9e78cf2c08ea8d910e0d995fd38fbcef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e73dae5f26367b06ed6c8df84ce79f0a9e78cf2c08ea8d910e0d995fd38fbcef->enter($__internal_e73dae5f26367b06ed6c8df84ce79f0a9e78cf2c08ea8d910e0d995fd38fbcef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "jumbotron"));

        $__internal_1105a87499eef06a3ed5e465ba904bfe49dfae5d830ba381b3d4536cfb3e6ddf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1105a87499eef06a3ed5e465ba904bfe49dfae5d830ba381b3d4536cfb3e6ddf->enter($__internal_1105a87499eef06a3ed5e465ba904bfe49dfae5d830ba381b3d4536cfb3e6ddf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "jumbotron"));

        // line 3
        echo "<!-- Jumbotron -->
<div class=\"jumbotron color-azul\">
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-md-8 col-md-offset-2\">
        <center><h1>Portfolis</h1></center>
      </div>
    </div>
    <div class=\"row\">
      <div class=\"col-md-2  col-md-offset-5\">
        ";
        // line 13
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 14
            echo "        <a href=\"/createportfoli\" class=\"btn btn-primary\">Create portfoli</a>
        ";
        } else {
            // line 16
            echo "        <a href=\"#\" class=\"btn btn-primary window jumbo1\" disabled>
                <span>You must be logged to create portfolis</span>
                      Create portfoli</a>
        ";
        }
        // line 20
        echo "      </div>
      </div>
    </div>
  </div>
      </div>
<!-- End Jumbotron -->
";
        
        $__internal_1105a87499eef06a3ed5e465ba904bfe49dfae5d830ba381b3d4536cfb3e6ddf->leave($__internal_1105a87499eef06a3ed5e465ba904bfe49dfae5d830ba381b3d4536cfb3e6ddf_prof);

        
        $__internal_e73dae5f26367b06ed6c8df84ce79f0a9e78cf2c08ea8d910e0d995fd38fbcef->leave($__internal_e73dae5f26367b06ed6c8df84ce79f0a9e78cf2c08ea8d910e0d995fd38fbcef_prof);

    }

    // line 27
    public function block_body($context, array $blocks = array())
    {
        $__internal_236d87b3c9603edeee53e905615e70f504c3fdf476f6c5e1ffb3d6fbab9c20a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_236d87b3c9603edeee53e905615e70f504c3fdf476f6c5e1ffb3d6fbab9c20a0->enter($__internal_236d87b3c9603edeee53e905615e70f504c3fdf476f6c5e1ffb3d6fbab9c20a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_973199985b66be5ee0c295a6477421a8bd754d908d31f34ece6798b48b453a3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_973199985b66be5ee0c295a6477421a8bd754d908d31f34ece6798b48b453a3c->enter($__internal_973199985b66be5ee0c295a6477421a8bd754d908d31f34ece6798b48b453a3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_973199985b66be5ee0c295a6477421a8bd754d908d31f34ece6798b48b453a3c->leave($__internal_973199985b66be5ee0c295a6477421a8bd754d908d31f34ece6798b48b453a3c_prof);

        
        $__internal_236d87b3c9603edeee53e905615e70f504c3fdf476f6c5e1ffb3d6fbab9c20a0->leave($__internal_236d87b3c9603edeee53e905615e70f504c3fdf476f6c5e1ffb3d6fbab9c20a0_prof);

    }

    public function getTemplateName()
    {
        return "default/header/header_all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 27,  74 => 20,  68 => 16,  64 => 14,  62 => 13,  50 => 3,  41 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block jumbotron %}
<!-- Jumbotron -->
<div class=\"jumbotron color-azul\">
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-md-8 col-md-offset-2\">
        <center><h1>Portfolis</h1></center>
      </div>
    </div>
    <div class=\"row\">
      <div class=\"col-md-2  col-md-offset-5\">
        {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
        <a href=\"/createportfoli\" class=\"btn btn-primary\">Create portfoli</a>
        {% else %}
        <a href=\"#\" class=\"btn btn-primary window jumbo1\" disabled>
                <span>You must be logged to create portfolis</span>
                      Create portfoli</a>
        {% endif %}
      </div>
      </div>
    </div>
  </div>
      </div>
<!-- End Jumbotron -->
{% endblock %}
{% block body %}{% endblock %}
", "default/header/header_all.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/header/header_all.html.twig");
    }
}
