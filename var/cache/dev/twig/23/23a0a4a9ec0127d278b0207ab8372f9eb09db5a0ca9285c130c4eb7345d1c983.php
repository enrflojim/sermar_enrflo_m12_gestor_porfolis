<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_d1451a6b41781b5c9ee3090c82c7e7d5ce5c92e8f5614c0c3476f41a4f976281 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_55e7b76e18bcb3e299802695fa30236a4831b8325d9e4790b3a4ff2db99fa63c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_55e7b76e18bcb3e299802695fa30236a4831b8325d9e4790b3a4ff2db99fa63c->enter($__internal_55e7b76e18bcb3e299802695fa30236a4831b8325d9e4790b3a4ff2db99fa63c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_1ef0ab692c8dcc15cd29e95973b326fed8e77c272269f1fa0f055dbc378aead8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ef0ab692c8dcc15cd29e95973b326fed8e77c272269f1fa0f055dbc378aead8->enter($__internal_1ef0ab692c8dcc15cd29e95973b326fed8e77c272269f1fa0f055dbc378aead8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_55e7b76e18bcb3e299802695fa30236a4831b8325d9e4790b3a4ff2db99fa63c->leave($__internal_55e7b76e18bcb3e299802695fa30236a4831b8325d9e4790b3a4ff2db99fa63c_prof);

        
        $__internal_1ef0ab692c8dcc15cd29e95973b326fed8e77c272269f1fa0f055dbc378aead8->leave($__internal_1ef0ab692c8dcc15cd29e95973b326fed8e77c272269f1fa0f055dbc378aead8_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_61a2e00b5f9b243d42b41335fbae46779df949f3238195d93f37cefa7072a4ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_61a2e00b5f9b243d42b41335fbae46779df949f3238195d93f37cefa7072a4ee->enter($__internal_61a2e00b5f9b243d42b41335fbae46779df949f3238195d93f37cefa7072a4ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_a9befc2b52d541ee26b2102f4455713a65c48b5e59fc7db033cb31fa51ea543a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9befc2b52d541ee26b2102f4455713a65c48b5e59fc7db033cb31fa51ea543a->enter($__internal_a9befc2b52d541ee26b2102f4455713a65c48b5e59fc7db033cb31fa51ea543a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_a9befc2b52d541ee26b2102f4455713a65c48b5e59fc7db033cb31fa51ea543a->leave($__internal_a9befc2b52d541ee26b2102f4455713a65c48b5e59fc7db033cb31fa51ea543a_prof);

        
        $__internal_61a2e00b5f9b243d42b41335fbae46779df949f3238195d93f37cefa7072a4ee->leave($__internal_61a2e00b5f9b243d42b41335fbae46779df949f3238195d93f37cefa7072a4ee_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
