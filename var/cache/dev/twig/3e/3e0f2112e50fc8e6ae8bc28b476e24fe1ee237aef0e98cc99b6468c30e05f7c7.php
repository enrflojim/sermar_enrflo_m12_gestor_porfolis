<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_edcb3c4f4b4aa23a7bf7338479013cf07f85d9eb10ae5e2337649869eb8994f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ad5a90b7d07824277b68acb0d93acaddf7ab90a6020466aa8a9c5fec7f2c0e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ad5a90b7d07824277b68acb0d93acaddf7ab90a6020466aa8a9c5fec7f2c0e2->enter($__internal_2ad5a90b7d07824277b68acb0d93acaddf7ab90a6020466aa8a9c5fec7f2c0e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_3a411489bce3f6abc808c5deb3099e9aaa3792bc7d2b00112ac179b3b42e6f40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a411489bce3f6abc808c5deb3099e9aaa3792bc7d2b00112ac179b3b42e6f40->enter($__internal_3a411489bce3f6abc808c5deb3099e9aaa3792bc7d2b00112ac179b3b42e6f40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2ad5a90b7d07824277b68acb0d93acaddf7ab90a6020466aa8a9c5fec7f2c0e2->leave($__internal_2ad5a90b7d07824277b68acb0d93acaddf7ab90a6020466aa8a9c5fec7f2c0e2_prof);

        
        $__internal_3a411489bce3f6abc808c5deb3099e9aaa3792bc7d2b00112ac179b3b42e6f40->leave($__internal_3a411489bce3f6abc808c5deb3099e9aaa3792bc7d2b00112ac179b3b42e6f40_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_8518b7903b7f6817c8f492bc7d97d1b4f2cacf899755a8124df74922ebaa8bdf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8518b7903b7f6817c8f492bc7d97d1b4f2cacf899755a8124df74922ebaa8bdf->enter($__internal_8518b7903b7f6817c8f492bc7d97d1b4f2cacf899755a8124df74922ebaa8bdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_6cb726c9de8e1e015d613fe93e97f72f5b474c1819d3b7bc20190157a63314d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6cb726c9de8e1e015d613fe93e97f72f5b474c1819d3b7bc20190157a63314d7->enter($__internal_6cb726c9de8e1e015d613fe93e97f72f5b474c1819d3b7bc20190157a63314d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_6cb726c9de8e1e015d613fe93e97f72f5b474c1819d3b7bc20190157a63314d7->leave($__internal_6cb726c9de8e1e015d613fe93e97f72f5b474c1819d3b7bc20190157a63314d7_prof);

        
        $__internal_8518b7903b7f6817c8f492bc7d97d1b4f2cacf899755a8124df74922ebaa8bdf->leave($__internal_8518b7903b7f6817c8f492bc7d97d1b4f2cacf899755a8124df74922ebaa8bdf_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_f2a71565f791f25298fe77a81cd65974a91b987a536d271150d27bd7546e554e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f2a71565f791f25298fe77a81cd65974a91b987a536d271150d27bd7546e554e->enter($__internal_f2a71565f791f25298fe77a81cd65974a91b987a536d271150d27bd7546e554e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_685292ae631b7533d5545e1b653afbc4c5170db5a4f5808cb2fdcc76967117aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_685292ae631b7533d5545e1b653afbc4c5170db5a4f5808cb2fdcc76967117aa->enter($__internal_685292ae631b7533d5545e1b653afbc4c5170db5a4f5808cb2fdcc76967117aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_685292ae631b7533d5545e1b653afbc4c5170db5a4f5808cb2fdcc76967117aa->leave($__internal_685292ae631b7533d5545e1b653afbc4c5170db5a4f5808cb2fdcc76967117aa_prof);

        
        $__internal_f2a71565f791f25298fe77a81cd65974a91b987a536d271150d27bd7546e554e->leave($__internal_f2a71565f791f25298fe77a81cd65974a91b987a536d271150d27bd7546e554e_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_fa482f5beda6ddcb8357e455bb14a97659f5f0179a6ff84b01d3a83003ed7edb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa482f5beda6ddcb8357e455bb14a97659f5f0179a6ff84b01d3a83003ed7edb->enter($__internal_fa482f5beda6ddcb8357e455bb14a97659f5f0179a6ff84b01d3a83003ed7edb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_bb2e7beff76d3489098c41bd1bec1c775cac01c2f6e9af66f20bee0204c0f9b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb2e7beff76d3489098c41bd1bec1c775cac01c2f6e9af66f20bee0204c0f9b5->enter($__internal_bb2e7beff76d3489098c41bd1bec1c775cac01c2f6e9af66f20bee0204c0f9b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_bb2e7beff76d3489098c41bd1bec1c775cac01c2f6e9af66f20bee0204c0f9b5->leave($__internal_bb2e7beff76d3489098c41bd1bec1c775cac01c2f6e9af66f20bee0204c0f9b5_prof);

        
        $__internal_fa482f5beda6ddcb8357e455bb14a97659f5f0179a6ff84b01d3a83003ed7edb->leave($__internal_fa482f5beda6ddcb8357e455bb14a97659f5f0179a6ff84b01d3a83003ed7edb_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
