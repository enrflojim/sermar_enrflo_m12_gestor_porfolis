<?php

/* default/varios/contactme.html.twig */
class __TwigTemplate_b94c72c7bace70d90da92ba561f1d2524c8efa4c8c985638cd9f81174c592cb6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/header/header_portfoli.html.twig", "default/varios/contactme.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/header/header_portfoli.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0ccc5789847433cd8b750c71b5e18aaee53cb4802d5dcb0c2c86191b33707da = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0ccc5789847433cd8b750c71b5e18aaee53cb4802d5dcb0c2c86191b33707da->enter($__internal_f0ccc5789847433cd8b750c71b5e18aaee53cb4802d5dcb0c2c86191b33707da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/varios/contactme.html.twig"));

        $__internal_bf68cb0c8be23590667936fe5724773039e107527531ab7090ea851d1af847fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf68cb0c8be23590667936fe5724773039e107527531ab7090ea851d1af847fd->enter($__internal_bf68cb0c8be23590667936fe5724773039e107527531ab7090ea851d1af847fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/varios/contactme.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f0ccc5789847433cd8b750c71b5e18aaee53cb4802d5dcb0c2c86191b33707da->leave($__internal_f0ccc5789847433cd8b750c71b5e18aaee53cb4802d5dcb0c2c86191b33707da_prof);

        
        $__internal_bf68cb0c8be23590667936fe5724773039e107527531ab7090ea851d1af847fd->leave($__internal_bf68cb0c8be23590667936fe5724773039e107527531ab7090ea851d1af847fd_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_3b531b3ec52d1f77aef9686819dc1640fbfdd62af3e756e25073657158e6bfde = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b531b3ec52d1f77aef9686819dc1640fbfdd62af3e756e25073657158e6bfde->enter($__internal_3b531b3ec52d1f77aef9686819dc1640fbfdd62af3e756e25073657158e6bfde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b37a6ec23862409bff28d8010da63cea29b15d25912528faba14e374f9372259 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b37a6ec23862409bff28d8010da63cea29b15d25912528faba14e374f9372259->enter($__internal_b37a6ec23862409bff28d8010da63cea29b15d25912528faba14e374f9372259_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-10 col-md-offset-1 format-forms color-azul\">
              <div class=\"row\">
                <div class=\"col-md-11\">
                  <h1>";
        // line 8
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</h1>
                  <p>You can contact me using your email or this webpage.</p>
                  <fieldset>
                      <legend>My data:</legend>
                      Full name: ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "name", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "surname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "lastname", array()), "html", null, true);
        echo "<br/>
                      Date of birth: ";
        // line 13
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "date", array()), "Y-m-d"), "html", null, true);
        echo "<br/>
                      Phone number: ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "phone", array()), "html", null, true);
        echo "<br/>
                      Email: ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "email", array()), "html", null, true);
        echo "<br/>
                      City: ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "city", array()), "html", null, true);
        echo "<br/>
                      Country ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["portfoli"] ?? $this->getContext($context, "portfoli")), "idUser", array()), "country", array()), "html", null, true);
        echo "<br/>
                  </fieldset>
                  <hr>
                  ";
        // line 20
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
                  ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
                </div>
              </div>
            </div>
        </div>
    </div>
";
        
        $__internal_b37a6ec23862409bff28d8010da63cea29b15d25912528faba14e374f9372259->leave($__internal_b37a6ec23862409bff28d8010da63cea29b15d25912528faba14e374f9372259_prof);

        
        $__internal_3b531b3ec52d1f77aef9686819dc1640fbfdd62af3e756e25073657158e6bfde->leave($__internal_3b531b3ec52d1f77aef9686819dc1640fbfdd62af3e756e25073657158e6bfde_prof);

    }

    public function getTemplateName()
    {
        return "default/varios/contactme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 21,  93 => 20,  87 => 17,  83 => 16,  79 => 15,  75 => 14,  71 => 13,  63 => 12,  56 => 8,  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/header/header_portfoli.html.twig' %}
{% block body %}
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-10 col-md-offset-1 format-forms color-azul\">
              <div class=\"row\">
                <div class=\"col-md-11\">
                  <h1>{{title}}</h1>
                  <p>You can contact me using your email or this webpage.</p>
                  <fieldset>
                      <legend>My data:</legend>
                      Full name: {{portfoli.idUser.name}} {{portfoli.idUser.surname}} {{portfoli.idUser.lastname}}<br/>
                      Date of birth: {{portfoli.idUser.date|date('Y-m-d')}}<br/>
                      Phone number: {{portfoli.idUser.phone}}<br/>
                      Email: {{portfoli.idUser.email}}<br/>
                      City: {{portfoli.idUser.city}}<br/>
                      Country {{portfoli.idUser.country}}<br/>
                  </fieldset>
                  <hr>
                  {{ form_start(form) }}
                  {{ form_end(form) }}
                </div>
              </div>
            </div>
        </div>
    </div>
{% endblock %}
", "default/varios/contactme.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/app/Resources/views/default/varios/contactme.html.twig");
    }
}
