<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_5b712adbfe12e7c866a4c67439b6a3980ee5996e786786a1cda6ad79515c8c98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4b20376abd5d4905d24730c9543c19d84517d1b44268310cbd3b58ac05d0f03 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4b20376abd5d4905d24730c9543c19d84517d1b44268310cbd3b58ac05d0f03->enter($__internal_b4b20376abd5d4905d24730c9543c19d84517d1b44268310cbd3b58ac05d0f03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_e7e97b948468b4286ddfd1886ed69be86a3eb2aec76e7b39812e611273e2ac17 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7e97b948468b4286ddfd1886ed69be86a3eb2aec76e7b39812e611273e2ac17->enter($__internal_e7e97b948468b4286ddfd1886ed69be86a3eb2aec76e7b39812e611273e2ac17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b4b20376abd5d4905d24730c9543c19d84517d1b44268310cbd3b58ac05d0f03->leave($__internal_b4b20376abd5d4905d24730c9543c19d84517d1b44268310cbd3b58ac05d0f03_prof);

        
        $__internal_e7e97b948468b4286ddfd1886ed69be86a3eb2aec76e7b39812e611273e2ac17->leave($__internal_e7e97b948468b4286ddfd1886ed69be86a3eb2aec76e7b39812e611273e2ac17_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_7bd3ab8a8af1fcefa956cabada8890063793f5b86d0d1ebfbd6e1c005e2a1700 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7bd3ab8a8af1fcefa956cabada8890063793f5b86d0d1ebfbd6e1c005e2a1700->enter($__internal_7bd3ab8a8af1fcefa956cabada8890063793f5b86d0d1ebfbd6e1c005e2a1700_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_208572149386f651601bb36d2df9d98e53c746feda1c057a62c61c2a38220639 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_208572149386f651601bb36d2df9d98e53c746feda1c057a62c61c2a38220639->enter($__internal_208572149386f651601bb36d2df9d98e53c746feda1c057a62c61c2a38220639_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_208572149386f651601bb36d2df9d98e53c746feda1c057a62c61c2a38220639->leave($__internal_208572149386f651601bb36d2df9d98e53c746feda1c057a62c61c2a38220639_prof);

        
        $__internal_7bd3ab8a8af1fcefa956cabada8890063793f5b86d0d1ebfbd6e1c005e2a1700->leave($__internal_7bd3ab8a8af1fcefa956cabada8890063793f5b86d0d1ebfbd6e1c005e2a1700_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f776dadb82b336468beede9ed5607d787239cbcc40e639187af9e94cda369897 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f776dadb82b336468beede9ed5607d787239cbcc40e639187af9e94cda369897->enter($__internal_f776dadb82b336468beede9ed5607d787239cbcc40e639187af9e94cda369897_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_b37628ceebc8e441fc43f0789d3b8c88d237b912cad624094c5efab43366d020 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b37628ceebc8e441fc43f0789d3b8c88d237b912cad624094c5efab43366d020->enter($__internal_b37628ceebc8e441fc43f0789d3b8c88d237b912cad624094c5efab43366d020_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_b37628ceebc8e441fc43f0789d3b8c88d237b912cad624094c5efab43366d020->leave($__internal_b37628ceebc8e441fc43f0789d3b8c88d237b912cad624094c5efab43366d020_prof);

        
        $__internal_f776dadb82b336468beede9ed5607d787239cbcc40e639187af9e94cda369897->leave($__internal_f776dadb82b336468beede9ed5607d787239cbcc40e639187af9e94cda369897_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_b79a53d50e5cef008f557950797a56f59b7a37a716c06d5cff7c4b0b020b3ed3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b79a53d50e5cef008f557950797a56f59b7a37a716c06d5cff7c4b0b020b3ed3->enter($__internal_b79a53d50e5cef008f557950797a56f59b7a37a716c06d5cff7c4b0b020b3ed3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_2d8fb8f5f60a1a72f5a7ac2ddf45afe632844e677fc027d3b3215b3f65ba513b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d8fb8f5f60a1a72f5a7ac2ddf45afe632844e677fc027d3b3215b3f65ba513b->enter($__internal_2d8fb8f5f60a1a72f5a7ac2ddf45afe632844e677fc027d3b3215b3f65ba513b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_2d8fb8f5f60a1a72f5a7ac2ddf45afe632844e677fc027d3b3215b3f65ba513b->leave($__internal_2d8fb8f5f60a1a72f5a7ac2ddf45afe632844e677fc027d3b3215b3f65ba513b_prof);

        
        $__internal_b79a53d50e5cef008f557950797a56f59b7a37a716c06d5cff7c4b0b020b3ed3->leave($__internal_b79a53d50e5cef008f557950797a56f59b7a37a716c06d5cff7c4b0b020b3ed3_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
