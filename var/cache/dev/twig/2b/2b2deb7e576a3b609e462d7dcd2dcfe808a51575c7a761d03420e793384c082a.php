<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_55d93e2896641743c6c407412a740b5f126ff4e55691abf303423d7b4ab4c278 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_893359a2c3db4a429e9c89ed7ee6418490094b623aa42a95621cbd412d66f2e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_893359a2c3db4a429e9c89ed7ee6418490094b623aa42a95621cbd412d66f2e7->enter($__internal_893359a2c3db4a429e9c89ed7ee6418490094b623aa42a95621cbd412d66f2e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_7d5704cb8888cf5f93ee043021c33677bf2d7a604976089d301a6daf4cb8fc02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d5704cb8888cf5f93ee043021c33677bf2d7a604976089d301a6daf4cb8fc02->enter($__internal_7d5704cb8888cf5f93ee043021c33677bf2d7a604976089d301a6daf4cb8fc02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_893359a2c3db4a429e9c89ed7ee6418490094b623aa42a95621cbd412d66f2e7->leave($__internal_893359a2c3db4a429e9c89ed7ee6418490094b623aa42a95621cbd412d66f2e7_prof);

        
        $__internal_7d5704cb8888cf5f93ee043021c33677bf2d7a604976089d301a6daf4cb8fc02->leave($__internal_7d5704cb8888cf5f93ee043021c33677bf2d7a604976089d301a6daf4cb8fc02_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_13ddd41d97cb0835fb269c46625e7796d6fb923b30d25c887ca5c7981c5fb821 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13ddd41d97cb0835fb269c46625e7796d6fb923b30d25c887ca5c7981c5fb821->enter($__internal_13ddd41d97cb0835fb269c46625e7796d6fb923b30d25c887ca5c7981c5fb821_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_74147c8b4be2178e78f07ce385650c2d694c7eafa821965e874ffa7ef0abb578 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_74147c8b4be2178e78f07ce385650c2d694c7eafa821965e874ffa7ef0abb578->enter($__internal_74147c8b4be2178e78f07ce385650c2d694c7eafa821965e874ffa7ef0abb578_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_74147c8b4be2178e78f07ce385650c2d694c7eafa821965e874ffa7ef0abb578->leave($__internal_74147c8b4be2178e78f07ce385650c2d694c7eafa821965e874ffa7ef0abb578_prof);

        
        $__internal_13ddd41d97cb0835fb269c46625e7796d6fb923b30d25c887ca5c7981c5fb821->leave($__internal_13ddd41d97cb0835fb269c46625e7796d6fb923b30d25c887ca5c7981c5fb821_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d3dc9aace55a1b660f6a21641cd95dfb6aeb5ef3e0903977116f9cfca74231fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d3dc9aace55a1b660f6a21641cd95dfb6aeb5ef3e0903977116f9cfca74231fb->enter($__internal_d3dc9aace55a1b660f6a21641cd95dfb6aeb5ef3e0903977116f9cfca74231fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_3f4e159558cdc47f40e5d81e8834557f8d397be7c457a6bd6e1e0f963375e8dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f4e159558cdc47f40e5d81e8834557f8d397be7c457a6bd6e1e0f963375e8dd->enter($__internal_3f4e159558cdc47f40e5d81e8834557f8d397be7c457a6bd6e1e0f963375e8dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_3f4e159558cdc47f40e5d81e8834557f8d397be7c457a6bd6e1e0f963375e8dd->leave($__internal_3f4e159558cdc47f40e5d81e8834557f8d397be7c457a6bd6e1e0f963375e8dd_prof);

        
        $__internal_d3dc9aace55a1b660f6a21641cd95dfb6aeb5ef3e0903977116f9cfca74231fb->leave($__internal_d3dc9aace55a1b660f6a21641cd95dfb6aeb5ef3e0903977116f9cfca74231fb_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_d064ae447a17f5b40be4d0cf14a080e300e92665666c20657e78cb55e7247e7b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d064ae447a17f5b40be4d0cf14a080e300e92665666c20657e78cb55e7247e7b->enter($__internal_d064ae447a17f5b40be4d0cf14a080e300e92665666c20657e78cb55e7247e7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_f22359d74351ba6a7823ec857573b19521e36fa5aaad1eeb8b4d983b94b4d6e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f22359d74351ba6a7823ec857573b19521e36fa5aaad1eeb8b4d983b94b4d6e0->enter($__internal_f22359d74351ba6a7823ec857573b19521e36fa5aaad1eeb8b4d983b94b4d6e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_f22359d74351ba6a7823ec857573b19521e36fa5aaad1eeb8b4d983b94b4d6e0->leave($__internal_f22359d74351ba6a7823ec857573b19521e36fa5aaad1eeb8b4d983b94b4d6e0_prof);

        
        $__internal_d064ae447a17f5b40be4d0cf14a080e300e92665666c20657e78cb55e7247e7b->leave($__internal_d064ae447a17f5b40be4d0cf14a080e300e92665666c20657e78cb55e7247e7b_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/a14sermarbal/public_html/sermar_enrflo_m12_gestor_porfolis/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
