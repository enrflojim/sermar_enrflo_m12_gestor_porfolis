<?php

namespace Proxies\__CG__\AppBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Portfoli extends \AppBundle\Entity\Portfoli implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', 'id', 'title', 'isPremium', 'isInformatic', 'imgPortfoli', 'idCategory', 'cards', 'idUser', 'datePortfoli', 'dateString'];
        }

        return ['__isInitialized__', 'id', 'title', 'isPremium', 'isInformatic', 'imgPortfoli', 'idCategory', 'cards', 'idUser', 'datePortfoli', 'dateString'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Portfoli $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setTitle($title)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTitle', [$title]);

        return parent::setTitle($title);
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTitle', []);

        return parent::getTitle();
    }

    /**
     * {@inheritDoc}
     */
    public function setIsPremium($isPremium)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIsPremium', [$isPremium]);

        return parent::setIsPremium($isPremium);
    }

    /**
     * {@inheritDoc}
     */
    public function getIsPremium()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIsPremium', []);

        return parent::getIsPremium();
    }

    /**
     * {@inheritDoc}
     */
    public function setImgPortfoli($imgPortfoli)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setImgPortfoli', [$imgPortfoli]);

        return parent::setImgPortfoli($imgPortfoli);
    }

    /**
     * {@inheritDoc}
     */
    public function getImgPortfoli()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getImgPortfoli', []);

        return parent::getImgPortfoli();
    }

    /**
     * {@inheritDoc}
     */
    public function setIdCategory(\AppBundle\Entity\Category $idCategory = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIdCategory', [$idCategory]);

        return parent::setIdCategory($idCategory);
    }

    /**
     * {@inheritDoc}
     */
    public function getIdCategory()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIdCategory', []);

        return parent::getIdCategory();
    }

    /**
     * {@inheritDoc}
     */
    public function addCard(\AppBundle\Entity\Card $card)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addCard', [$card]);

        return parent::addCard($card);
    }

    /**
     * {@inheritDoc}
     */
    public function removeCard(\AppBundle\Entity\Card $card)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeCard', [$card]);

        return parent::removeCard($card);
    }

    /**
     * {@inheritDoc}
     */
    public function getCards()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCards', []);

        return parent::getCards();
    }

    /**
     * {@inheritDoc}
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIdUser', [$idUser]);

        return parent::setIdUser($idUser);
    }

    /**
     * {@inheritDoc}
     */
    public function getIdUser()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIdUser', []);

        return parent::getIdUser();
    }

    /**
     * {@inheritDoc}
     */
    public function setDatePortfoli($datePortfoli)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDatePortfoli', [$datePortfoli]);

        return parent::setDatePortfoli($datePortfoli);
    }

    /**
     * {@inheritDoc}
     */
    public function getDatePortfoli()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDatePortfoli', []);

        return parent::getDatePortfoli();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateString($dateString)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateString', [$dateString]);

        return parent::setDateString($dateString);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateString()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateString', []);

        return parent::getDateString();
    }

    /**
     * {@inheritDoc}
     */
    public function setIsInformatic($isInformatic)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIsInformatic', [$isInformatic]);

        return parent::setIsInformatic($isInformatic);
    }

    /**
     * {@inheritDoc}
     */
    public function getIsInformatic()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIsInformatic', []);

        return parent::getIsInformatic();
    }

}
