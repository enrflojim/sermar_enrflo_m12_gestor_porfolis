$(document).ready(function(){

  $(".fileimg").on("change", function(){
	  /*Limpio vista previa*/
      $(".mostrar").html("");

      /*recogemos el archivo*/
      var file = document.getElementById($('.fileimg').attr('id')).files;

      //Nos servira para recoger la ruta del archivo
      var nav= window.URL || window.webkitURL;

      //recogeremos tamaño, tipo y nombre
      var size=file[0].size;
      var type=file[0].type;
      var name=file[0].name;


      //Comprobamos un tipo
      if(type!="image/jpeg" && type!="image/jpg" && type!="image/png" && type!="image/gif"){
           $(".mostrar").append('<h5>File incorrect only jpeg/jpg/png/gif</h5>');
      }
      else{
          //Comprobamos tamaño
          if(size>1024*1024){
            $(".mostrar").append('<h5 class="window"><span>Large size the image must be smaller than 500 x 500</span></h5>');
          }
          else{
            //Guardamos la ruta
            var ruta = nav.createObjectURL(file[0]);

            //mostramos la imagen
            $(".mostrar").append('<img src="'+ruta+'" width="148" alt="'+ruta+'" title="'+ruta+'">');
          }
      }
    });

    $(function() {
       $.getJSON("./../php/dato.php", function(result){
         $( "#tag" ).autocomplete({
              source: result
         });
       });
    });
});
