<?php

include 'database.php';

class Usuarios extends DataBase{

    public function __construct() {
        parent::__construct();
	}

    public function buscar(){
        $datos = array();
        $sth = $this->prepare('SELECT name_category FROM category UNION SELECT name_subcategory FROM subcategory GROUP BY name_subcategory');
        $sth->execute();
        $result = $sth->fetchAll();

        foreach ($result as $key => $value) {
            array_push($datos, $value["name_category"]);
        }

        return $datos;

    }

}
