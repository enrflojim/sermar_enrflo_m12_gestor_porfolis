# Projecto fin de curso Gestor de porfolis
#####Sergi Martinez y Enrique Flo

##Nombre del proyecto

Portfolis

##Descripción
	Este projecto se servirá para crear y agregar portfolios (Son una especie de curriculums)  
	donde podrás incluir todos tus projectos de cualquier ámbito para que así las empresas puedan  
	verlos y sí les interesa tu trabajo se puedan poner en contacto contigo

##Integrantes del grupo

- Sergi Martínez
- Enrique Flo

##Descarga del projecto

En el bitbucket dentro del projecto copiamos el enlace de la pestaña clonar y desde la terminal lo descargamos con el siguiente comando:

        git clone https://sermarbal@bitbucket.org/enrflojim/sermar_enrflo_m12_gestor_porfolis.git

##Servidor SMTP i correu on es reben els correus de contacte:

        Email: portfoliscontact@gmail.com
        Password: HelloWorld

##Base de datos

Local

	BBDD: SerMar_EnrFlo_Portfolis
	usuario: root
	password: hardcore

Labs

    BBDD:  a14sermarbal_Portfolis
    user: a14sermarbal_se
    password: ausias

##Estado del projecto
	
Faltarian implementar los anuncios en los portfolios free pero por todo lo demás está finalizado aún sabiendo que hay muchas posibles mejoras.

## Documentación

Carpeta Docs

##Comandaments d'ajuda per crear la db utilitzant symfony

	php bin/console doctrine:database:drop --force
	php bin/console doctrine:database:create
	php bin/console  doctrine:generate:entities AppBundle
	sudo php bin/console doctrine:schema:update --force

## Servidor Apache2 HTTPS

### Pasos previs:

Utilitzarem ubuntu 16.04 amb LAMP instal·lat.

### 1- Creació del certificat SSL

Mitjançant la següent comanda crearem el certificat SSL

       sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt

Al introduir el comandament surtirà el següent formulari: L'omplirem aproximadament com està descrit:

       Country Name (2 letter code) [AU]:EU
       State or Province Name (full name) [Some-State]:Spain
       Locality Name (eg, city) []:Barcelona
       Organization Name (eg, company) [Internet Widgits Pty Ltd]:SerFlo, Inc.
       Organizational Unit Name (eg, section) []:Portfolis
       Common Name (e.g. server FQDN or YOUR name) []:Tu ip o dominio
       Email Address []:portfoliscontact@gmail.com

Seguidament ens farà falta fer totes les variables "aleatories" per al correcte funcionament del certificat SSL. Per això introduïm aquesta comanda i esperem que acabi de fer-los.

       sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048

### 2- Configuració d'Apache2

Ara ens toca configurar el servidor Apache2 per que utilitzi el certificat i escolti pel protocol https que per defecte sempre serà el port 443.

Primer farem una forta configuració de xifrat configurant el següent arxiu que es crearà:

       sudo nano /etc/apache2/conf-available/ssl-params.conf

       # from https://cipherli.st/
       # and https://raymii.org/s/tutorials/Strong_SSL_Security_On_Apache2.html

       SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH
       SSLProtocol All -SSLv2 -SSLv3
       SSLHonorCipherOrder On
       # Disable preloading HSTS for now.  You can use the commented out header line that includes
       # the "preload" directive if you understand the implications.
       #Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains; preload"
       Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains"
       Header always set X-Frame-Options DENY
       Header always set X-Content-Type-Options nosniff
       # Requires Apache >= 2.4
       SSLCompression off 
       SSLSessionTickets Off
       SSLUseStapling on 
       SSLStaplingCache "shmcb:logs/stapling-cache(150000)"

       SSLOpenSSLConfCmd DHParameters "/etc/ssl/certs/dhparam.pem"

Ara configurarem l'arxiu del virtual host per a que escolti pel port 443

Primer farem una copia de l'arxiu existent per a tenir un respald

       sudo cp /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf.bak

Seguidament el modificarem:

       sudo nano /etc/apache2/sites-available/default-ssl.conf

I farem els canvis remarcats:

       <IfModule mod_ssl.c>
        <VirtualHost _default_:443>
                
                // linea a modificar
                ServerAdmin tu mail

                // linea a agregar
                ServerName ip o dominio del servidor

                DocumentRoot /var/www/html

                ErrorLog ${APACHE_LOG_DIR}/error.log
                CustomLog ${APACHE_LOG_DIR}/access.log combined

                SSLEngine on

                // lineas a agregar
                SSLCertificateFile      /etc/ssl/certs/apache-selfsigned.crt
                SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key

                <FilesMatch "\.(cgi|shtml|phtml|php)$">
                                SSLOptions +StdEnvVars
                </FilesMatch>
                <Directory /usr/lib/cgi-bin>
                                SSLOptions +StdEnvVars
                </Directory>

                //Lineas a agregar
                BrowserMatch "MSIE [2-6]" \
                             nokeepalive ssl-unclean-shutdown \
                             downgrade-1.0 force-response-1.0

        </VirtualHost>
</IfModule>

Amb això ja tenim feta la connexió i els arxius respectius a aquesta. Tot i així ja que no farem servir la connexió http ( http not secure) la deshabilitarem fent un redireccionament. Primero copiaremos:

       sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf.bak

Iremos al archivo:

       sudo nano /etc/apache2/sites-available/000-default.conf

Y agregamos la línea

       <VirtualHost *:80>
           . . .
           // linea a agregar
           Redirect permanent "/" "https://ip o dominio/"
           . . .
       </VirtualHost>

### 3- Configuració del firewall

En cas que tinguem el firewall ufw habilitat haurem de configurar-lo com es diu a continuació:

       sudo ufw allow 'Apache Full'
       sudo ufw delete allow 'Apache'

Quedant així si comprovem l'estat

       sudo ufw status

       Status: active

       To                         Action      From
       --                         ------      ----
       OpenSSH                    ALLOW       Anywhere
       Apache Full                ALLOW       Anywhere
       OpenSSH (v6)               ALLOW       Anywhere (v6)
       Apache Full (v6)           ALLOW       Anywhere (v6)

### 4- Habilitar els canvis i fer funcionar el servidor HTTPS

Ja hem configurat tot el servidor per a que ens faci la connexió HTTPS. Ara la habilitarem per a que funcioni.

Habilitem els moduls relacionats amb la connexió https propis d'Apache2

    sudo a2enmod ssl
    sudo a2enmod headers

Habilitem els arxius modificats previament:

    sudo a2ensite default-ssl
    sudo a2enconf ssl-params

Comprovem que tot està correcte:

    sudo apache2ctl configtest

    Output
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.1.1. Set the 'ServerName' directive globally to suppress this message
Syntax OK

Reiniciem el servidor amb un dels comandaments proposats:

    sudo service apache2 restart
    sudo systemctl restart apache2

Link de referencia del tutorial oficial de digital ocean: 

        https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-16-04#step-1-create-the-ssl-certificate

## Insertar Symfony a Apache2

### Pasos previs

Haurem d'actualitzar el llistat de repositoris i actualitzacions i instal·lar els següents paquets:
    
Actualització paquets de repositoris i actualitzacions

    sudo apt-get update

Instal·lació de git i  phps varis necesaris

    sudo apt-get install git php php-cli php-curl php-xml

Instal·lació de composer mitjançant curl

    sudo curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

### Clonació Projecte

Anem a /var/www i creem la carpeta sermar_enrflo_m12_gestor_porfolis

    cd /var/www
    sudo mkdir sermar_enrflo_m12_gestor_porfolis
    sudo chown user:www-data sermar_enrflo_m12_gestor_porfolis

Seguidament clonem el projecte a la carpeta creada previament

    sudo git clone https://sermarbal@bitbucket.org/enrflojim/sermar_enrflo_m12_gestor_porfolis.git sermar_enrflo_m12_gestor_porfolis

###Possar permisos:

El propietari de la carpeta hauria de ser l'usuari user que fem servir però si no ho es fem servir el següent comandament:

    sudo chown -R user:user sermar_enrflo_m12_gestor_porfolis
    sudo chmod -R 775 sermar_enrflo_m12_gestor_porfolis

I ara permetem que internament l'usuari gestor d'apache pugui modificar els següents directoris

    sudo setfacl -R -m u:www-data:rX sermar_enrflo_m12_gestor_porfolis
    sudo setfacl -R -m u:www-data:rwX sermar_enrflo_m12_gestor_porfolis/var/cache/ sermar_enrflo_m12_gestor_porfolis/var/logs sermar_enrflo_m12_gestor_porfolis/var/sessions sermar_enrflo_m12_gestor_porfolis/web 
    sudo setfacl -dR -m u:www-data:rwX sermar_enrflo_m12_gestor_porfolis/var/cache sermar_enrflo_m12_gestor_porfolis/var/logs sermar_enrflo_m12_gestor_porfolis/var/sessions sermar_enrflo_m12_gestor_porfolis/web 

Introduim la variable d'entorn necessaria per a que symfony es pugui gestionar.

    export SYMFONY_ENV=prod

And change the line of the file web/app.php

    Modificar $kernel = new AppKernel('prod', false);

to

    $kernel = new AppKernel('prod', true);

###Apache2 configuration files

####Time zone php configuration

Modify the time/zone of the file /etc/php/7.0/apache2/php.ini

    ...
    [Date]
    ; Defines the default timezone used by the date functions
    ; http://php.net/date.timezone
    date.timezone = Europe/Madrid
    ...

000-default.conf (port 80 file http connection):

    ...
    ServerAdmin portfoliscontact@gmail.com
    DocumentRoot /var/www/sermar_enrflo_m12_gestor_porfolis/web
    Redirect permanent "/" "https://192.168.205.158"
    ...

default-ssl.conf (port 443 file https connection):

    ...
    ServerAdmin portfoliscontact@gmail.com
    ServerName 192.168.205.158
    DocumentRoot /var/www/sermar_enrflo_m12_gestor_porfolis/web
    <Directory /var/www/sermar_enrflo_m12_gestor_porfolis/web>
        Require all granted
        AllowOverride All
        Order Allow,Deny
        Allow from All
        <IfModule mod_rewrite.c>
            Options -MultiViews
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*)$ app.php [QSA,L]
        </IfModule>
    </Directory>
    ...

Habilitem el modul de reescriure les urls per a que no surti app.php a la url

    sudo a2enmod rewrite

# Ja funciona el servidor apache2 https amb symfony.

### Configurar MYSQL

Per crear la base de dades mitjançant la consola de symfony anirem a l'arrel del projecte e introduirem els següents comandaments

Base del projecte

    cd /var/www/sermar_enrflo_m12_gestor_porfolis

Comandaments a executar per crear la database, les entitats i actualitzar l'esquema.

    php bin/console doctrine:database:drop --force
    php bin/console doctrine:database:create
    php bin/console  doctrine:generate:entities AppBundle
    php bin/console doctrine:schema:update --force

### Creació d'usuari gestor de la bd de symfony

Ens loguem com a root

    mysql -u root -p

Com que ja tenim la db creada mitjançant l'usuari creem l'usuari i li donem privilegis

    CREATE USER 'user'@'localhost' IDENTIFIED BY 'ausias';
    GRANT ALL PRIVILEGES ON SerMar_EnrFlo_Portfolis.* TO 'user'@'localhost';
    FLUSH PRIVILEGES;
    quit;

Provem a loguejar-nos amb el nou usuari i comprovem que té la db associada

    mysql -u user -p
    SHOW DATABASES;

    Output
    +-------------------------+
    | Database                |
    +-------------------------+
    | information_schema      |
    | SerMar_EnrFlo_Portfolis |
    +-------------------------+
    quit;

## Instal·lació symfony al labs

Si voleu treballar amb symfony podeu fer-ho. Teniu oberts els ports del 9000 al 10.000. Sí per exemple tothom fa servir com a referència l'IP del seu terminal real no hi haurà problemes
Igualment, també podeu publicar les vostres aplicacions al vostre directory "public_html"
Per llençar-lo i fer-lo accessible des de l'exterior heu d'indicar l'IP 0.0.0.0

    php bin/console server:run 0.0.0.0:9159

Si teniu problemes de permisos heu d'accedir a al fitxer web/web_app.php i comentar la línia exit 

Modificar Parameters.yml

    ...
    database_name: a14sermarbal_Portfolis 
    database_user: a14sermarbal_se
    ...

Modificar database.php (web/php/database.php)

    public function __construct() {
        try{
            // Linea a cambiar
            parent::__construct('mysql:host=localhost;dbname=a14sermarbal_Portfolis','a14sermarbal_se','ausias');
            parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->exec("set names utf8");
        } catch (Exception $ex) {
            echo "Error al conectar a la base de datos. <br>".$ex;
        }

    }


Links:

https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-16-04#step-5-test-encryption

https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04#step-2-%E2%80%94-configuring-mysql

https://tecadmin.net/install-symphony-3-framework-on-ubuntu/

http://stackoverflow.com/questions/12037938/symfony-error-500-with-app-php-works-fine-on-app-dev-php

http://symfony.com/doc/current/setup/web_server_configuration.html