<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="card")
 */
class Card{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

	/**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @Assert\Url(
     *    message = "The url '{{ value }}' is not a valid url",
     * )
     */
    protected $link;

    /**
     * @ORM\ManyToOne(targetEntity="Portfoli", inversedBy="cards")
     * @ORM\JoinColumn(name="portfoli_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $idPortfoli;

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="idCard")
     */
    protected $articles;

    /**
     * @ORM\ManyToMany(targetEntity="Subcategory", inversedBy="cards")
     * @ORM\JoinTable(name="cards_categories")
     */
    protected $subCategories;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    protected $dateCard;

    /**
    * @ORM\Column(type="string", length=100)
    */
    protected $dateStringCard;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subCategories = new ArrayCollection();
        $this->articles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Card
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Card
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Card
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set idPortfoli
     *
     * @param \AppBundle\Entity\Portfoli $idPortfoli
     *
     * @return Card
     */
    public function setIdPortfoli(\AppBundle\Entity\Portfoli $idPortfoli = null)
    {
        $this->idPortfoli = $idPortfoli;

        return $this;
    }

    /**
     * Get idPortfoli
     *
     * @return \AppBundle\Entity\Portfoli
     */
    public function getIdPortfoli()
    {
        return $this->idPortfoli;
    }

    /**
     * Add article
     *
     * @param \AppBundle\Entity\Article $article
     *
     * @return Card
     */
    public function addArticle(\AppBundle\Entity\Article $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \AppBundle\Entity\Article $article
     */
    public function removeArticle(\AppBundle\Entity\Article $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Add subCategory
     *
     * @param \AppBundle\Entity\Subcategory $subCategory
     *
     * @return Card
     */
    public function addSubCategory(\AppBundle\Entity\Subcategory $subCategory)
    {
        $this->subCategories[] = $subCategory;

        return $this;
    }

    /**
     * Remove subCategory
     *
     * @param \AppBundle\Entity\Subcategory $subCategory
     */
    public function removeSubCategory(\AppBundle\Entity\Subcategory $subCategory)
    {
        $this->subCategories->removeElement($subCategory);
    }

    /**
     * Get subCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubCategories()
    {
        return $this->subCategories;
    }

    /**
     * Set dateCard
     *
     * @param \DateTime $dateCard
     *
     * @return Card
     */
    public function setDateCard($dateCard)
    {
        $this->dateCard = $dateCard;

        return $this;
    }

    /**
     * Get dateCard
     *
     * @return \DateTime
     */
    public function getDateCard()
    {
        return $this->dateCard;
    }

    /**
     * Set dateStringCard
     *
     * @param string $dateStringCard
     *
     * @return Card
     */
    public function setDateStringCard($dateStringCard)
    {
        $this->dateStringCard = $dateStringCard;

        return $this;
    }

    /**
     * Get dateStringCard
     *
     * @return string
     */
    public function getDateStringCard()
    {
        return $this->dateStringCard;
    }
}
