<?php

/*
 * By Sergi Martínez & Enrique Flo
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="portfoli")
 */
class Portfoli{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $title;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isPremium;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isInformatic;

	   /**
     * @ORM\Column(type="string", length=100)
     */
    protected $imgPortfoli;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="idPortfolis")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $idCategory;

    /**
     * @ORM\OneToMany(targetEntity="Card", mappedBy="idPortfoli")
     */
    protected $cards;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="portfolis")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $idUser;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    protected $datePortfoli;

    /**
    * @ORM\Column(type="string", length=100)
    */
    protected $dateString;

    public function __construct()
    {
        $this->cards = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Portfoli
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set isPremium
     *
     * @param boolean $isPremium
     *
     * @return Portfoli
     */
    public function setIsPremium($isPremium)
    {
        $this->isPremium = $isPremium;

        return $this;
    }

    /**
     * Get isPremium
     *
     * @return boolean
     */
    public function getIsPremium()
    {
        return $this->isPremium;
    }

    /**
     * Set imgPortfoli
     *
     * @param string $imgPortfoli
     *
     * @return Portfoli
     */
    public function setImgPortfoli($imgPortfoli)
    {
        $this->imgPortfoli = $imgPortfoli;

        return $this;
    }

    /**
     * Get imgPortfoli
     *
     * @return string
     */
    public function getImgPortfoli()
    {
        return $this->imgPortfoli;
    }

    /**
     * Set idCategory
     *
     * @param \AppBundle\Entity\Category $idCategory
     *
     * @return Portfoli
     */
    public function setIdCategory(\AppBundle\Entity\Category $idCategory = null)
    {
        $this->idCategory = $idCategory;

        return $this;
    }

    /**
     * Get idCategory
     *
     * @return \AppBundle\Entity\Category
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }

    /**
     * Add card
     *
     * @param \AppBundle\Entity\Card $card
     *
     * @return Portfoli
     */
    public function addCard(\AppBundle\Entity\Card $card)
    {
        $this->cards[] = $card;

        return $this;
    }

    /**
     * Remove card
     *
     * @param \AppBundle\Entity\Card $card
     */
    public function removeCard(\AppBundle\Entity\Card $card)
    {
        $this->cards->removeElement($card);
    }

    /**
     * Get cards
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCards()
    {
        return $this->cards;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return Portfoli
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set datePortfoli
     *
     * @param \DateTime $datePortfoli
     *
     * @return Portfoli
     */
    public function setDatePortfoli($datePortfoli)
    {
        $this->datePortfoli = $datePortfoli;

        return $this;
    }

    /**
     * Get datePortfoli
     *
     * @return \DateTime
     */
    public function getDatePortfoli()
    {
        return $this->datePortfoli;
    }

    /**
     * Set dateString
     *
     * @param string $dateString
     *
     * @return Portfoli
     */
    public function setDateString($dateString)
    {
        $this->dateString = $dateString;

        return $this;
    }

    /**
     * Get dateString
     *
     * @return string
     */
    public function getDateString()
    {
        return $this->dateString;
    }

    /**
     * Set isInformatic
     *
     * @param boolean $isInformatic
     *
     * @return Portfoli
     */
    public function setIsInformatic($isInformatic)
    {
        $this->isInformatic = $isInformatic;

        return $this;
    }

    /**
     * Get isInformatic
     *
     * @return boolean
     */
    public function getIsInformatic()
    {
        return $this->isInformatic;
    }
}
