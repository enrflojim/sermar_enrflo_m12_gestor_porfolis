<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="category")
 */
class Category{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    protected $nameCategory;

    /**
     * @ORM\OneToMany(targetEntity="Subcategory", mappedBy="category")
     */
    protected $subCategories;

    /**
     * @ORM\OneToMany(targetEntity="Portfoli", mappedBy="idCategory")
     */
    protected $idPortfolis;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subCategories = new ArrayCollection();
        $this->idPortfolis = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameCategory
     *
     * @param string $nameCategory
     *
     * @return Category
     */
    public function setNameCategory($nameCategory)
    {
        $this->nameCategory = $nameCategory;

        return $this;
    }

    /**
     * Get nameCategory
     *
     * @return string
     */
    public function getNameCategory()
    {
        return $this->nameCategory;
    }

    /**
     * Add subCategory
     *
     * @param \AppBundle\Entity\Subcategory $subCategory
     *
     * @return Category
     */
    public function addSubCategory(\AppBundle\Entity\Subcategory $subCategory)
    {
        $this->subCategories[] = $subCategory;

        return $this;
    }

    /**
     * Remove subCategory
     *
     * @param \AppBundle\Entity\Subcategory $subCategory
     */
    public function removeSubCategory(\AppBundle\Entity\Subcategory $subCategory)
    {
        $this->subCategories->removeElement($subCategory);
    }

    /**
     * Get subCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubCategories()
    {
        return $this->subCategories;
    }

    /**
     * Add idPortfoli
     *
     * @param \AppBundle\Entity\Portfoli $idPortfoli
     *
     * @return Category
     */
    public function addIdPortfoli(\AppBundle\Entity\Portfoli $idPortfoli)
    {
        $this->idPortfolis[] = $idPortfoli;

        return $this;
    }

    /**
     * Remove idPortfoli
     *
     * @param \AppBundle\Entity\Portfoli $idPortfoli
     */
    public function removeIdPortfoli(\AppBundle\Entity\Portfoli $idPortfoli)
    {
        $this->idPortfolis->removeElement($idPortfoli);
    }

    /**
     * Get idPortfolis
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdPortfolis()
    {
        return $this->idPortfolis;
    }
}
