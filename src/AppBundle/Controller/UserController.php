<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
 * Manages all the methods referred to the users.
 */
class UserController extends Controller {

    /**
     * Allow to register an user.
     * @Route("/register", name="register")
     * @param Request $request Form data receibed.
     */
    public function registroAction(Request $request) {
        // Booleans controlling that the data in mail is a mail and the user is available.
        $errormail = false;
        $erroruser = false;
        // Create the form to show.
        $formUser = new User();
        $form = $this->createFormBuilder($formUser)
                ->add('name', TextType::class, array('label' => 'Name:'))
                ->add('surname', TextType::class, array('label' => 'Surname:'))
                ->add('lastname', TextType::class, array('label' => 'Lastname:'))
                ->add('date', DateType::class, array('label' => 'Date:', 'years' => range(1920, 2025),
                    'attr' => array("class" => "date")))
                ->add('gender', ChoiceType::class, array('label' => 'Gender:',
                    'choices' => array('--- Select sex ---' => "",
                        'Male' => 'Male', 'Female' => 'Female', "Other" => "Other")))
                ->add('phone', TextType::class, array('label' => 'Telephone:'))
                ->add('email', TextType::class, array('label' => 'Email:'))
                ->add('type', ChoiceType::class, array('label' => 'Type street:',
                    'choices' => array('--- Select type street ---' => '',
                        'Avenue' => 'Avenue', 'Street' => 'Street',
                        'Square' => 'Square', "Round" => "Round")))
                ->add('street', TextType::class, array('label' => 'Street:'))
                ->add('number', TextType::class, array('label' => 'Number:', 'required' => false))
                ->add('floor', TextType::class, array('label' => 'Floor:', 'required' => false))
                ->add('door', TextType::class, array('label' => 'Door:', 'required' => false))
                ->add('cp', TextType::class, array('label' => 'Postal code:'))
                ->add('city', TextType::class, array('label' => 'City:'))
                ->add('country', TextType::class, array('label' => 'Country:'))
                ->add('username', TextType::class, array('label' => 'Username:'))
                ->add('plainPassword', RepeatedType::class, array('type' => PasswordType::class,
                    'required' => true,
                    'invalid_message' => 'You entered an invalid value, it should include %num% letters',
                    'first_options' => array('label' => 'Password'),
                    'second_options' => array('label' => 'Rep. Password'),))
                ->add('linkImage', FileType::class, array('label' => false, "attr" => array("class" => "fileimg")))
                ->getForm();

        // Get the data form.
        $form->handleRequest($request);

        // Check if the form is submitted and is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            // Check if the mail is used by another user.
            $repository = $this->getDoctrine()->getRepository('AppBundle:User');
            $user = $repository->findOneByEmail($formUser->getEmail());
            if (count($user) == 0) {
                // Check if the username is used by another user.
                $repository = $this->getDoctrine()->getRepository('AppBundle:User');
                $user = $repository->findOneByUsername($formUser->getUsername());
                if (count($user) == 0) {
                    // Check if the form has an image and insert the current datetime to the image name.
                    if ($formUser->getLinkImage() != null) {
                        $dateTime = date('m-d-Y_h-i-s_a', time());
                        $file = $formUser->getLinkImage();
                        $formUser->setLinkImage($dateTime . $file->getClientOriginalName());
                        $cvDir = $this->container->getparameter('kernel.root_dir') . '/../web/img/images';
                        $file->move($cvDir, $formUser->getLinkImage());
                    } else {
                        // Set the default image.
                        $formUser->setLinkImage("asd.jpg");
                    }
                    // Encode the password for security and put it.
                    $password = $this->get('security.password_encoder')
                            ->encodePassword($formUser, $formUser->getPlainPassword());
                    $formUser->setPassword($password);
                    // Insert the new user.
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($formUser);
                    $em->flush();
                    // Redirect to login template.
                    return $this->redirectToRoute('login');
                } else {
                    // Updates the control booleans.
                    $erroruser = true;
                    $errormail = false;
                }
            } else {
                // Updates the control booleans.
                $errormail = true;
                $erroruser = false;
            }
        }
        // Call the form register template.
        return $this->render('default/user/form_register.html.twig', array(
                    'title' => 'Insert User',
                    "name" => "",
                    "errormail" => $errormail,
                    "erroruser" => $erroruser,
                    'form' => $form->createView(),));
    }

    /**
     * Allow to login an user.
     * @Route("/", name="login")
     */
    public function loginAction() {
        // if the user is already logged it is redirected to his portfolis.
        if ($this->get('security.authorization_checker')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            return $this->redirectToRoute('myportfolis');
        }
        // Get authentication utils by Symfony.
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        
        // Call the login template.
        return $this->render('default/user/login.html.twig', array(
                    'last_username' => $lastUsername,
                    'error' => $error,
        ));
    }

    /**
     * Allow to edit an existing user.
     * @Route("/edituser", name="edituser")
     * @param Request $request Form data receibed.
     */
    public function editUserAction(Request $request) {
        // Get the current user.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        // booleans to control the form mail and username.
        $errormail = false;
        $erroruser = false;
        // Get data from the current user.
        $mail = $user->getEmail();
        $username = $user->getUsername();
        $userimage = $user->getLinkImage();
        // Creates the user's edit form.
        $form = $this->createFormBuilder($user)
                ->add('name', TextType::class, array('label' => 'Name:'))
                ->add('surname', TextType::class, array('label' => 'Surname:'))
                ->add('lastname', TextType::class, array('label' => 'Lastname:'))
                ->add('date', DateType::class, array('label' => 'Date:', "years" => range(1920, 2025),
                    'attr' => array("class" => "date")))
                ->add('gender', ChoiceType::class, array('label' => 'Sexe:',
                    'choices' => array('--- Select sex ---' => "",
                        'Male' => 'Male', 'Female' => 'Female')))
                ->add('phone', TextType::class, array('label' => 'Telephone:'))
                ->add('email', TextType::class, array('label' => 'Email:'))
                ->add('type', ChoiceType::class, array('label' => 'Type street:',
                    'choices' => array('--- Select type street ---' => '',
                        'Avenue' => 'Avenue', 'Street' => 'Street',
                        'Square' => 'Square')))
                ->add('street', TextType::class, array('label' => 'Street:'))
                ->add('number', TextType::class, array('label' => 'Number:', 'required' => false))
                ->add('floor', TextType::class, array('label' => 'Floor:', 'required' => false))
                ->add('door', TextType::class, array('label' => 'Door:', 'required' => false))
                ->add('cp', TextType::class, array('label' => 'Postal code:'))
                ->add('city', TextType::class, array('label' => 'City:'))
                ->add('country', TextType::class, array('label' => 'Country:'))
                ->add('username', TextType::class, array('label' => 'Username:'))
                ->add('linkImage', FileType::class, array("label" => false, "required" => false, 'data_class' => null,
                    "attr" => array("class" => "fileimg")))
                ->getForm();

        // Get the data by the form.
        $form->handleRequest($request);
        // Check if the form is submitted and if it is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            // Check if the email is already used.
            $repository = $this->getDoctrine()->getRepository('AppBundle:User');
            $userup = $repository->findOneByEmail($user->getEmail());
            if (count($userup) == 0 || (count($userup) == 1 && $userup->getEmail() == $mail)) {
                // Check if the user is already used.
                $repository = $this->getDoctrine()->getRepository('AppBundle:User');
                $userup = $repository->findOneByUsername($user->getUsername());
                if (count($userup) == 0 || (count($userup) == 1 && $userup->getUsername() == $username)) {
                    // Check if a new image is inserted. If it is the case we concatenate the datetime and put it in the server.
                    if ($user->getLinkImage() == null) {
                        $file = $userimage;
                        $user->setLinkImage($file);
                    } else {
                        $dateTime = date('m-d-Y_h-i-s_a', time());
                        $file = $user->getLinkImage();
                        $user->setLinkImage($dateTime . $file->getClientOriginalName());
                        $cvDir = $this->container->getparameter('kernel.root_dir') . '/../web/img/images';
                        $file->move($cvDir, $user->getLinkImage());
                    }
                    // Updates the user data.
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                } else {
                    // Updates the boolean control variables
                    $erroruser = true;
                    $errormail = false;
                }
            } else {
                // Updates the boolean control variables
                $errormail = true;
                $erroruser = false;
            }
        }
        // Updates the route for the image.
        $user->setLinkImage("img/images/" . $user->getLinkImage());
        
        // Call the form update user template.
        return $this->render('default/user/form_update.html.twig', array(
                    'title' => 'Update User',
                    "user" => $user->getName() . " " . $user->getSurname(),
                    "img" => $user->getLinkImage(),
                    "errormail" => $errormail,
                    "erroruser" => $erroruser,
                    'form' => $form->createView(),));
    }

    /**
     * Alow to recovery the password of an user.
     * @Route("/forgotpass", name="forgotpass")
     * @param Request $request Form data receibed.
     */
    public function forgotUserAction(Request $request) {
        // Get the current user logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        // Boolean controlling the mail is valid.
        $errormail = true;
        // Creates the forgot password form.
        $userPass = new User();
        $form = $this->createFormBuilder($userPass)
                ->add('email', TextType::class, array('label' => 'Email:'))
                ->add('plainPassword', RepeatedType::class, array('type' => PasswordType::class,
                    'required' => true,
                    'invalid_message' => 'You entered an invalid value, it should include %num% letters',
                    'first_options' => array('label' => 'Password'),
                    'second_options' => array('label' => 'Rep. Password')))
                ->getForm();

        // Get the data by the form.
        $form->handleRequest($request);
        
        // Check if the form is submitted and if it is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            // Check if the mail is used by an user.
            $repository = $this->getDoctrine()->getRepository('AppBundle:User');
            $userup = $repository->findOneByEmail($userPass->getEmail());
            if (count($userup) == 1) {
                // if it is used by an user we allow to change the password.
                $password = $this->get('security.password_encoder')
                        ->encodePassword($userup, $userPass->getPlainPassword());
                // Updates the password 
                $userup->setPassword($password);
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                // Redirect to login template.
                return $this->redirectToRoute("login");
            } else {
                // Updates the mail boolean.
                $errormail = false;
            }
        }
        // Call the forgot password template.
        return $this->render('default/user/form_forgot.html.twig', array(
                    'title' => 'Forgot Password',
                    "errormail" => $errormail,
                    'form' => $form->createView(),));
    }

}
