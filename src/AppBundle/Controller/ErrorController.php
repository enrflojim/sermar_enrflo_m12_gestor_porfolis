<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Card;

/**
 * Manage all the methods referred to Articles.
 */
class ErrorController extends Controller {

    /**
     * Sacces denied in this page
     * @Route("/accesdenied", name="accesdenied")
     */
    public function accessDeniedction() {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        // Get the current user's full name.
        $name = $user->getName() . " " . $user->getSurname();
        // Call the list articles template.
        return $this->render("default/error/acces_denied.html.twig", array("user"=>$name));
    }

    /**
     * Sacces denied in this page
     * @Route("/notfound", name="notfound")
     */
    public function norFoundAction() {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        // Get the current user's full name.
        $name = $user->getName() . " " . $user->getSurname();
        // Call the list articles template.
        return $this->render("default/error/not_found.html.twig", array("user"=>$name));
    }

}
