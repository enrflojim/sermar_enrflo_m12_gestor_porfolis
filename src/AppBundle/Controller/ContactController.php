<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Portfoli;

/**
 * Manages all the methods referred to contacting.
 */
class ContactController extends Controller {

    /**
     * Allow to contact the user of the portfoli.
     * @Route("/contactme/{portfoli}", name="contactme")
     * @param Request $request Form data receibed.
     * @param Portfoli $portfoli Portfoli of the user target.
     */
    public function contactMeFormAction(Request $request, Portfoli $portfoli) {

        // Get the email from the portfoli's user.
        $emailUserPortfoli = $portfoli->getIdUser()->getEmail();

        // Get the current user.
        $user = $this->get('security.token_storage')->getToken()->getUser();

	if($user != "anon.")
            $user = $user->getName() . " " . $user->getSurname();

        // Create the contact form
        $defaultData = array();
        $form = $this->createFormBuilder($defaultData)
                ->add('name', TextType::class, array('label' => 'Name:', 'required' => true))
                ->add('surname', TextType::class, array('label' => 'Surname:', 'required' => true))
                ->add('lastname', TextType::class, array('label' => 'Lastname:', 'required' => true))
                ->add('email', EmailType::class, array('label' => 'Email:', 'required' => true))
                ->add('phone', TextType::class, array('label' => 'Phone:', 'required' => false))
                ->add('content', TextareaType::class, array('label' => 'Message content:', 'required' => true))
                ->add('send', SubmitType::class, array('attr' => array('class' => 'btn btn-primary btn-md col-md-offset-3')))
                ->getForm();

        // Get the form data.
        $form->handleRequest($request);

        // Check if the form is submitted and if it is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            // insert the form data to an array.
            $data = $form->getData();

            // Prepare the message to be sended by swiftmailer.
            $message = \Swift_Message::newInstance()
                    ->setSubject($data["name"])
                    ->setFrom($emailUserPortfoli)
                    ->setTo($emailUserPortfoli)
                    ->setBody(
                    $this->renderView('default/varios/contact_email.html.twig', array("data" => $data)), 'text/html'
            );
            // Send the message.
            $this->get('mailer')->send($message);
            // Redirect to portfolis list.
            return $this->redirectToRoute('portfolis');
        }

        // Call the contactme template.
        return $this->render('default/varios/contactme.html.twig', array(
		    'rute' => $_SESSION["rute"],
                    'title' => 'Contact me',
                    "user" => $user,
                    'form' => $form->createView(),
                    'portfoli' => $portfoli
        ));
    }

    /**
     * Allow to contact the properties of the page (us).
     * @Route("/contact", name="contact")
     * @param Request $request Form data receibed.
     */
    public function contactFormAction(Request $request) {
        // Get the current user.
        $name = "";
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            $name = $user;
        } else {
            $name = $user->getName() . " " . $user->getSurname();
        }
        // Creates the contact form.
        $defaultData = array();
        $form = $this->createFormBuilder($defaultData)
                ->add('name', TextType::class, array('label' => 'Name:', 'required' => true))
                ->add('surname', TextType::class, array('label' => 'Surname:', 'required' => true))
                ->add('lastname', TextType::class, array('label' => 'Lastname:', 'required' => true))
                ->add('email', EmailType::class, array('label' => 'Email:', 'required' => true))
                ->add('phone', TextType::class, array('label' => 'Phone:', 'required' => false))
                ->add('content', TextareaType::class, array('label' => 'Message content:', 'required' => true))
                ->add('send', SubmitType::class, array('attr' => array('class' => 'btn btn-primary btn-md col-md-offset-3')))
                ->getForm();

        // Get the data form.
        $form->handleRequest($request);

        // Check if the form is submitted and if it is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            // Prepare the message to be sended by swiftmailer.
            $message = \Swift_Message::newInstance()
                    ->setSubject($data["name"])
                    ->setFrom('a14sermarbal@iam.cat')
                    ->setTo("a14sermarbal@iam.cat")
                    ->setBody(
                    $this->renderView('default/varios/contact_email.html.twig', array("data" => $data)), 'text/html'
            );

            // Send message.
            $this->get('mailer')->send($message);
            // Redirect to portfolis.
            return $this->redirectToRoute('portfolis');
        }

        // Call the contact template.
        return $this->render('default/varios/contact.html.twig', array(
                    'title' => 'Contact us',
                    "user" => $name,
                    'form' => $form->createView(),));
    }

}
