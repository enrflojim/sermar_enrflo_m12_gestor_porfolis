<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Portfoli;
use AppBundle\Entity\Card;
use AppBundle\Functions\Functions;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\ORM\EntityRepository;

/**
 * Manages all the methods referred to cards.
 */
class CardsController extends Controller {

    /**
     * show all the cards of a portfoli.
     * @Route("/cards/{portfoli}", name="cards")
     * @param Portfoli $portfoli portfoli cards owner.
     */
    public function listCardsAction(Portfoli $portfoli) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $portfoli->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Call the cards list template.
        return $this->render('default/cards/list_cards.html.twig', array(
                    "user" => $user->getName() . " " . $user->getSurname(),
                    "rute" => $_SESSION["rute"],
                    "portfoli" => $portfoli));
    }

    /**
     * Allow to create a card.
     * @Route("/formcard/{portfoli}", name="formcard")
     * @param Request $request Form data receibed.
     * @param Portfoli $portfoli Portfoli where we create the card.
     */
    public function createCardAction(Request $request, Portfoli $portfoli) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $portfoli->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Create the card form.
        $formCard = new Card();
        $form = $this->createFormBuilder($formCard)
                ->add('title', TextType::class, array('label' => 'Name:'))
                ->add('description', TextareaType::class, array('label' => 'description:', "required" => false,
                    "attr" => array("rows" => 6, "cols" => 45)))
                ->add('subCategories', EntityType::class, array('class' => 'AppBundle:Subcategory',
                    "multiple" => true,
                    "attr" => array("class" => "tam"),
                    'query_builder' => function (EntityRepository $er) use ( $portfoli ) {
                        return $er->createQueryBuilder('s')->join("s.category", "c")
                                ->where('c.id=:id ')
                                ->setParameter("id", $portfoli->getIdCategory()->getId());
                    },
                    'choice_label' => 'name_subcategory', 'label' => 'Categories'))
                ->add('link', TextType::class, array('label' => 'Link:', "required" => false))
                ->getForm();

        // Get the form data.
        $form->handleRequest($request);

        // Check if the form is submitted and if it is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            $formCard->setDateCard(new \DateTime("now"));
            $formCard->setDateStringCard(date("F") . " " . date("j") . "," . date("Y"));
            $formCard->setIdPortfoli($portfoli);
            // Insert the new card.
            $em = $this->getDoctrine()->getManager();
            $em->persist($formCard);
            $em->flush();
            // Redirect the route to portfoli cards template.
            return $this->redirectToRoute('cards', array("portfoli" => $portfoli->getId()));
        }

        // Call the create card template.
        return $this->render('default/cards/create_card.html.twig', array(
                    "user" => $user->getName() . " " . $user->getSurname(),
                    "rute" => $_SESSION["rute"],
                    'title' => 'Create Card',
                    'portfoli' => $portfoli,
                    'form' => $form->createView(),));
    }

    /**
     * Allow to edit a card.
     * @Route("/editcard/{card}", name="editcard")
     * @param Request $request Form data receibed
     * @param Card $card Card to edit.
     */
    public function editCardAction(Request $request, Card $card) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $card->getIdPortfoli()->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Creates the card form by the existing card data.
        $portfoli = $card->getIdPortfoli();
        $form = $this->createFormBuilder($card)
                ->add('title', TextType::class, array('label' => 'Name:'))
                ->add('description', TextareaType::class, array('label' => 'description:',
                    "attr" => array("rows" => 6, "cols" => 35)))
                ->add('subCategories', EntityType::class, array('class' => 'AppBundle:Subcategory',
                    "multiple" => true,
                    "attr" => array("class" => "tam"),
                    'query_builder' => function (EntityRepository $er) use ( $portfoli ) {
                        return $er->createQueryBuilder('s')->join("s.category", "c")
                                ->where('c.id=:id ')
                                ->setParameter("id", $portfoli->getIdCategory()->getId());
                    },
                    'choice_label' => 'name_subcategory', 'label' => 'Categories'))
                ->add('link', TextType::class, array('label' => 'Link:', "required" => false))
                ->getForm();

        // Get the form data.
        $form->handleRequest($request);

        // Check if the form is submitted and if it is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            // Updates the card.
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            // Redirect to card lists.
            return $this->redirectToRoute('cards', array("portfoli" => $card->getIdPortfoli()->getId()));
        }
        // Call the create card template.
        return $this->render('default/cards/create_card.html.twig', array(
                    "user" => $user->getName() . " " . $user->getSurname(),
                    "rute" => $_SESSION["rute"],
                    'title' => 'Edit Card',
                    'portfoli' => $portfoli,
                    'form' => $form->createView(),));
    }

    /**
     * Allow to remove a card.
     * @Route("/deletecard/{card}", name="deletecard")
     * @param Card $card Card to remove.
     */
    public function deleteArticleAction(Card $card) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $card->getIdPortfoli()->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Remove the card
        $em = $this->getDoctrine()->getManager();
        $em->remove($card);
        $em->flush();
        // Call the list cards template.
        return $this->render('default/cards/list_cards.html.twig', array(
                    "user" => $user->getName() . " " . $user->getSurname(),
                    "rute" => $_SESSION["rute"],
                    "portfoli" => $card->getIdPortfoli()));
    }

    /**
     * Show a card depending it it is a programmer card or a global card.
     * @Route("/card/{card}", name="card")
     * @param Card $card Card to show.
     */
    public function test1Action(Card $card) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            $name = $user;
            $_SESSION["rute"] = "all";
        }
        elseif ($user->getUsername() != $card->getIdPortfoli()->getIdUser()->getUsername()) {
            $name = $user->getName() . " " . $user->getSurname();
            $_SESSION["rute"] = "all";
        }
        else{
            $name = $user->getName() . " " . $user->getSurname();
            $_SESSION["rute"] = "user";
        }
        // Manages do not repeat the subcategories in the accordion.
        $fun = new Functions();
        $subcategories = $fun->unique($card->getIdPortfoli()->getCards());

        // Check and update the card type.
        $typeCard = "";
        if ($card->getIdPortfoli()->getIsInformatic()) {
            $typeCard = 'default/cardTemplates/programmerCard.html.twig';
        } else {
            $typeCard = 'default/cardTemplates/allCard.html.twig';
        }

        // Call the programmer card template or the global card template depending the card type.
        return $this->render($typeCard, array(
                    "card" => $card,
                    "portfoli" => $card->getIdPortfoli(),
                    "rute" => $_SESSION["rute"],
                    "sub" => $subcategories,
                    "user" => $name
        ));
    }

}
