<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Portfoli;
use AppBundle\Functions\Functions;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
 * Manage all methods refered to Portfolis
 */
class PortfolisController extends Controller {

    /**
     * Show the global portfolis page.
     * @Route("/portfolis", name="portfolis")
     */
    public function portfolisAction() {
        // Controls the header for show only the global options (not the user logged options).
        $_SESSION["rute"] = "all";

        // Query to database for all portfolis
        $portfolis = $this->getDoctrine()
                ->getRepository('AppBundle:Portfoli')
                ->findAll();
        // if user is logged take the name if not it's null.
        $name = "";
        if ($this->getUser() != null) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $name = $user->getName() . " " . $user->getSurname();
        }
        // Call the portfolisList template
        return $this->render('default/portfolis/portfolisList.html.twig', array(
                    "user" => $name, "portfolis" => $portfolis));
    }

    /**
     * Show the portfolis wanted by the search credentials.
     * @Route("/searchportfolis", name="searchportfolis")
     */
    public function searchPortfolisAction() {
        // Controls the header for show only the global options (not the user logged options).
        $_SESSION["rute"] = "all";

        // if user is logged take the name if not it's null.
        $name = "";
        if ($this->getUser() != null) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $name = $user->getName() . " " . $user->getSurname();
        }

        // create a list of portfolis with the given credentials.
        $portfolis = new Portfoli();
        $em = $this->getDoctrine()->getRepository('AppBundle:Portfoli');
        $query = $em->createQueryBuilder('p')->join('p.cards', 'ca')->leftJoin('ca.subCategories', 's')->join('s.category', 'c')
                ->where('c.nameCategory = :nombre OR s.nameSubcategory = :nombre')->setParameter('nombre', $_GET["tag"])
                ->getQuery();
        $portfolis = $query->getResult();
        // Call the portfolisTemplate using the query with the filtred portfolis.
        return $this->render('default/portfolis/portfolisList.html.twig', array(
                    "user" => $name, "portfolis" => $portfolis));
    }

    /**
     * Show the content of a portfoli (the cards).
     * @Route("/portfoli/{portfoli}", name="portfoli")
     * @param Portfoli $portfoli Portfoli to show cards.
     */
    public function portfoliAction(Portfoli $portfoli) {
        // Get the current user.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            $name = $user;
            $_SESSION["rute"] = "all";
        }
        elseif ($user->getUsername() != $portfoli->getIdUser()->getUsername()) {
            $name = $user->getName() . " " . $user->getSurname();
            $_SESSION["rute"] = "all";
        }
        else{
            $name = $user->getName() . " " . $user->getSurname();
            $_SESSION["rute"] = "user";
        }

        // Manages do not repeat the subcategories in the accordion.
        $fun = new Functions();
        $subcategories = $fun->unique($portfoli->getCards());

        // Call the portfoli cards template.
        return $this->render('default/portfolis/portfoli.html.twig', array(
                    "rute" => $_SESSION["rute"],
                    "user" => $name,
                    "sub" => $subcategories,
                    "portfoli" => $portfoli));
    }

    /**
     * Allow to create a portfoli.
     * @Route("/createportfoli", name="createportfoli")
     * @param Request $request Form data receibed.
     */
    public function createPortfoliAction(Request $request) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }

        // Controls if the current user has a free portfoli already.
        $free = "";
        // Get get the portfolis by the current user.
        $portfolis = $this->getDoctrine()
                ->getRepository('AppBundle:Portfoli')
                ->findByIdUser($user->getId());
        // Creates the Portfoli creation form.
        $formPortfoli = new Portfoli();
        $form = $this->createFormBuilder($formPortfoli)
                ->add('title', TextType::class, array('label' => 'Name:', 'required' => true))
                ->add('isPremium', CheckboxType::class, array('label' => 'Is premium:', "required" => false))
                ->add('isInformatic', CheckboxType::class, array('label' => 'Is Informatic:', "required" => false))
                ->add('idCategory', EntityType::class, array('class' => 'AppBundle:Category',
                    'choice_label' => 'name_category', 'label' => 'Categories'))
                ->add('imgPortfoli', FileType::class, array('label' => false, 'required' => false, "attr" => array("class" => "fileimg")))
                ->add('Create Portfoli', SubmitType::class, array('attr' => array('class' => 'btn btn-primary btn-md col-md-offset-3')))
                ->getForm();

        // Get the data form.
        $form->handleRequest($request);

        // Check if the form is submitted and is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            // Checks if the current user has a free portfoli
            $premiun = true;
            foreach ($portfolis as $value) {
                if ($value->getIsPremium() == 0) {
                    $premiun = false;
                    break;
                }
            }
            // Updates the free values depending if the user has or not a free portfoli already.
            if ($premiun == false && $premiun == $formPortfoli->getIsPremium()) {
                $free = "free";
            } else {
                // Check if the portfoli form has an image and insert the current datetime to the image name.
                if ($formPortfoli->getImgPortfoli() != null) {
                    $dateTime = date('m-d-Y_h-i-s_a', time());
                    $file = $formPortfoli->getImgPortfoli();
                    $formPortfoli->setImgPortfoli($dateTime.$file->getClientOriginalName());
                    $cvDir = $this->container->getparameter('kernel.root_dir') . '/../web/img/portfolis';
                    $file->move($cvDir, $formPortfoli->getImgPortfoli());
                }
                // Insert the portfoli data.
                $formPortfoli->setIdUser($user);
                $formPortfoli->setDatePortfoli(new \DateTime("now"));
                $formPortfoli->setDateString(date("F") . " " . date("j") . " " . date("Y"));
                $em = $this->getDoctrine()->getManager();
                $em->persist($formPortfoli);
                $em->flush();
                // Redirect to myportfolis user's page.
                return $this->redirectToRoute('myportfolis');
            }
        }
        // Call the create portfoli template.
        return $this->render('default/portfolis/create_portfoli.html.twig', array(
                    'title' => 'Create Portfoli',
                    "free" => $free,
                    "user" => $user->getName() . " " . $user->getSurname(),
                    'form' => $form->createView(),));
    }

    /**
     * Allow to edit an existing portfoli.
     * @Route("/editportfoli/{portfoli}", name="editeportfoli")
     * @param Request $request Form data receibed.
     * @param Portfoli $portfoli Portfoli to edit.
     */
    public function editPortfoliAction(Request $request, Portfoli $portfoli) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $portfoli->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Create the portfoli edit form.
        $form = $this->createFormBuilder($portfoli)
                ->add('title', TextType::class, array('label' => 'Name:', 'required' => true))
                ->add('imgPortfoli', FileType::class, array('label' => false, 'required' => true, 'data_class' => null))
                ->getForm();

        // Get the data form.
        $form->handleRequest($request);

        // Check if the form is submitted and is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            // Check if the portfoli form has an image and insert the current datetime to the image name.
            if ($portfoli->getImgPortfoli() != null) {
                $dateTime = date('m-d-Y_h-i-s_a', time());
                $file = $portfoli->getImgPortfoli();
                $portfoli->setImgPortfoli($dateTime.$file->getClientOriginalName());
                $cvDir = $this->container->getparameter('kernel.root_dir') . '/../web/img/portfolis';
                $file->move($cvDir, $portfoli->getImgPortfoli());
            }
            // Update the portfoli data.
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            // Redirect to portfoli template with the portfoli id.
            return $this->redirectToRoute('portfoli', array("portfoli" => $portfoli->getId()));
        }
        // Call the edit portfoli template.
        return $this->render('default/portfolis/edit_portfoli.html.twig', array(
                    'title' => 'Edit Image Portfoli',
                    "user" => $user->getName() . " " . $user->getSurname(),
                    "portfoli" => $portfoli,
                    "rute" => $_SESSION["rute"],
                    'form' => $form->createView(),));
    }

    /**
     * Show the portfolis of the current user.
     * @Route("/myportfolis", name="myportfolis")
     */
    public function myPortfolisAction() {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        // Allow to show the logged user buttons in the header.
        $_SESSION["rute"] = "user";
        // Get the full name for the header.
        $name = $user->getName() . " " . $user->getSurname();
        // Get portfolis by id user.
        $portfolis = $this->getDoctrine()
                ->getRepository('AppBundle:Portfoli')
                ->findByIdUser($user->getId());
        // Call the portfolis list template.
        return $this->render('default/portfolis/portfolisList.html.twig', array(
                    "user" => $name,
                    "portfolis" => $portfolis));
    }

    /**
     * Allow to remove an existent template of the current user.
     * @Route("/deleteportfoli/{portfoli}", name="deleteportfoli")
     * @param Portfoli $portfoli Portfoli to remove.
     */
    public function deletePortfoliAction(Portfoli $portfoli) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $portfoli->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Removes the portfoli receibed by url.
        $em = $this->getDoctrine()->getManager();
        $em->remove($portfoli);
        $em->flush();
        // Redirect to my portfolis template.
        return $this->redirectToRoute('myportfolis');
    }

}
