<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Card;
use AppBundle\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/**
 * Manage all the methods referred to Articles.
 */
class ArticleController extends Controller {

    /**
     * Show the articles list of a card.
     * @Route("/articles/{card}", name="articles")
     * @param Card $card card that contains the articles to show.
     */
    public function listArticlesAction(Card $card) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $card->getIdPortfoli()->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Get the current user's full name.
        $name = $user->getName() . " " . $user->getSurname();
        // Call the list articles template.
        return $this->render('default/articles/list_article.html.twig', array(
                    "user" => $name,
                    "rute" => $_SESSION["rute"],
                    "portfoli" => $card->getIdPortfoli(),
                    "card" => $card));
    }

    /**
     * Allow to create an article.
     * @Route("/formarticle/{card}", name="formarticle")
     * @param Request $request data form receibed.
     * @param Card $card card articles owner
     */
    public function createArticlesAction(Request $request, Card $card) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $card->getIdPortfoli()->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Get the current user's full name.
        $name = $user->getName() . " " . $user->getSurname();
        // Create the article creation form.
        $formCard = new Article();
        $form = $this->createFormBuilder($formCard)
                ->add('title', TextType::class, array('label' => 'Name:'))
                ->add('content', TextareaType::class, array('label' => 'Content:', "required" => false,
                    "attr" => array("rows" => 6, "cols" => 45)))
                ->getForm();

        // Get the form data.
        $form->handleRequest($request);

        // Check if the form is submitted and if it is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            $formCard->setIdCard($card);
            // Insert the article.
            $em = $this->getDoctrine()->getManager();
            $em->persist($formCard);
            $em->flush();
            // Redirect to article list template.
            return $this->redirectToRoute('articles', array("card" => $card->getId()));
        }

        // Call the create article template.
        return $this->render('default/articles/create_article.html.twig', array(
                    "user" => $name,
                    "rute" => $_SESSION["rute"],
                    'title' => 'Create Article',
                    "card" => $card,
                    "portfoli" => $card->getIdPortfoli(),
                    'form' => $form->createView(),));
    }

    /**
     * Allow to edit an article.
     * @Route("/editarticle/{article}", name="editarticle")
     * @param Request $request Data form receibed.
     * @param Article $article Article to edit.
     */
    public function editArticlesAction(Request $request, Article $article) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $article->getIdCard()->getIdPortfoli()->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Get the current user's full name.
        $name = $user->getName() . " " . $user->getSurname();
        // Create the article form by an existing article.
        $form = $this->createFormBuilder($article)
                ->add('title', TextType::class, array('label' => 'Name:'))
                ->add('content', TextareaType::class, array('label' => 'Content:',
                    "attr" => array("rows" => 6, "cols" => 45)))
                ->getForm();

        // Get the form data.
        $form->handleRequest($request);

        // Check if the form is submitted and if it is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('articles', array("card" => $article->getIdCard()->getId()));
        }

        // Call the create article template.
        return $this->render('default/articles/create_article.html.twig', array(
                    "user" => $name,
                    "rute" => $_SESSION["rute"],
                    'title' => 'Edit Article',
                    "portfoli" => $article->getIdCard()->getIdPortfoli(),
                    'card' => $article->getIdCard(),
                    'form' => $form->createView(),));
    }

    /**
     * Allow to remove an article.
     * @Route("/deletearticle/{article}", name="deletearticle")
     * @param Article $article Article to remove.
     */
    public function deleteArticleAction(Article $article) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $article->getIdCard()->getIdPortfoli()->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Removes the article.
        $em = $this->getDoctrine()->getManager();
        $em->remove($article);
        $em->flush();
        // Call the articles list template.
        return $this->render('default/articles/list_article.html.twig', array(
                    "user" => $user->getName() . " " . $user->getSurname(),
                    "rute" => $_SESSION["rute"],
                    "portfoli" => $article->getIdCard()->getIdPortfoli(),
                    'card' => $article->getIdCard()));
    }

}
