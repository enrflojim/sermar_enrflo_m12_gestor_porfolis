<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Portfoli;
use AppBundle\Entity\Subcategory;
use AppBundle\Entity\Card;
use AppBundle\Entity\Article;
use AppBundle\Entity\Image;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Functions\Functions;

/**
 * Manage all the methods referred to images.
 */
class ImageController extends Controller {

    /**
     * Show the table with the images of the id article receibed.
     * @Route("/image/{article}", name="image")
     * @param Article $article Article to show the images.
     */
    public function listImagesAction(Article $article) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $article->getIdCard()->getIdPortfoli()->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Call the images list template.
        return $this->render('default/images/list_images.html.twig', array(
                    "user" => $user->getName() . " " . $user->getSurname(),
                    "rute" => $_SESSION["rute"],
                    "portfoli" => $article->getIdCard()->getIdPortfoli(),
                    "article" => $article));
    }

    /**
     * Allow to create an image for the id article receibed.
     * @Route("/formimage/{article}", name="formimage")
     * @param Request $request Form data receibed.
     * @param Article $article Article to create a new image.
     */
    public function createImagesAction(Request $request, Article $article) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }

        if ($user->getUsername() != $article->getIdCard()->getIdPortfoli()->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Create the image form.
        $formImage = new Image();
        $form = $this->createFormBuilder($formImage)
                ->add('title', TextType::class, array('label' => 'Name:'))
                ->add('description', TextareaType::class, array('label' => 'Description:',
                    "attr" => array("rows" => 6, "cols" => 35), 'required' => FALSE))
                ->add('image', FileType::class, array('label' => false, "attr" => array("class" => "fileimg")))
                ->getForm();

        // Get the form data.
        $form->handleRequest($request);

        // Check if the form is submitted and if it is valid.
        if ($form->isSubmitted() && $form->isValid()) {
            // Check if the form has an image and insert the current datetime to the image name.
            if ($formImage->getImage() != null) {
                $dateTime = date('m-d-Y_h-i-s_a', time());
                $file = $formImage->getImage();
                $formImage->setImage($dateTime.$file->getClientOriginalName());
                $cvDir = $this->container->getparameter('kernel.root_dir') . '/../web/img/articles';
                $file->move($cvDir, $formImage->getImage());
            }
            // Insert the image in the article adding the article referred.
            $formImage->setIdArticle($article);
            $em = $this->getDoctrine()->getManager();
            $em->persist($formImage);
            $em->flush();

            // Redirect to images list template.
            return $this->redirectToRoute('image', array("article" => $article->getId()));

        }

        // Call the create image form template.
        return $this->render('default/images/create_images.html.twig', array(
                    "user" => $user->getName() . " " . $user->getSurname(),
                    "rute" => $_SESSION["rute"],
                    'title' => 'Create Image',
                    "portfoli" => $article->getIdCard()->getIdPortfoli(),
                    "article" => $article,
                    'form' => $form->createView(),));
    }

    /**
     * Allow to edit an image of an article.
     * @Route("/editimage/{image}", name="editimage")
     * @param Request $request Form data receibed.
     * @param Image $image Image to edit.
     */
    public function editImagesAction(Request $request, Image $image) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $image->getIdArticle()->getIdCard()->getIdPortfoli()->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Create the form
        $form = $this->createFormBuilder($image)
                ->add('title', TextType::class, array('label' => 'Name:'))
                ->add('description', TextareaType::class, array('label' => 'Description:',
                    "attr" => array("rows" => 6, "cols" => 35)))
                ->add('image', FileType::class, array('label' => false, 'data_class' => null, 'required' => true,
                    "attr" => array("class" => "fileimg")))
                ->getForm();

        // Get the form data.
        $form->handleRequest($request);

        // Check if the form is valid and if it is submitted.
        if ($form->isSubmitted() && $form->isValid()) {
            // Check if the form has an image and insert the current datetime to the image name.
            if ($image->getImage() != null) {
                $dateTime = date('m-d-Y_h-i-s_a', time());
                $file = $image->getImage();
                $image->setImage($dateTime.$file->getClientOriginalName());
                $cvDir = $this->container->getparameter('kernel.root_dir') . '/../web/img/articles';
                $file->move($cvDir, $image->getImage());
            }
            // Updates the image.
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            // Redirect to images list template.
            return $this->redirectToRoute('image', array("article" => $image->getIdArticle()->getId()));
        }

        // Call the create image template.
        return $this->render('default/images/create_images.html.twig', array(
                    "user" => $user->getName() . " " . $user->getSurname(),
                    "rute" => $_SESSION["rute"],
                    'title' => 'Edit Image',
                    "portfoli" => $image->getIdArticle()->getIdCard()->getIdPortfoli(),
                    "article" => $image->getIdArticle(),
                    'form' => $form->createView(),));
    }

    /**
     * Allow to remove an image.
     * @Route("/deleteimage/{image}", name="deleteimage")
     * @param Image $image image to remove.
     */
    public function deleteImagesAction(Image $image) {
        // Get the current user and redirect it if it is not logged.
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user == "anon.") {
            return $this->redirectToRoute("login");
        }
        if ($user->getUsername() != $image->getIdArticle()->getIdCard()->getIdPortfoli()->getIdUser()->getUsername()) {
            return $this->redirectToRoute("accesdenied");
        }
        // Removes the image.
        $em = $this->getDoctrine()->getManager();
        $em->remove($image);
        $em->flush();
        // Call the images list template.
        return $this->render('default/images/list_images.html.twig', array(
                    "user" => $user->getName() . " " . $user->getSurname(),
                    "rute" => $_SESSION["rute"],
                    "portfoli" => $image->getIdArticle()->getIdCard()->getIdPortfoli(),
                    "article" => $image->getIdArticle()));
    }

}
