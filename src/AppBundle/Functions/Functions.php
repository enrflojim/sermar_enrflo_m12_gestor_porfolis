<?php

namespace AppBundle\Functions;

class Functions{
  public function unique($dades) {
    $m=array();
    foreach ($dades as $cards) {
      foreach ($cards->getSubCategories() as $subcategories) {
        array_push($m, $subcategories->getNameSubcategory());
      }
    }
    return array_unique($m);
  }
}

?>
