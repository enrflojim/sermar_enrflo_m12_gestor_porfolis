<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Portfoli;
use AppBundle\Entity\Subcategory;
use AppBundle\Entity\Card;
use AppBundle\Functions\Functions;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\ORM\EntityRepository;

class CardsController extends Controller {

    /**
     * @Route("/cards/{portfoli}", name="cards")
     */
    public function listCardsAction(Request $request, Portfoli $portfoli) {
        $user = $this->get('security.token_storage')->getToken()->getUser();
    		if($user=="anon."){
    			return $this->redirectToRoute("login");
    		}
        // replace this example code with whatever you need
        return $this->render('default/cards/list_cards.html.twig', array(
                             "user" => $user->getName() . " " . $user->getSurname(),
                             "rute" => $_SESSION["rute"],
                             "portfoli" => $portfoli));
    }

    /**
     * @Route("/formcard/{portfoli}", name="formcard")
     */
    public function createCardAction(Request $request, Portfoli $portfoli) {
        $user = $this->get('security.token_storage')->getToken()->getUser();
		    if($user=="anon."){
			       return $this->redirectToRoute("login");
		    }
        $formCard=new Card();
        $form = $this->createFormBuilder($formCard)
                ->add('title', TextType::class, array('label' => 'Name:'))
                ->add('description', TextareaType::class, array('label' => 'description:', "required"=>false,
                                                                "attr" => array("rows"=> 6, "cols"=>45)))
                ->add('subCategories', EntityType::class, array('class' => 'AppBundle:Subcategory',
                      "multiple"=> true,
                      "attr" => array("class"=>"tam"),
                      'query_builder' => function (EntityRepository $er) use ( $portfoli ) {
                          return $er->createQueryBuilder('s')->join("s.category", "c")
                                    ->where('c.id=:id ')
                                    ->setParameter("id", $portfoli->getIdCategory()->getId());
                                  },
                      'choice_label' => 'name_subcategory', 'label' => 'Categories'))
                ->add('link', TextType::class, array('label' => 'Link:', "required"=>false))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          $formCard->setIdPortfoli($portfoli);
          $em = $this->getDoctrine()->getManager();
          $em->persist($formCard);
          $em->flush();
          return $this->redirectToRoute('cards', array("portfoli" => $portfoli->getId()));
        }
        return $this->render('default/cards/create_card.html.twig', array(
                             "user" => $user->getName() . " " . $user->getSurname(),
                             "rute" => $_SESSION["rute"],
                             'title' => 'Create Card',
                             'portfoli'=>$portfoli,
                             'form' => $form->createView(),));
    }

    /**
     * @Route("/editcard/{card}", name="editcard")
     */
    public function editCardAction(Request $request, Card $card) {
    		$user = $this->get('security.token_storage')->getToken()->getUser();
    		if($user=="anon."){
    			return $this->redirectToRoute("login");
    		}
        $portfoli=$card->getIdPortfoli();
        $form = $this->createFormBuilder($card)
                ->add('title', TextType::class, array('label' => 'Name:'))
                ->add('description', TextareaType::class, array('label' => 'description:',
                                                                "attr" => array("rows"=> 6, "cols"=>35)))
                ->add('subCategories', EntityType::class, array('class' => 'AppBundle:Subcategory',
                      "multiple"=> true,
                      "attr" => array("class"=>"tam"),
                      'query_builder' => function (EntityRepository $er) use ( $portfoli ) {
                          return $er->createQueryBuilder('s')->join("s.category", "c")
                                    ->where('c.id=:id ')
                                    ->setParameter("id", $portfoli->getIdCategory()->getId());
                                  },
                      'choice_label' => 'name_subcategory', 'label' => 'Categories'))
                ->add('link', TextType::class, array('label' => 'Link:', "required"=>false))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $em->flush();
          return $this->redirectToRoute('cards', array("portfoli" => $card->getIdPortfoli()->getId()));
        }
        return $this->render('default/cards/create_card.html.twig', array(
                             "user" => $user->getName() . " " . $user->getSurname(),
                             "rute" => $_SESSION["rute"],
                             'title' => 'Edit Card',
                             'portfoli'=>$portfoli,
                             'form' => $form->createView(),));
    }

    /**
     * @Route("/deletecard/{card}", name="deletecard")
     */
    public function deleteArticleAction(Request $request, Card $card) {
        $user = $this->get('security.token_storage')->getToken()->getUser();
    		if($user=="anon."){
    			return $this->redirectToRoute("login");
    		}
        $em = $this->getDoctrine()->getManager();
        $em->remove($card);
        $em->flush();
        // replace this example code with whatever you need
        return $this->render('default/cards/list_cards.html.twig', array(
                             "user" => $user->getName() . " " . $user->getSurname(),
                             "rute" => $_SESSION["rute"],
                             "portfoli" => $card->getIdPortfoli()));
    }

    /**
     * @Route("/card/{card}", name="card")
     */
    public function test1Action(Request $request, Card $card) {
        $user = $this->getUser();

        $fun=new Functions();
        $subcategories=$fun->unique($card->getIdPortfoli()->getCards());

        return $this->render('default/cardTemplates/programmerCard.html.twig', array(
                    "card" => $card,
                    "articles" => $card->getArticles(),
                    "portfoli" => $card->getIdPortfoli(),
                    "rute" => $_SESSION["rute"],
                    "sub" => $subcategories,
                    "user" => $user->getName() . " " . $user->getSurname()
        ));
    }

}
