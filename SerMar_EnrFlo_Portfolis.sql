-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 27-05-2017 a las 13:39:58
-- Versión del servidor: 5.7.18-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `SerMar_EnrFlo_Portfolis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `card_id` int(11) DEFAULT NULL,
  `title` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `article`
--

INSERT INTO `article` (`id`, `card_id`, `title`, `content`) VALUES
(1, 1, 'Madrid', '<p>Madrid es una de las ciudades por las que siento una gran predilecci&oacute;n por lo que siempre que tengo ocasi&oacute;n me gusta representarla de diferentes maneras y en diferentes &eacute;pocas.</p>\r\n<p>Algunas de mis obras mas representativas son:</p>'),
(2, 1, 'Naturaleza', '<p>Otra de mis grandes pasiones es pintar sobre la naturaleza pero no de una forma paisajística sino centrándome mas en el  costumbrismo representando tanto plantas en solitario como mezclando con actividades cotidianas del día a día.</p>'),
(3, 1, 'Retratos', '<p>En mis retratos como en mis pinturas sobre la naturaleza intento alejarme de la magnificencia de la s grandes obras para centrarme en representar entre medio camino del realismo al surrealismo la vida de personas comunes de la vida cotidiana dandoles un tono sosegado y personal.</p><p>Algunos de mis cuadros son:</p>'),
(4, 2, 'Retratos', '<p>Como en mi estilo predominante dentro de la pintura también he realizado diversos dibujos sobre retratos en los que como en casi toda mi obra sobre los retratos he intentado recoger la sosegado vida rural y las costumbre de la época de una forma realista casi como fotografías. sean mis pinturas al lápiz puedo conseguir de una forma más efectiva ese realismo.</p><p>Algunas de mis obras son:</p>'),
(5, 2, 'Interiorismo', '<p>A lápiz también hago representaciones de la vida urbana con todo el realismo que me representa</p>'),
(6, 2, 'Naturaleza', '<p>En cambio las pinturas hechas a lápiz aunque intento reflejar esa realidad que me representa, intento por otro lado darle una sencillez tan extrema que pueden llegar a parecer en vez de pinturas bocetos de unas pinturas que pueden realizarse en un futuro.</p><p>Algunos de estos cuadros mas representativos son:</p>'),
(7, 3, 'Madera', '<p>Mi carrera dentro de la escultura en madera se ha caracterizado por la representación de la figura humana casi siempre de una forma hiperrealista, aunque también he realizado obras con una tendencia claramente surrealista.</p><p>Algunas de mis obras son:</p>'),
(8, 3, 'Bronze', '<p>Por el contrario mis esculturas en bronce aún siendo de menos cantidad si que puedo decir que son más personales y en las cuales sigo tratando el tema principal en todas mis esculturas que el la figura humana.</p><p>Algunas de mis obras son:</p>'),
(9, 4, 'ncncvncvncvncvnvcn', '<p>cvncvncvncvncncncncncncn</p>'),
(22, 10, 'Detalles', '<div class="learn-more-content" style="visibility: visible; display: block;">\r\n<ul class="prim">\r\n<li><strong>Tienda Online / Virtual: </strong>con CMS <a href="http://www.prestashop.com/es/" rel="nofollow">PrestaShop </a>.</li>\r\n<li><strong>Detalles Generales:</strong>\r\n<ul>\r\n<li>M&oacute;dulo visualizaci&oacute;n &uacute;ltimos post del blog WordPress</li>\r\n<li>Traducci&oacute;n y cambios de textos del theme y m&oacute;dulos</li>\r\n<li>M&oacute;dulo TPV Redsys: instalaci&oacute;n, configuraci&oacute;n y verificaci&oacute;n de su funcionamiento tanto en entorno de prueba y real</li>\r\n<li>M&oacute;dulo Slider</li>\r\n<li>M&oacute;dulo para tarjetas regalo</li>\r\n<li>Integraci&oacute;n con chat Zopim: instalaci&oacute;n y configuraci&oacute;n</li>\r\n<li>Asesoramiento y configuracion de par&aacute;metros para el filtrado de productos por caracter&iacute;sticas</li>\r\n<li>Instalaci&oacute;n de c&oacute;digos Analytics y search console de google</li>\r\n<li>Configuraci&oacute;n y generaci&oacute;n de sitemap</li>\r\n<li>Asesoramiento para el poscionamiento SEO</li>\r\n<li>Instalaci&oacute;n SSL sobre PrestaShop y verificaci&oacute;n de su funcionamiento en el proceso de compra.</li>\r\n<li>M&oacute;dulo de ASM: instalaci&oacute;n, configuraci&oacute;n y revisi&oacute;n del funcionamiento</li>\r\n<li>Configuraci&oacute;n de provincias de Espa&ntilde;a.</li>\r\n<li>Configuraci&oacute;n de transportes en funci&oacute;n de zonas de Espa&ntilde;a (Pen&iacute;nsula, Canarias, Ceuta y Melilla)</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</div>'),
(23, 10, 'Galeria', '<p>Se pueden apreciar algunas im&aacute;genes de como ha quedado la p&aacute;gina al finalizar el trabajo.</p>'),
(24, 11, 'Detalles', '<div class="learn-more-content" style="visibility: visible;">\r\n<ul class="prim">\r\n<li><strong>Tienda Online / Virtual</strong></li>\r\n<li><strong>Version:</strong> PrestaShop</li>\r\n<li><strong>Detalles Generales:</strong>\r\n<ul>\r\n<li>PHP</li>\r\n<li>HTML</li>\r\n<li>CSS</li>\r\n<li>JavaScript</li>\r\n<li>SQL</li>\r\n<li>Implementaci&oacute;n de scripts propios para la gesti&oacute;n de homepage.</li>\r\n<li>Optimizaci&oacute;n del proceso de compra.</li>\r\n<li>M&oacute;dulo Infogr&aacute;fico para productos.</li>\r\n<li>M&oacute;dulo e integraci&oacute;n de google analytics, tracking con anal&iacute;tica de Facebook.</li>\r\n<li>M&oacute;dulo Visualizaci&oacute;n de atributos en pedidos.</li>\r\n<li>M&oacute;dulo visualizaci&oacute;n de mensaje env&iacute;o en funci&oacute;n del tipo de cliente.</li>\r\n<li>Instalaci&oacute;n de configuraci&oacute;n de diversos m&oacute;dulos: redes sociales, nacex, devoluci&oacute;n, etc..</li>\r\n<li>M&oacute;dulo de redes sociales</li>\r\n<li>Asesoramiento y resoluci&oacute;n de tareas Prestashop.</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</div>'),
(25, 11, 'Galeria', '<p>Imagenes del estado final de la p&aacute;gina.</p>'),
(26, 12, 'Aualé', '<p>Aual&eacute; es una interfaz gr&aacute;fica para el juego de la familia mancala <em>oware</em>. Un software libre y multiplataforma creado para el motor de inteligencia artificial Aalina.</p>'),
(27, 12, 'Aalina', '<p>Un motor de inteligencia artificial para el juego de tablero oware. Desarrollado inicialmente como parte de mi Trabajo Final del Grado de Ingenier&iacute;a Inform&aacute;tica.</p>'),
(28, 13, 'Ejemplo de clase utilizada en el proyecto', '<p>Una de las clases gestoras del proyecto.</p>\r\n<p>{code}package ig1.project;<br /><br />/**<br />&nbsp;* Classe que encapsula l\'escena a tractar<br />&nbsp;*<br />&nbsp;* @author&nbsp;&nbsp;&nbsp; UOC<br />&nbsp;* @author&nbsp;&nbsp;&nbsp; Joan Sala Soler<br />&nbsp;* @version&nbsp;&nbsp; 1.0.0<br />&nbsp;*/<br />public class Escena<br />{ <br />&nbsp;&nbsp;&nbsp; private SolidFronteres solids[];&nbsp; // vector de s�lids<br />&nbsp;&nbsp;&nbsp; private FontLlum lights[];&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Fonts de llum<br />&nbsp;&nbsp;&nbsp; private int ns;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // nombre de s�lids a l\'escena<br />&nbsp;&nbsp;&nbsp; private int numLights;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Nombre de fonts de llums<br /><br />&nbsp;&nbsp;&nbsp; // constructor d\'una escena buida, que tindr� maxsolids s�lids com a m�xim<br />&nbsp;&nbsp;&nbsp; public Escena(int maxsolids)<br />&nbsp;&nbsp;&nbsp; {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; solids = new SolidFronteres[maxsolids];<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; lights = new FontLlum[8];<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ns = 0;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; numLights = 0;<br />&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp; // consultors<br />&nbsp;&nbsp;&nbsp; public int getNumSolids()<br />&nbsp;&nbsp;&nbsp; {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; return ns;<br />&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp; public SolidFronteres getSolid(int i)<br />&nbsp;&nbsp;&nbsp; {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; return solids[i];<br />&nbsp;&nbsp;&nbsp; }<br /><br />&nbsp;&nbsp;&nbsp; // modificadors<br />&nbsp;&nbsp;&nbsp; public void addSolid(SolidFronteres s)<br />&nbsp;&nbsp;&nbsp; {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; solids[ns]=s;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ns++;<br />&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp; /**<br />&nbsp;&nbsp;&nbsp;&nbsp; * Torna el nombre de fonts de llum de l\'escena.<br />&nbsp;&nbsp;&nbsp;&nbsp; *<br />&nbsp;&nbsp;&nbsp;&nbsp; * @return nombre de fonts de llum<br />&nbsp;&nbsp;&nbsp;&nbsp; */<br />&nbsp;&nbsp;&nbsp; public int getNumLights() {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; return numLights;<br />&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp; /**<br />&nbsp;&nbsp;&nbsp;&nbsp; * Torna la font de llum especificada.<br />&nbsp;&nbsp;&nbsp;&nbsp; *<br />&nbsp;&nbsp;&nbsp;&nbsp; * @param index&nbsp; �ndex de la font de llum<br />&nbsp;&nbsp;&nbsp;&nbsp; * @throws UnsupportedOperationException&nbsp; si l\'escena no cont� la llum<br />&nbsp;&nbsp;&nbsp;&nbsp; */<br />&nbsp;&nbsp;&nbsp; public FontLlum getLight(int index) throws UnsupportedOperationException {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if (index &gt;= getNumLights())<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; throw new UnsupportedOperationException(<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "L\'escena no cont� cap llum per a l\'�ndex");<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; return lights[index];<br />&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp; /**<br />&nbsp;&nbsp;&nbsp;&nbsp; * Afegeix una font de llum a l\'escena. Es poden afegir com a m�xim<br />&nbsp;&nbsp;&nbsp;&nbsp; * vuit fonts de llum diferents.<br />&nbsp;&nbsp;&nbsp;&nbsp; *<br />&nbsp;&nbsp;&nbsp;&nbsp; * @param f&nbsp; Font de llum.<br />&nbsp;&nbsp;&nbsp;&nbsp; *<br />&nbsp;&nbsp;&nbsp;&nbsp; * @throws UnsupportedOperationException si el nombre m�xim de llums<br />&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; possibles ja est� assignat.<br />&nbsp;&nbsp;&nbsp;&nbsp; **/<br />&nbsp;&nbsp;&nbsp; public void addLight(FontLlum f) throws UnsupportedOperationException {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if (numLights &gt; 7)<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; throw new UnsupportedOperationException(<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "No es pot afegir la font de llum perqu� sobrepassarieu" +<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "el nombre m�xim perm�s de fonts de llum" <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; );<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; lights[numLights] = f;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; numLights++;<br />&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp; // m�tode per a obtenir la capsa m�nima contenidora de l\'escena<br />&nbsp;&nbsp;&nbsp; public Capsa capsacontenidora()<br />&nbsp;&nbsp;&nbsp; {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // retorna la capsa contenidora de l\'escena<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Capsa cap;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; float xmin, xmax, ymin, ymax, zmin, zmax;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if (ns==0) {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; cap = new Capsa(new Vertex(0.f,0.f,0.f), new Vertex(0.f,0.f,0.f));<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; else {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // inicialitzem els valors m�xims i m�nims als del primer v�rtex<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; xmin = xmax = solids[0].getVertex(0).getX();<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ymin = ymax = solids[0].getVertex(0).getY();<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; zmin = zmax = solids[0].getVertex(0).getZ();<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // recorrem l\'escena per a trobar els v�rtexs m�xim i m�nim<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; for (int obj=0; obj&lt;ns; obj++) { // per cada s�lid<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; for (int v=0; v&lt;solids[obj].getNumVertexs(); v++) {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // comprovem component X<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; float aux = solids[obj].getVertex(v).getX();<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if (aux &gt; xmax) xmax = aux;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; else if (aux &lt; xmin) xmin = aux;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // comprovem component Y<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; aux = solids[obj].getVertex(v).getY();<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if (aux &gt; ymax) ymax = aux;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; else if (aux &lt; ymin) ymin = aux;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // comprovem component Z<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; aux = solids[obj].getVertex(v).getZ();<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if (aux &gt; zmax) zmax = aux;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; else if (aux &lt; zmin) zmin = aux;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; cap = new Capsa(new Vertex(xmin, ymin, zmin),<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; new Vertex(xmax, ymax, zmax));<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; return cap;<br />&nbsp;&nbsp;&nbsp; }<br />}{code}<br /><br /></p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `card`
--

CREATE TABLE `card` (
  `id` int(11) NOT NULL,
  `portfoli_id` int(11) DEFAULT NULL,
  `title` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_card` date NOT NULL,
  `date_string_card` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `card`
--

INSERT INTO `card` (`id`, `portfoli_id`, `title`, `description`, `link`, `date_card`, `date_string_card`) VALUES
(1, 5, 'Óleos', '<p>Como pintor prefiero la pintura al &oacute;leo que busca entre la realidad que me rodea aquellos aspectos cotidianos, en el que recogo con un tratamiento pleno de detallismo, rozando lo fotogr&aacute;fico. Mis preferencias artisticas pueden abarcar diferentes aspectos de la vida como:.</p>', NULL, '2017-05-24', 'May 24,2017'),
(2, 5, 'Lápiz', '<p>Otro estilo sobre el cual tambi&eacute;n me ha gustado trabajar el cual me ha gustado pintar es el l&aacute;piz que aunque mi obra en este aspecto no ha sido tan prol&iacute;fica s&iacute; que he realizado diferentes trabajos sobre diversos temas entre los cuales est&aacute;n:</p>', NULL, '2017-05-24', 'May 24,2017'),
(3, 5, 'Madera y bronce', '<p>Aunque soy m&aacute;s prol&iacute;fico en la pintura tambi&eacute;n he realizado deferentes obras en el mundo de la escultura sobre todo he trabajado con madera y bronce, tratando como linea general tratando casi siempre la figura humana, Algunas de mis esculturas m&aacute;s conocidas son:</p>', NULL, '2017-05-24', 'May 24,2017'),
(4, 1, 'STDiscos', '<p>etsefsdfsdfsdgsdgsgsdgsdg</p>', NULL, '2017-05-25', 'May 25,2017'),
(10, 9, 'Mr.Barbero', '<p><span class="et_pb_fullwidth_header_subhead">Tienda online de cosm&eacute;ticos para hombre</span></p>', 'http://www.mrbarbero.com/', '2017-05-27', 'May 27,2017'),
(11, 9, 'thebrukaker', '<p><span class="et_pb_fullwidth_header_subhead">Tienda online: La marca de ropa que produce en las mejores f&aacute;bricas nacionales.</span></p>', 'https://thebrubaker.com/', '2017-05-27', 'May 27,2017'),
(12, 10, 'Oware', '<p>Pertenece a la familia de los juegos <em>mancalas</em>, y son uno de los juegos de tablero m&aacute;s antiguos de los que se tiene constancia.</p>\r\n<p>Requiere un <em>tablero oware</em>, que es una tabla rectangular que posee dos hileras excavadas de seis hoyos. En los extremos de la tabla hay otras dos cavidades, un poco m&aacute;s grandes, que son dep&oacute;sitos para las piezas capturadas.</p>\r\n<p>Las piezas que se usan son semillas o guijarros (del tama&ntilde;o de un garbanzo o poco m&aacute;s). Hacen falta 48.</p>\r\n<p>Los juegos <em>mancalas</em> tienen las siguientes caracter&iacute;sticas:</p>\r\n<ul>\r\n<li>No hay fichas de uno u otro jugador, sino un campo propio y uno del contrario.</li>\r\n<li>Cada campo est&aacute; formado por una fila de seis casillas.</li>\r\n<li>Las semillas (fichas) se siembran y recogen.</li>\r\n<li>La siembra se hace cogiendo todas las semillas y dej&aacute;ndolas de una en una en las casillas siguientes.</li>\r\n</ul>', NULL, '2017-05-27', 'May 27,2017'),
(13, 10, 'Informática gráfica', '<p>Un mundo virtual tridimensional por el que nos podemos desplazar. Implementaci&oacute;n de rutinas de gesti&oacute;n de luces, texturas, creaci&oacute;n de figuras y movimientos de la c&aacute;mara.</p>', 'http://www.joansala.com/files/ig-epf.zip', '2017-05-27', 'May 27,2017');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cards_categories`
--

CREATE TABLE `cards_categories` (
  `card_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cards_categories`
--

INSERT INTO `cards_categories` (`card_id`, `subcategory_id`) VALUES
(1, 6),
(2, 6),
(3, 5),
(4, 1),
(10, 2),
(11, 2),
(12, 1),
(13, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name_category` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`id`, `name_category`) VALUES
(2, 'Artist'),
(5, 'Boss'),
(4, 'Employee'),
(1, 'Informatics'),
(3, 'Ingeners');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `article_id` int(11) DEFAULT NULL,
  `title` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `image`
--

INSERT INTO `image` (`id`, `article_id`, `title`, `description`, `image`) VALUES
(1, 1, 'Madrid desde la torre de bomberos de Vallecas', '<p>Pintado entre los años 1990 hasta el 2006 y su medida total de 250 x 406cm. Pretendo una obra realista, cotidiana y bien proporcionada. Los colores son alegres, entre ellos esta el rojo, diferentes tonos de azúl, naranja, tonos verdes por los campos.</p><p>Se expone en el Museo Thysse-Bornemisza.</p>', 'madrid_vallecas.jpg'),
(2, 1, 'Madrid desde Torres Blancas', '<p>Cuadro que pinte sobre tabla de 145 por 244 centímetros entre los años 1976 y 1982, representa en final del atardecer de una tarde veraniega madrileña.</p><p>Se expone el Museo Thyssen-Bornemisz.</p>', 'madrid_isto_desde_torres_blancas.jpg'),
(3, 1, 'Madrid from Capitán Haya', '<p>Pintado sobre lienzo pegado a tabla con un tamaño de184 x 245 cm y pintado entre los años 1987 y 1996 represento dos de mis temas favoritos el realismo casi fotográfico y la ciudad de Madrid.</p><p>Se expone en el museo Museo Thyssen-Bornemisza.</p>', 'madrid_haya.jpg'),
(4, 1, 'Calle de Santa Rita', '<p>Pintado sobre tabla con unas dimensiones de 62 x 88,5 cm y pintado en el año 1961 con este cuadro entre costumbrista y fotográfico intento mostrar la vida cotidiana de esta popular calle de Madrid.</p><p>Este cuadro no está expuesto en ningún museo ya que pertenece a una colección particular.</p>', 'santarita.jpg'),
(5, 2, 'Carmen Jugando', '<p>En este cuadro pintado sobre tela de unas dimensiones e 106,5 x 149,5 cm y pintado en 1960 intento representar la vida cotidiana de la España rural de la época con el paisaje natural de la nisma de la forma más realista posible.</p><p>Este cuadro se muestra en el Museo Thyssen-Bornemisza</p>', 'carnen_jugando.jpg'),
(6, 2, 'La parra', '<p>Pintada sobre tela  en 1955 y de unas dimensiones de 48,5 x 48,5 cm intente crear un contraste con la visión aérea y lejana, esta visión cercana, táctil, que palpa los contornos de las ramas, las hojas y las frutas.</p><p>Este cuadro se expone en el museo thyssen-bornemisz</p>', 'la_parra.jpg'),
(7, 2, 'Lirios y Rosas', '<p>Pintado sobre table en 1980 y de unas dimensiones 66,5 x 66,5 cm con este cuadro intento relajarme des pué de tanta pintura sobre la vida urbana para rememorar cuadros que representen la sencillez de la vida como una parra (La parra 1955 ) o un membrillero (Membrillero 1966).</p><p>Este cuadro pertenece a una colección particular</p>', 'lirios_y_rosa_1980.jpg'),
(8, 2, 'El jardín de atrás', '<p>Cuadro pintado sobre tela  con unas dimensiones de 86,5 x 100 cm y pintado en 1969 intento como en todos mis cuadros sobre la naturaleza alejarne de la vida urbana y representar con sencillez el costumbrismo de la vida rural.</p><p>Este cuadro se expone en el Museo Thyssen-Bornemisza</p>', 'jardin_atras.jpg'),
(9, 3, 'Simforoso y Josefa', '<p>Pintado sobre tela en 1955 y de unas dimensiones de 62 x 88 cm es mi primer cuadro pintado y represento muchos de mis temas recurrentes en mis retratos familia, sencillez. Aunque en este cuadro no se puede apreciar aún ese realismo casi fotografico de muchos de mis siguientes retratos.</p>', 'simforoso_josefa.jpg'),
(10, 3, 'Antonio y Carmen', '<p>Pintado sobre tela en 1956 y de unas dimensiones de 60,5 x 83,5 cm este cuadro sobe mis padres ya refleja aparte de los teman más recurrentes en mi obra sobre retratos el realismo que intento transmitis en mi obra.</p><p>Este cuadro pertenece a una coleccion particular.</p>', 'padres.jpg'),
(11, 3, 'Carmencita de comunión', '<p>Pintado sobre tabla en 1960 y de unas dimensiones de 100 x 81 cm intento representar de la forma mas realista posible una situación cotidiana de la vida de las personas.</p><p>Este cuadro pertenece a una colección particular</p>', 'comunion.jpg'),
(12, 4, 'Josefa', '<p>Esta litografía pintada a lápiz pintada en 1981 con unas dimensiones de 35 x 50 cm representa un momento en la vida cotidiana de mi madre del cual se puede observar ese realismo fotográfico que me caracteriza.</p>', 'josefa.jpg'),
(13, 4, 'María', '<p>Pintura sobre papel adherido a tabla de 1972 de dimensiones 70 x 53 cm en el que puede apreciarse ese realismo característico de mis pinturas a lápiz que bien puede pasar por una fotografía.</p><p>Este cuadro se expone en el Museo Thyssen-Bornemisza.</p>', 'maria.gif'),
(14, 5, 'Estudio con tres puertas', '<p>Pintada sobre papel en 1970 de 98 x 113 representa el estudio de la Plaza de Infancia donde he realizado gran parte de las obras que me caracterizan.</p><p>Este cuadro se puede ver en el Museo Thyssen-Bornemisza.</p>', 'real_lopez_estudio.jpg'),
(15, 5, 'Casa de Antonio López Torres', '<p>Lo pinte sobre papel entre 1972-1975 de 82 x 68 cm donde vuelvo a representar de una forma casi fotográfica un paisaje urbano de interior nuestra vivienda familiar.</p><p>Este cuadro esta en el Museo Thyssen-Bornemisza.</p>', 'antonio_lopez_thyssen_5-640x640x80.jpg'),
(16, 6, 'Ciruelo', '<p>Pintura sobre papel del año 1989 de unas dimensiones 73 x 87 cm en la que puede apreciarse más que una pintura podría ser un boceto de una pintura.</p><p>Pertenece al Museo Thyssen-Bornemisza.</p>', 'ciruelo.jpg'),
(17, 6, 'Calabazas', '<p>Pintada sobre papel entre 1994-1995 de 72,7 x 90,8 cm clásica representación de este tipo de pinturas que suelo realizar.</p><p>Pertenece a Colecciones I.C.O.</p>', 'calabazas.jpg'),
(18, 6, 'Árbol de membrillo', '<p>Pintada sobre papel en 1990 de 104 x 120 cm, esta obra aunque algo más elaborada continua con la esteica sinple que representan mis obras sobre este tema.</p><p>Obra expuesta en el museo Museo Thyssen-Bornemisza.</p>', 'membrillo.JPG'),
(19, 7, 'Hombre y mujer', '<p>Realizada madera de abedul y cristal entre 1968-1994 con la técnica de talla, modelado, ensamblaje, encolado y policromía es una de mis obras más realistas.</p><p>El hombre mide 1,95 metos y la mujer 1,69 metros.</p>', 'hombre_mujer.png'),
(20, 7, 'María dormida', '<p>Realizada en madera policromada el año 1964 con unas dimensiones de 24,5 x 45 x 76 cm es otra de mis obras mas realistas en la que se puede vislumbrar con todo lujo de detalles la figura de la niña..</p><p>Pertenece a una coleccion privada pero esta expuesta en el museo Museo Thyssen-Bornemisza.</p>', 'niño_dormido.jpg'),
(21, 7, 'Mujer durmiendo', '<p>Realizada en madera con la tecnica de la talla, encolado y policromía en el año 1963 y de  121 x 205 x 12 cm la considero una de mis esculturas en madera en la que tengo presente el surrealismo aunque sin dejar de lado ese realismo que me caracteriza.</p>', 'mujer_durmiendo.jpeg'),
(22, 8, 'Día y noche', '<p>Son dos cabezas de seis metros y representa la cabeza de una de mis nietas de unos meses. En “Día” la niña dirige a los espectadores una mirada atenta y despierta, mientras que en “Noche“, con los ojos cerrados duerme plácidamente.</p><p>Pueden verse en la estación de atocha.</p>', 'día-noche-antonio-lópez.jpg'),
(23, 8, 'Muger de coslada', '<p>Es la escultura de un busto de una mujer realizada en 2010 siendo una de mis últimas obras de  5m de alto y 3000 kg de peso como casi siempre realizado con gran realismo y que esta expuesta en la rambla de coslada en madrid.</p>', 'coslada.jpg'),
(24, 22, 'Estadísticas', '<p>Porcentajes de uso de cada c&oacute;digo de programaci&oacute;n utilizado para crear la p&aacute;gina.</p>', '05-27-2017_12-00-32_pmCaptura de pantalla 2017-05-27 a las 11.57.27.png'),
(25, 23, 'homepage', '<p>P&aacute;gina inicial de la web.</p>', '05-27-2017_12-09-48_pmmrbarbeiro.jpeg'),
(26, 23, 'Barbas page', '<p>P&aacute;gina interna con los productos para barbas.</p>', '05-27-2017_12-14-51_pmasd.png'),
(27, 25, 'homepage', '<p>P&aacute;gina indice de la web thebrukaker</p>', '05-27-2017_12-34-27_pmthebrubaker.jpeg'),
(28, 24, 'Estadísticas', '<p>Porcentajes de uso de los diferentes lenguajes de programaci&oacute;n para programar la p&aacute;gina web.</p>', '05-27-2017_12-38-38_pmCaptura de pantalla 2017-05-27 a las 12.20.41.png'),
(29, 26, 'Aualé', '<p>Imagen de la interfaz gr&aacute;fica de usuario Aual&eacute; para Oware.</p>', '05-27-2017_12-46-35_pmauale.jpeg'),
(30, 27, 'Aalina', '<p>Imagen de una m&iacute;nima parte del tipo de respuestas que da la inteligencia artificial Aalina</p>', '05-27-2017_12-48-25_pmaalina.jpeg'),
(31, 28, 'Visualización del estado final del proyecto', '<p>Se puede apreciar el estado final del proyecto.</p>', '05-27-2017_12-57-29_pminfogra.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portfoli`
--

CREATE TABLE `portfoli` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_premium` tinyint(1) NOT NULL,
  `is_informatic` tinyint(1) NOT NULL,
  `img_portfoli` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_portfoli` date NOT NULL,
  `date_string` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `portfoli`
--

INSERT INTO `portfoli` (`id`, `category_id`, `user_id`, `title`, `is_premium`, `is_informatic`, `img_portfoli`, `date_portfoli`, `date_string`) VALUES
(1, 1, 1, 'EF Informatics', 0, 1, 'EF_INFORMATICS.png', '2017-05-24', 'May 24 2017'),
(2, 4, 1, 'EF Emoloyee', 1, 0, 'EF_Employee.png', '2017-05-24', 'May 24 2017'),
(3, 2, 3, 'FC Artist', 0, 0, 'FC_Artist.png', '2017-05-24', 'May 24 2017'),
(4, 2, 2, 'LS Artist', 0, 0, 'LS_Artist.png', '2017-05-24', 'May 24 2017'),
(5, 2, 4, 'AL_Artist', 0, 0, 'AL_Artist.jpg', '2017-05-24', 'May 24 2017'),
(9, 1, 5, 'JC Informatic', 0, 1, '05-27-2017_12-21-58_pmprestashop.jpeg', '2017-05-27', 'May 27 2017'),
(10, 1, 6, 'JS Informatic', 0, 1, '05-27-2017_01-38-59_pmjoansala.jpeg', '2017-05-27', 'May 27 2017');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategory`
--

CREATE TABLE `subcategory` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name_subcategory` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `subcategory`
--

INSERT INTO `subcategory` (`id`, `category_id`, `name_subcategory`) VALUES
(1, 1, 'DAM'),
(2, 1, 'DAW'),
(3, 1, 'ASIX'),
(4, 1, 'SMIX'),
(5, 2, 'Esculture'),
(6, 2, 'Pinture'),
(7, 2, 'Actors'),
(8, 2, 'Musicians'),
(9, 3, 'Architecture'),
(10, 3, 'Aerospace'),
(11, 3, 'Biomedical'),
(12, 3, 'chemistry'),
(13, 3, 'mathematical'),
(14, 4, 'laborer'),
(15, 4, 'electrician'),
(16, 4, 'salesman'),
(17, 4, 'functionary'),
(18, 4, 'palette'),
(24, 5, 'Chief of staff'),
(25, 5, 'Head of administration'),
(26, 5, 'Director'),
(27, 5, 'Head of RRHH'),
(28, 5, 'Head of I+D');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `gender` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `floor` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `door` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `surname`, `lastname`, `date`, `gender`, `phone`, `email`, `type`, `street`, `number`, `floor`, `door`, `cp`, `city`, `country`, `username`, `password`, `link_image`) VALUES
(1, 'Enrique', 'Flo', 'Jimeno', '1966-11-22', 'Male', '11111111', 'a@aa.aa', 'Street', 'Sacedon', '20-22', '2º', '2ª', '08032', 'Barcelona', 'España', 'kike', '$2y$13$YZ3JQVAtenSqd08n41sfTOp/OdfB9mVDY8Mj7SUZiZ/RWr3cg1FMK', 'foto_kike.png'),
(2, 'Laura', 'Sanchez', 'Gonzalez', '1994-06-06', 'Female', '222222222', 'b@bb.bb', 'Street', 'Campoamor', '12', NULL, NULL, '08032', 'Barcelona', 'España', 'laura', '$2y$13$HIBCFVzZoOhR.SJyoUEmPOKqtcpLnwIxCY1id4V61QJnVn0nRJIpi', 'foto_laura.jpg'),
(3, 'Adolfo "fito"', 'Cabrales', 'Sanchis', '1966-10-04', 'Male', '333333333', 'c@cc.cc', 'Square', 'Pastrana', '10', '1º', '1ª', '08032', 'Barcelona', 'España', 'fito', '$2y$13$sZwESHWQgWnbdndN/ylkyuim0DxEdRAmYzrL4u58Slf/50x6SxCIK', 'fito.jpg'),
(4, 'Antonio', 'López', 'García', '1936-01-06', 'Male', '555555555', 'e@ee.ee', 'Street', 'Dante', '12', '2º', '1ª', '08032', 'Barcelona', 'España', 'tony', '$2y$13$wcKOgANkHX3hp/s98oYSvO0btmJEpXyfQYgFhC0iBM.a6tgyVW3G.', 'antonio_lopez.jpg'),
(5, 'Juan', 'Carlos', 'Ruiz', '1974-01-01', 'Male', '934682546', 'juanCarlosRuiz@example.example', 'Street', 'Bilbao', '2', '3º', '4a', '08013', 'Barcelona', 'Spain', 'Juan', '$2y$13$t.wD/.Ib8AVfeZp1DfvtDOUa2rPe4jymdY.uxeZnqRJMth.ug34tK', 'wordpress.jpeg'),
(6, 'Joan', 'Sala', 'Lopez', '1987-10-09', 'Male', '96452835', 'joanSalaLopez@example.example', 'Street', 'Paralel', '23', NULL, NULL, '08013', 'Barcelona', 'Spain', 'Joan', '$2y$13$c49h/ap96eg0DSZ8CP8Dg.aa1eR7dspHDKcAOp2J.DIB4INQEVJaa', 'joansala.jpeg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_23A0E664ACC9A20` (`card_id`);

--
-- Indices de la tabla `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_161498D3D12E326C` (`portfoli_id`);

--
-- Indices de la tabla `cards_categories`
--
ALTER TABLE `cards_categories`
  ADD PRIMARY KEY (`card_id`,`subcategory_id`),
  ADD KEY `IDX_A3577FF34ACC9A20` (`card_id`),
  ADD KEY `IDX_A3577FF35DC6FE57` (`subcategory_id`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_64C19C12A9DEC0F` (`name_category`);

--
-- Indices de la tabla `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C53D045F7294869C` (`article_id`);

--
-- Indices de la tabla `portfoli`
--
ALTER TABLE `portfoli`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5ED441A512469DE2` (`category_id`),
  ADD KEY `IDX_5ED441A5A76ED395` (`user_id`);

--
-- Indices de la tabla `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_DDCA448EF2E894` (`name_subcategory`),
  ADD KEY `IDX_DDCA44812469DE2` (`category_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `card`
--
ALTER TABLE `card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `portfoli`
--
ALTER TABLE `portfoli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `FK_23A0E664ACC9A20` FOREIGN KEY (`card_id`) REFERENCES `card` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `card`
--
ALTER TABLE `card`
  ADD CONSTRAINT `FK_161498D3D12E326C` FOREIGN KEY (`portfoli_id`) REFERENCES `portfoli` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `cards_categories`
--
ALTER TABLE `cards_categories`
  ADD CONSTRAINT `FK_A3577FF34ACC9A20` FOREIGN KEY (`card_id`) REFERENCES `card` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_A3577FF35DC6FE57` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategory` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `FK_C53D045F7294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `portfoli`
--
ALTER TABLE `portfoli`
  ADD CONSTRAINT `FK_5ED441A512469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_5ED441A5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `subcategory`
--
ALTER TABLE `subcategory`
  ADD CONSTRAINT `FK_DDCA44812469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
